package net.minecraft.src;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import net.halalaboos.client.Client;

public final class GameWindowListener extends WindowAdapter
{
    public void windowClosing(WindowEvent par1WindowEvent)
    {
    	Client.getInstance().onEnd();
        System.err.println("Someone is closing me!");
        System.exit(1);
    }
}
