package net.halalaboos.client.mods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipFile;

import net.halalaboos.client.manager.ModManager;
import net.minecraft.client.Minecraft;

public class ModLoader {

	public ModLoader() {
		
	}
	
	public Mod loadMod(File jarFile, String classPath) throws ClassNotFoundException, MalformedURLException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ClassLoader loader = URLClassLoader.newInstance(new URL[] { jarFile.toURI().toURL() }, getClass().getClassLoader());
		Class<?> modClass = Class.forName(classPath, true, loader);
		Class<? extends Mod> startClass = modClass.asSubclass(Mod.class);
		Constructor<? extends Mod> constructor = startClass.getConstructor();	
		return (Mod) constructor.newInstance();
	}
	
	public void loadExternalMods(Minecraft mc) {
		File externalModFolder = new File(mc.theClient.getClientUtils().getSaveDirectory(), "mods");
		if(!externalModFolder.exists())
			externalModFolder.mkdirs();
		int externalCount = 0;
		for(File jarFile : externalModFolder.listFiles()) {
			if(jarFile.getName().endsWith(".jar")) {
				try {
					String classPath = getJarFileClassPath(jarFile, new JarFile(jarFile));
					Mod mod = loadMod(jarFile, classPath != null ? classPath:"net.halalaboos.client.mods.external.ExternalMod");
					ModManager.load(mod);
					externalCount++;
				} catch (ClassNotFoundException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (MalformedURLException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (SecurityException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (InstantiationException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
					e.printStackTrace();
				}
			} else if(jarFile.getName().endsWith(".zip")) {
				try {
					for(File internalJar : mc.theClient.getClientUtils().extractZipFile(jarFile, externalModFolder)) {
						if(internalJar.getName().endsWith(".jar")) {
							try {
								String classPath = getJarFileClassPath(internalJar, new JarFile(internalJar));
								Mod mod = loadMod(internalJar, classPath != null ? classPath:"net.halalaboos.client.mods.external.ExternalMod");
								ModManager.load(mod);
								externalCount++;
							} catch (ClassNotFoundException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							} catch (SecurityException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							} catch (InstantiationException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								mc.theClient.addChatMessage("Unable to load jar:\2474 " + e.getMessage());
								e.printStackTrace();
							}
						}
					}
				} catch (IOException e) {
					mc.theClient.addChatMessage("Unable to unzip file:\2474 " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		mc.theClient.addChatMessage(externalCount + " external mods loaded.");
	}
	
	public void deleteFilesWithinFolder(File file) {
		if(file.isDirectory()) {
			for(File internalFile : file.listFiles()) {
				if(internalFile.isDirectory())
					deleteFilesWithinFolder(internalFile);
				internalFile.delete();
			}
		}
	}
	
	public String getJarFileClassPath(File actualFile, JarFile jarFile) {
		try {
			Enumeration<JarEntry> enu= jarFile.entries();
			while (enu.hasMoreElements()) {   
				JarEntry je = enu.nextElement();
				if(je.getName().equalsIgnoreCase("mod.info") && !je.isDirectory()) {
					InputStream is = jarFile.getInputStream(je);
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			        for(String s; (s = reader.readLine()) != null; ) {
			        	if(s.toLowerCase().startsWith("classpath") && s.contains(":")) {
			        		return s.substring(10);
			        	}
			        }
			        reader.close();			         
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
