package net.halalaboos.client.mods.world;

import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.client.base.Value;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventBlockInteract;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.event.events.EventServerUpdate;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.mods.player.Speedmine;
import net.halalaboos.client.overrides.HEntityClientPlayerMP;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.Coordinates;
import net.minecraft.src.*;

import org.lwjgl.input.*;
import org.lwjgl.opengl.GL11;

public class Nuker extends Mod implements Listener {

	private Coordinates currentBlock, lastBlock;
	private int nukerDelay, selectedID = -1;
	private double currentBlockDamage;
	private boolean sendFirstPacket = true;
	Value creativeSpeed;
	private Speedmine speedmine;
	
	public Nuker() {
		super("Nuker", Keyboard.KEY_COMMA);
		setCategory(WindowCategory.WORLD);
		setType(Type.MINING);
		creativeSpeed = new Value("Creative Nuker", 4, 1, 5, 1);
		ValueManager.add(creativeSpeed);
		setDescription("Automatically break selected block");

	}

	public void preServerUpdate(HEntityClientPlayerMP p) {
		if (currentBlock != null)
			return;
		
		int width = 5;
		for (int x = width; x >= -width; x--) {
			for (int y = width; y >= -width; y--) {
				for (int z = width; z >= -width; z--) {

					int xx = (int) p.posX + x;
					int yy = (int) p.posY + y;
					int zz = (int) p.posZ + z;
					int bid = mc.theWorld.getBlockId(xx, yy, zz);
					Block b = Block.blocksList[bid];
					if (canReachBlock(xx, yy, zz) && bid == selectedID) {
						currentBlock = new Coordinates(xx, yy, zz);
						if (mc.playerController.isInCreativeMode()) {
							if (ModManager.getNoCheatMode().isState())
								return;
							else
								breakBlock(false);
						} else {
							sendFirstPacket = true;
							nigger(0);
							return;
						}
					}
				}
			}
		}

	}

	
	public void postServerUpdate(HEntityClientPlayerMP p) {
		updateBlock();

		if (currentBlock == null)
			return;

		if (nukerDelay > 0) {
			nukerDelay--;
			return;
		}

		if (sendFirstPacket) {
			sendFirstPacket = false;
			return;
		}

		int id = mc.theWorld.getBlockId((int) currentBlock.getX(),
				(int) currentBlock.getY(), (int) currentBlock.getZ());
		Block block = Block.blocksList[id];

		currentBlockDamage += (block.getPlayerRelativeBlockHardness(
				p, p.worldObj, (int) currentBlock.getX(),
				(int) currentBlock.getY(), (int) currentBlock.getZ()))
				* (speedmine.isState() ? speedmine.getSpeed().getValue() : 1);
		
		mc.theWorld.destroyBlockInWorldPartially(p.entityId,
				(int) currentBlock.getX(), (int) currentBlock.getY(),
				(int) currentBlock.getZ(),
				(int) (currentBlockDamage * 10.0F) - 1);
		if (currentBlockDamage >= 1.0 || mc.playerController.isInCreativeMode()) {
			breakBlock(true);
		}
	}

	public void breakBlock(boolean delayOthers) {
		Block b = Block.blocksList[mc.theWorld.getBlockId( (int) currentBlock.getX(), (int) currentBlock.getY(), (int) currentBlock.getZ())];
		
		if(delayOthers && !superPick(currentBlock))
			stopJonahFromTakingTheCookies(mc.playerController.isInCreativeMode());
		
		if (mc.playerController.isInCreativeMode()) {
				nigger(0);
		} else {
			if(!(superPick(currentBlock))) 
				nigger(2);
		}
		mc.playerController.onPlayerDestroyBlock((int) currentBlock.getX(), (int) currentBlock.getY(), (int) currentBlock.getZ(), 1);
		currentBlockDamage = 0;
		lastBlock = currentBlock;
		currentBlock = null;
	}

	public boolean superPick(Coordinates block) {
		Block b = Block.blocksList[mc.theWorld.getBlockId( (int) block.getX(), (int) block.getY(), (int) block.getZ())];
		if(b == null || block == null)
			return false;
		return b.getPlayerRelativeBlockHardness(mc.thePlayer, mc.theWorld,  (int) block.getX(), (int) block.getY(), (int) block.getZ()) >= 1;
	}
	
	/**
	 * Send packet method.
	 * */
	public void nigger(int swag) {
		if(swag == 1)
			mc.getNetHandler().addToSendQueue(new Packet14BlockDig(swag, (int) currentBlock.getX(), (int) currentBlock.getY(), (int) currentBlock.getZ(), -1));
		else
			mc.getNetHandler().addToSendQueue(new Packet14BlockDig(swag, (int) currentBlock.getX(), (int) currentBlock.getY(), (int) currentBlock.getZ(), 1));
			mc.getNetHandler().addToSendQueue(new Packet18Animation(mc.thePlayer, 1));
	}

	public void updateBlock() {
		if (currentBlock == null)
			return;

		if (!canReachBlock(currentBlock.getX(), currentBlock.getY(),
						currentBlock.getZ()))
			resetBlock();
		else if(mc.theWorld.getBlockId((int) currentBlock.getX(),
				(int) currentBlock.getY(), (int) currentBlock.getZ()) == 0) 
			currentBlock = null;
		else {
	        mc.getNetHandler().addToSendQueue(new Packet18Animation(mc.thePlayer, 1));
		}
	}

	public boolean canReachBlock(double x, double y, double z) {
		double distance = mc.theClient.getClientUtils().getDistance(x, y, z);
		return distance <= 3.8 && distance >= -3.8;
	}

	/**
	 * Simple delay, lol.
	 * */
	public void stopJonahFromTakingTheCookies(boolean creative) {
		nukerDelay = (creative ? (ModManager.getNoCheatMode().isState() ? (int) (5 - creativeSpeed.getValue()) : 0): 5);
	}

	public void resetBlock() {
		if(currentBlock != null) {
			nigger(1);
			this.currentBlock = null;
		}
 	}
	
	public boolean fakeRotation() {
		return currentBlock != null;
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
				EventMovement mEvent = (EventMovement)event;
			if(mEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				
				preServerUpdate(mc.thePlayer);
				
			} else if(mEvent.getType().equals(EventMovement.EventType.POST_UPDATE)) {
				
				postServerUpdate(mc.thePlayer);
				
			}
		} else if(event instanceof EventBlockInteract) {
			EventBlockInteract iEvent = (EventBlockInteract)event;
			if(iEvent.getType().equals(EventBlockInteract.EventType.RIGHT_CLICK)) {
				
				if (iEvent.getBlock() != null) {
					selectedID = mc.theWorld.getBlockId((int) iEvent.getBlockCoords().getX(), (int) iEvent.getBlockCoords().getY(), (int) iEvent.getBlockCoords().getZ());
					ItemStack i = new ItemStack(iEvent.getBlock());
					mc.theClient.addChatMessage("Now nuking '" + i.getDisplayName() + "' (" + selectedID + ")");
				}
			}
		} else if(event instanceof EventServerUpdate) {
			EventServerUpdate rEvent = (EventServerUpdate) event;
			if(fakeRotation()) {
				float[] rotations = mc.theClient.getClientUtils().getRotation(currentBlock.getX() + 0.5, currentBlock.getY() + 0.5, currentBlock.getZ() + 0.5);
				rEvent.setRotationYaw(rotations[0]);
				rEvent.setRotationPitch(rotations[1]);
			}
		} else if(event instanceof EventRender3D) {
			if(currentBlock != null) {
				double renderX = currentBlock.getX() - RenderManager.renderPosX;
				double renderY = currentBlock.getY() - RenderManager.renderPosY;
				double renderZ = currentBlock.getZ() - RenderManager.renderPosZ;
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				glColor3d(1 - currentBlockDamage, currentBlockDamage, 0D);
				mc.theClient.getRenderUtils().drawOutlinedBox(new AxisAlignedBB(0, 0, 0, 1, 1, 1));
				glColor4d(1 - currentBlockDamage, currentBlockDamage, 0D, 0.1D);
				mc.theClient.getRenderUtils().drawBox(new AxisAlignedBB(0, 0, 0, 1, 1, 1));
				glPopMatrix();
			}
		}
	}

	@Override
	public void onToggle() {
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);
		eventHandler.registerListener(EventServerUpdate.class, this);
		eventHandler.registerListener(EventBlockInteract.class, this);
		eventHandler.registerListener(EventRender3D.class, this);
		if(speedmine == null)
			speedmine = (Speedmine)ModManager.getMod("Speedmine");
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
		eventHandler.unRegisterListener(EventServerUpdate.class, this);
		eventHandler.unRegisterListener(EventBlockInteract.class, this);
		eventHandler.unRegisterListener(EventRender3D.class, this);

	}
}