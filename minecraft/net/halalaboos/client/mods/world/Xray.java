package net.halalaboos.client.mods.world;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRenderBlock;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.files.XrayFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;

public class Xray extends Mod implements Listener {

	public Xray() {
		super("Xray", Keyboard.KEY_X);
		setCategory(WindowCategory.WORLD);
		setType(Type.WORLD);
		setRenderIngame(false);
		setDescription("X-ray vision maaan!");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRenderBlock) {
			EventRenderBlock rEvent = (EventRenderBlock) event;
			if(rEvent.getType().equals(EventRenderBlock.EventType.RENDER_ALL_FACES)) {
				rEvent.setCancelled(isState() ? XrayFile.contains(rEvent.getBlock().blockID) : false);
			
			} else if(rEvent.getType().equals(EventRenderBlock.EventType.RENDER_TYPE)) {
				rEvent.setCancelled(isState() ? !XrayFile.contains(rEvent.getBlock().blockID) : false);

			} else if(rEvent.getType().equals(EventRenderBlock.EventType.RENDER_AS_NORMAL)) {
				rEvent.setCancelled(isState() ? !XrayFile.contains(rEvent.getBlock().blockID) : false);
			}
		} else if(event instanceof EventTick) {
			mc.gameSettings.gammaSetting = 10F;
		}
	}

	private float oldGamma;
	
	@Override
	public void onToggle() {
		mc.renderGlobal.loadRenderers();
		if(isState()) {
			oldGamma = mc.gameSettings.gammaSetting;
			mc.gameSettings.gammaSetting = 10F;
		} else 
			mc.gameSettings.gammaSetting = oldGamma;

	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRenderBlock.class, this);
		eventHandler.registerListener(EventTick.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRenderBlock.class, this);
		eventHandler.unRegisterListener(EventTick.class, this);

	}

}
