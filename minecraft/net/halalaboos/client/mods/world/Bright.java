package net.halalaboos.client.mods.world;

import javax.imageio.ImageIO;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.ClientUtils;
import net.halalaboos.client.utils.Counter;

public class Bright extends Mod implements Listener {

	private long lastBright;
	private float oldGamma = 0;
	private Counter counter = new Counter(15L, 0);
	public Bright() {
		super("Bright", Keyboard.KEY_C);
		setCategory(WindowCategory.WORLD);
		setType(Type.WORLD);
		setDescription("Brightens up the world");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventTick) {
			counter.count();
			if(counter.getCount() > 10) {
				mc.gameSettings.gammaSetting = 10;
				eventHandler.unRegisterListener(EventTick.class, this);
				counter.reset();
				return;
			}
			mc.gameSettings.gammaSetting = counter.getCount();
		}
	}

	@Override
	public void onToggle() {
		if(isState())
			oldGamma = mc.gameSettings.gammaSetting;
		else
			mc.gameSettings.gammaSetting = oldGamma;
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventTick.class, this);
		
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventTick.class, this);
		
	}

}
