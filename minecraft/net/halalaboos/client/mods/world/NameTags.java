package net.halalaboos.client.mods.world;

import org.lwjgl.input.Keyboard;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRecieveMessage;
import net.halalaboos.client.event.events.EventRenderNamePlate;
import net.halalaboos.client.event.events.EventSendMessage;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.EntityPlayer;
import static org.lwjgl.opengl.GL11.*;


public class NameTags extends Mod implements Listener {

	public NameTags() {
		super("NameTags", Keyboard.KEY_P);
		setCategory(WindowCategory.WORLD);
		setType(Type.WORLD);
		setDescription("Enlarges name tags");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRenderNamePlate) {
			EventRenderNamePlate rEvent = (EventRenderNamePlate) event;
			double distance = mc.theClient.getClientUtils().getDistance(rEvent.getX(), rEvent.getY(), rEvent.getZ());
			if((distance / 6) > 1 && rEvent.getEntityLiving() instanceof EntityPlayer) {
				glScaled((distance / 6), (distance / 6), (distance / 6));
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRenderNamePlate.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRenderNamePlate.class, this);
	}
}
