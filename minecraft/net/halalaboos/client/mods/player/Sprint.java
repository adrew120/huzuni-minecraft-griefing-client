package net.halalaboos.client.mods.player;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.mods.pvp.KillAura;
import net.halalaboos.client.ui.window.WindowCategory;

public class Sprint extends Mod implements Listener {
		
	public Sprint() {
		super("Sprint", Keyboard.KEY_M);
		setCategory(WindowCategory.PLAYER);
		setType(Type.MOVEMENT);
		setDescription("Automatically sprint");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventTick) {
			if(canSprint())
				mc.thePlayer.setSprinting(true);
		}
	}

	public boolean canSprint() {
		return !mc.thePlayer.isSneaking() 
		&& mc.thePlayer.movementInput.moveForward > 0
		&& mc.thePlayer.getFoodStats().getFoodLevel() > 6
		&& !mc.thePlayer.isCollidedHorizontally;
	}
	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventTick.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventTick.class, this);
		
	}

}
