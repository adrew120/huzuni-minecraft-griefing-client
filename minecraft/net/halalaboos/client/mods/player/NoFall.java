package net.halalaboos.client.mods.player;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventRecievePacket;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.Packet10Flying;

public class NoFall extends Mod implements Listener {

	public NoFall() {
		super("NoFall", Keyboard.KEY_N);
		setCategory(WindowCategory.PLAYER);
		setType(Type.MOVEMENT);
		setDescription("No fall damage");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement mEvent = (EventMovement) event;
			if(mEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				if(mc.thePlayer.fallDistance > 3)
					mc.thePlayer.onGround = true;
			} else if(mEvent.getType().equals(EventMovement.EventType.POST_UPDATE)) {
				if(mc.thePlayer.fallDistance > 3)
					mc.thePlayer.onGround = false;
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
		
	}

}
