package net.halalaboos.client.mods.player;

import net.halalaboos.client.base.Value;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventBlockInteract;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;

public class FastPlace extends Mod implements Listener {

	private Value speed;

	public FastPlace() {
		super("Fast Place", -1);
		speed = new Value("FastPlace Speed", 4f, 1f, 4, 1f);
		ValueManager.add(speed);
		setCategory(WindowCategory.PLAYER);
		setType(Type.PLAYER);
		setDescription("Place blocks faster.");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventBlockInteract) {
			EventBlockInteract iEvent = (EventBlockInteract) event;
			if(iEvent.getType().equals(EventBlockInteract.EventType.RIGHT_CLICK)) {
				mc.rightClickDelayTimer = (int) (4 - (speed.getValue()));
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventBlockInteract.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventBlockInteract.class, this);
		
	}

}
