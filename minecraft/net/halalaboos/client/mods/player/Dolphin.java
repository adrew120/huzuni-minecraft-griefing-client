package net.halalaboos.client.mods.player;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.MathHelper;
import org.lwjgl.input.Keyboard;

public class Dolphin extends Mod implements Listener {

	public Dolphin() {
		super("Dolphin", Keyboard.KEY_J);
		setCategory(WindowCategory.PLAYER);
		setType(Type.FLUID);
		setDescription("Swim like a dolphin");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement pEvent = (EventMovement)event;
			if(pEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				if (mc.thePlayer.handleWaterMovement()) {
					mc.thePlayer.setSprinting(true);
					mc.thePlayer.motionY += 0.04;
					mc.thePlayer.motionX = mc.thePlayer.motionX*1.02;
					mc.thePlayer.motionZ = mc.thePlayer.motionZ*1.02;
				}
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);
		
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
		
	}
}
