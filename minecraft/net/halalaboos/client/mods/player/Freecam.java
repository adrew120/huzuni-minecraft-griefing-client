package net.halalaboos.client.mods.player;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventServerUpdate;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.*;

public class Freecam extends Mod implements Listener {
	private EntityOtherPlayerMP fakePlayer;

	public Freecam() {
		super("Freecam", Keyboard.KEY_U);
		setCategory(WindowCategory.PLAYER);
		setType(Type.PLAYER);
		setDescription("Fly like a G6 through walls");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement mEvent = (EventMovement) event;
			if(mEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				mEvent.setCancelled(true);
			}
		} else if(event instanceof EventTick){
			mc.thePlayer.capabilities.isFlying = true;
			mc.thePlayer.noClip = true;
		}
	}

	@Override
	public void onToggle() {
		if(isState()) {
			fakePlayer = new EntityOtherPlayerMP(mc.theWorld, "\2479" + SettingsFile.getUsername());
			fakePlayer.copyDataFrom(mc.thePlayer, true);
			fakePlayer.setPositionAndRotation(mc.thePlayer.posX, mc.thePlayer.posY - 1.5, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
			fakePlayer.rotationYawHead = mc.thePlayer.rotationYawHead;
			mc.theWorld.addEntityToWorld(-69, fakePlayer);
			mc.thePlayer.capabilities.isFlying = true;
			mc.thePlayer.noClip = true;
		} else {
			mc.thePlayer.setPositionAndRotation(fakePlayer.posX, fakePlayer.posY + 1.5, fakePlayer.posZ, fakePlayer.rotationYaw, fakePlayer.rotationPitch);
			mc.thePlayer.noClip = false;
			mc.theWorld.removeEntityFromWorld(-69);
			mc.thePlayer.capabilities.isFlying = false;
		}
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);
		eventHandler.registerListener(EventTick.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
		eventHandler.unRegisterListener(EventTick.class, this);

	}

}
