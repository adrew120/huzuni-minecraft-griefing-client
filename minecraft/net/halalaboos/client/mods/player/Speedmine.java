package net.halalaboos.client.mods.player;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.base.Value;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventBlockInteract;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;

public class Speedmine extends Mod implements Listener {

	private Value speed;
	private Value delay;

	public Speedmine() {
		super("Speedmine", Keyboard.KEY_V);
		speed = new Value("Mine Speed", 1.3f, 1f, 2, 0.1f);
		delay = new Value("Mine Delay", 3f, 0f, 5, 1f);
		ValueManager.add(speed);
		ValueManager.add(delay);
		setCategory(WindowCategory.PLAYER);
		setType(Type.MINING);
		setDescription("Mine stuff better");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventBlockInteract) {
			EventBlockInteract bEvent = (EventBlockInteract)event;
			int x = (int) bEvent.getBlockCoords().getX();
			int y = (int) bEvent.getBlockCoords().getY();
			int z = (int) bEvent.getBlockCoords().getZ();
			if(bEvent.getType().equals(EventBlockInteract.EventType.HURT)) {
				if(bEvent.getBlock() != null)
					mc.playerController.curBlockDamageMP += bEvent.getBlock().getPlayerRelativeBlockHardness(this.mc.thePlayer, this.mc.thePlayer.worldObj, x, y, z) * (speed.getValue() - 1);
			}
			bEvent.setBlockHitDelay((int)delay.getValue());
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventBlockInteract.class, this);
		
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventBlockInteract.class, this);
		
	}

	public Value getSpeed() {
		return speed;
	}

}
