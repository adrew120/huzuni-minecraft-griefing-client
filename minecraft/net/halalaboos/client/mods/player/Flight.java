package net.halalaboos.client.mods.player;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.base.Value;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;

public class Flight extends Mod implements Listener {

	private Value speed;

	public Flight() {
		super("Flight", Keyboard.KEY_F);
		setCategory(WindowCategory.PLAYER);
		setType(Type.MOVEMENT);
		setDescription("Fly like a G6");
		speed = new Value("Fly Speed", 1f, 1f, 10, 0.5f);
		ValueManager.add(speed);
		
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement mEvent = (EventMovement) event;
			if(mEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				mc.thePlayer.capabilities.isFlying = true;				
				if(mc.thePlayer.fallDistance > 3)
					mc.thePlayer.onGround = true;
			} else if(mEvent.getType().equals(EventMovement.EventType.MOVE_ENTITY)) {
				mEvent.setMotionX(mEvent.getMotionX() * speed.getValue());
				mEvent.setMotionY(mEvent.getMotionY() * speed.getValue());	
				mEvent.setMotionZ(mEvent.getMotionZ() * speed.getValue());	
				if(mc.thePlayer.fallDistance > 3)
					mc.thePlayer.onGround = false;
			}
		}
	}

	@Override
	public void onToggle() {
		if(isState())
			mc.thePlayer.capabilities.isFlying = true;
		else
			mc.thePlayer.capabilities.isFlying = false;
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);

	}

}
