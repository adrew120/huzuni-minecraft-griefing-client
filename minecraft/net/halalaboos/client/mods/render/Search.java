package net.halalaboos.client.mods.render;

import java.awt.Color;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.event.events.EventRenderBlock;
import net.halalaboos.client.event.events.EventRenderBlock.EventType;
import net.halalaboos.client.files.SearchFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.Coordinates;
import net.halalaboos.client.utils.RenderUtils;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.RenderManager;
import static org.lwjgl.opengl.GL11.*;

public class Search extends Mod implements Listener {

	private List<EventRenderBlock> blocksList = new CopyOnWriteArrayList<EventRenderBlock>();

	public Search() {
		super("Search", -1);
		setCategory(WindowCategory.RENDER);
		setRenderIngame(false);
	}
	
	@Override
	public void onToggle() {
		blocksList.clear();
		if(isState())
			mc.renderGlobal.loadRenderers();
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRenderBlock.class, this);
		eventHandler.registerListener(EventRender3D.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRenderBlock.class, this);
		eventHandler.unRegisterListener(EventRender3D.class, this);
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRenderBlock) {
			EventRenderBlock rEvent = (EventRenderBlock) event;
			if(rEvent.getType().equals(EventType.RENDER_TYPE)) {
				if(rEvent.getBlock() != null) {
					if(SearchFile.getBlockIDs().contains(rEvent.getBlock().blockID)) {
						for(EventRenderBlock loadedRenderEvent : blocksList) {
							if(rEvent.getBlockCoordinates().compare(loadedRenderEvent.getBlockCoordinates()))
								return;
						}
						blocksList.add(rEvent);
					}
				}
			}
		} else if(event instanceof EventRender3D) {
			nigger();
		}
	}
	/**
	 * Kinky's amazing, ingenious, awesome, rad method naming.
	 * Thank kinky for this fucking method. Atleast it's name.
	 * I wrote everything in it. He actually watched.
	 * Ya
	 * @author iKinky
	 * @return null
	 * */
	public void nigger() {
		for(EventRenderBlock rEvent : blocksList) {
			Coordinates coordinates = rEvent.getBlockCoordinates();
			if(!SearchFile.getBlockIDs().contains(rEvent.getBlock().blockID) || mc.theWorld.getBlockId((int) coordinates.getX(), (int) coordinates.getY(), (int) coordinates.getZ()) == 0) {
				blocksList.remove(rEvent);
				continue;
			}
			RenderUtils rUtils = mc.theClient.getRenderUtils();
			glPushMatrix();
				glTranslated(coordinates.getX() - RenderManager.renderPosX, coordinates.getY() - RenderManager.renderPosY, coordinates.getZ() - RenderManager.renderPosZ);
				rUtils.drawCrossedBox(new AxisAlignedBB(0, 0, 0, 1, 1, 1), (float) 0.0F, 0.3F, 0.75F, false);
			glPopMatrix();
		}
	}

}
