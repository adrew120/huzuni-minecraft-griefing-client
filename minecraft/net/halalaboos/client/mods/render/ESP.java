package net.halalaboos.client.mods.render;


import org.lwjgl.input.Keyboard;
import org.lwjgl.util.glu.Cylinder;
import org.lwjgl.util.glu.GLU;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.files.FriendsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.RenderManager;
import static org.lwjgl.opengl.GL11.*;

public class ESP extends Mod implements Listener {
	Cylinder cylinder = new Cylinder();
	float r,g,b = 0;

	public ESP() {
		super("ESP", Keyboard.KEY_DELETE, false);
		setCategory(WindowCategory.RENDER);
		setType(Type.RENDER);
		setDescription("Draw rectangle around players.");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender3D) {
			for(Object o : mc.theWorld.playerEntities) {
				EntityPlayer player = (EntityPlayer) o;
				if(player == mc.thePlayer || player.isDead)
					continue;
				else {
					double renderX = player.posX - RenderManager.renderPosX;
					double renderY = player.posY - RenderManager.renderPosY;
					double renderZ = player.posZ - RenderManager.renderPosZ;
					if(FriendsFile.contains(player.username)){
						r = 0;
						g = 0.5f;
						b = 0.75f;
					}else {
						float distance = mc.thePlayer.getDistanceToEntity(player);
						r = 1 - (distance) / 32F;
						g = (distance / 32F);
						b = 0f;
					}
					glPushMatrix();
					glTranslated(renderX, renderY, renderZ);
					glRotatef(player.rotationYaw, 0.0F, player.height, 0.0F);
					double width = player.width / 1.5f;
					mc.theClient.getRenderUtils().drawCrossedBox(new AxisAlignedBB(-width, 0, -width, width, 
							player.height, width), r, g, b, false);
					glPopMatrix();
				}
			}
		}
	}

	@Override
	public void onToggle() {		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRender3D.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRender3D.class, this);
		
	}

}
