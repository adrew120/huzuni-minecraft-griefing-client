package net.halalaboos.client.mods.render;


import org.lwjgl.input.Keyboard;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.files.FriendsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.RenderManager;
import static org.lwjgl.opengl.GL11.*;

public class Tracer extends Mod implements Listener {

	public Tracer() {
		super("Tracer", Keyboard.KEY_B, false);
		setCategory(WindowCategory.RENDER);
		setType(Type.RENDER);
		setDescription("Renders line to players");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender3D) {
			for(Object o : mc.theWorld.playerEntities) {
				EntityPlayer player = (EntityPlayer) o;
				if(player == mc.thePlayer || player.isDead)
					continue;
				else {
					double renderX = player.posX - RenderManager.renderPosX;
					double renderY = player.posY - RenderManager.renderPosY;
					double renderZ = player.posZ - RenderManager.renderPosZ;
					if(FriendsFile.contains(player.username))
						glColor3f(0, 0.5f, 0.75f);
					else {
						float distance = mc.thePlayer.getDistanceToEntity(player);
						glColor3f(1 - (distance) / 32F, (distance / 32F), 0f);
					}
					glBegin(GL_LINES);
					glVertex3d(0, 0, 0);
					glVertex3d(renderX, renderY, renderZ);
					glEnd();
				}
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRender3D.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRender3D.class, this);
		
	}

}
