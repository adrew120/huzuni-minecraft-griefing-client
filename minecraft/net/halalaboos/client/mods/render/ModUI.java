package net.halalaboos.client.mods.render;

import java.awt.Color;
import java.util.Random;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.base.Binding;
import net.halalaboos.client.base.Value;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.manager.WindowManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.screen.PanelScreen;
import net.halalaboos.client.ui.tabbed.TabbedController;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.ClientUtils;
import net.halalaboos.client.utils.Texture;
import net.halalaboos.client.utils.TextureUtils;

public class ModUI extends Mod implements Listener {
		
	public ModUI() {
		super("GUI", Keyboard.KEY_RSHIFT, false);
		this.eventHandler.registerListener(EventRender.class, this);
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender) {
			
			if(isState()) {
				int yPos = 12;
				mc.fontRenderer.drawStringWithShadow("\247nMods", 2, yPos, 0xffffff);
				yPos += 11;
				for(Mod m : ModManager.getMods()) {
					if(m.isRenderIngame()) {
						mc.fontRenderer.drawStringWithShadow(m.getName() + ": " + m.getKeyBind().getKeyName(), 2, yPos, m.isState() ? 0x339900:0x990000);
						yPos += 11;
					}
				}
				mc.fontRenderer.drawStringWithShadow("\247nValues", 2, yPos, 0xffffff);
				yPos += 11;
				for(Value v : ValueManager.getValues()) {
					mc.fontRenderer.drawStringWithShadow(v.getName() + ": " + v.getValue(), 2, yPos, 0xffff33);
					yPos += 11;
				}
			}
			
			WindowManager.getModWindows().renderWindowsIngame(-1, -1);
		}
		
	}

	@Override
	public void onToggle() {
		mc.displayGuiScreen(new PanelScreen());
		this.setState(!isState());
	}

	@Override
	public void onEnable() {
		
	}

	@Override
	public void onDisable() {
		
	}

}
