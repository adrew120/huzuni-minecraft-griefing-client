package net.halalaboos.client.mods.render;

import java.util.*;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.Coordinates;
import net.minecraft.src.*;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.*;

public class History extends Mod implements Listener {

	private static Session history;
	
	public History() {
		super("History", Keyboard.KEY_Y);
		history = new Session();
		setCategory(WindowCategory.RENDER);
		setType(Type.RENDER);
		setDescription("Shows log of where you've been");

		eventHandler.registerListener(EventTick.class, this);
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender3D) {
			history.renderLine(0, 0, 0);
		}else if(event instanceof EventTick) {
			history.onTick();
		}
		
	}

	private class Crumb extends Coordinates {

		private String server;

		public Crumb(double x, double y, double z, String s) {
			super(x, y, z);
			server = s;
			// TODO Auto-generated constructor stub
		}

		public String getServer() {
			return server;
		}

	}

	public class Session {

		public ArrayList<Crumb> coords;
		private double lastX, lastY, lastZ;
		private boolean trail;
		private float r, g, b;

		public Session() {
			coords = new ArrayList<Crumb>();
			trail = true;
			Random ra = new Random();
			r = (float) (ra.nextInt(255) + 1) / 255f;
			g = (float) (ra.nextInt(255) + 1) / 255f;
			b = (float) (ra.nextInt(255) + 1) / 255f;
		}

		public void onTick() {
			if (mc.thePlayer == null || !trail)
				return;
			boolean changedX = lastX - mc.thePlayer.posX != 0;
			boolean changedY = lastY - mc.thePlayer.posY != 0;
			boolean changedZ = lastZ - mc.thePlayer.posZ != 0;

			if (changedX || changedY || changedZ) {
				lastX = (double) mc.thePlayer.posX;
				lastZ = (double) mc.thePlayer.posZ;
				lastY = (double) mc.thePlayer.posY;

				coords.add(new Crumb(mc.thePlayer.posX, mc.thePlayer.posY,
						mc.thePlayer.posZ, SettingsFile.getServerIP()));
			}
		}

		public void stopTrail() {
			trail = false;
		}

		public void renderLine(float xOffset, float yOffset, float zOffset) {
			GL11.glColor3f(r, g, b);
			GL11.glLineWidth(1.5F);
			GL11.glBegin(GL11.GL_LINE_STRIP);

			for (Crumb c : coords) {
				if (mc.theClient.getClientUtils().getDistance(c.getX(), c.getY(),
						c.getZ()) > 50
						|| !c.getServer().equalsIgnoreCase(SettingsFile.getServerIP()))
					continue;
				double xx = (double) (c.getX() - RenderManager.instance.renderPosX);
				double yy = (double) (c.getY() - RenderManager.instance.renderPosY);
				double zz = (double) (c.getZ() - RenderManager.instance.renderPosZ);
				GL11.glVertex3d(xx + xOffset, yy - (1.5) + yOffset, zz
						+ zOffset);
			}
			GL11.glEnd();
		}
	}

	@Override
	public void onEnable() {
		Random ra = new Random();
		history.r = (float) (ra.nextInt(255) + 1) / 255f;
		history.g = (float) (ra.nextInt(255) + 1) / 255f;
		history.b = (float) (ra.nextInt(255) + 1) / 255f;
		eventHandler.registerListener(EventRender3D.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRender3D.class, this);
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}
}