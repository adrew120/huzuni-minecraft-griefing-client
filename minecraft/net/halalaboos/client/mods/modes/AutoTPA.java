package net.halalaboos.client.mods.modes;

import net.halalaboos.client.base.Friend;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRecieveMessage;
import net.halalaboos.client.files.FriendsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.Packet3Chat;
import net.minecraft.src.StringUtils;
import org.lwjgl.input.Keyboard;
/*
 * Automatically accepts teleport requests if they're on your friends list.
 */
public class AutoTPA extends Mod implements Listener {

	public AutoTPA() {
		super("Friend TPAccept", -1, false);
		setCategory(WindowCategory.MODES);
		setDescription("Automatically accept TPA requests from friends.");
		setState(true);
		
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRecieveMessage) {
			EventRecieveMessage mEvent = (EventRecieveMessage)event;
			if(mEvent.getMessage().contains("has requested to teleport to you")){
	            for(Friend friend : FriendsFile.getFriends()){
	            	if(mEvent.getMessage().toLowerCase().contains(friend.getUsername().toLowerCase()) ||
	            			mEvent.getMessage().toLowerCase().contains(friend.getAlias().toLowerCase())) {
	            		mc.thePlayer.sendChatMessage("/tpaccept");
	            		return;
	            	}
	            }
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRecieveMessage.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRecieveMessage.class, this);
		
	}

}
