package net.halalaboos.client.mods.pvp;

import org.lwjgl.input.*;
import net.halalaboos.client.base.Value;
import net.halalaboos.client.event.*;
import net.halalaboos.client.event.events.*;
import net.halalaboos.client.files.FriendsFile;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.SideMod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.ClientUtils;
import net.minecraft.src.*;

public class KillAura extends Mod implements Listener {

	private Entity currentEntity;
	private long lastHitMS = 0;
	private Value speed;
	private SideMod players, mobs, animals, aimbot;
	
	public KillAura() {
		super("Kill Aura", Keyboard.KEY_R);
		setType(Type.PVP);
		setDescription("Kills entities around you.");
		speed = new Value("Kill Aura Speed", 11, 5, 15, 0.1f);
		ValueManager.add(speed);
		
		players = new SideMod("Players");
		ModManager.getMods().add(players);
		players.setState(true);
		
		mobs = new SideMod("Mobs");
		ModManager.getMods().add(mobs);
		
		animals = new SideMod("Animals");
		ModManager.getMods().add(animals);
		
		aimbot = new SideMod("Aimbot");
		aimbot.setDescription("Looks at the entity when using kill aura.");
		ModManager.getMods().add(aimbot);
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement mEvent = (EventMovement) event;
			if(mEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				preUpdate();
			} else if(mEvent.getType().equals(EventMovement.EventType.POST_UPDATE)) {
				postUpdate();
			}
		} else if(event instanceof EventServerUpdate) {
			EventServerUpdate rEvent = (EventServerUpdate) event;
			if(fakeRotation()) {
				float[] rotations = mc.theClient.getClientUtils().getRotation(currentEntity.posX, currentEntity.posY + currentEntity.getEyeHeight(), currentEntity.posZ);
				if(aimbot.isState()) {
					mc.thePlayer.rotationYaw = rotations[0];
					mc.thePlayer.rotationPitch = rotations[1];
				}
				rEvent.setRotationYaw(rotations[0]);
				rEvent.setRotationPitch(rotations[1]);
			}
		}
	}

	public void preUpdate() {
		if(!isHittableNotNull(currentEntity, true))
			currentEntity = findEntity();
		
	}
	
	public void postUpdate() {
		if(ClientUtils.getSystemTime() - lastHitMS >= (1000F / speed.getValue())) {
			if(isHittableNotNull(currentEntity, false)) {
				mc.thePlayer.swingItem();
				mc.playerController.attackEntity(mc.thePlayer, currentEntity);
				lastHitMS = ClientUtils.getSystemTime();
				currentEntity = null;
			}
		}
	}
	
	public Entity findEntity() {
		Entity closest = null;
		for(Object o : mc.theWorld.loadedEntityList) {
			Entity entity = (Entity) o;
			if(isHittableNotNull(entity, true) && isOnList(entity)) {
				if(closest == null)
					closest = entity;
				else {
					if(mc.thePlayer.getDistanceToEntity(entity) < mc.thePlayer.getDistanceToEntity(closest))
						closest = entity;
				}
			}
		}
		return closest;
	}
	
	public boolean fakeRotation() {
		return isHittableNotNull(currentEntity, true) && isState();
	}
	
	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isHittableNotNull(Entity entity, boolean justLook) {
		if(!(entity instanceof EntityLiving))
			return false;
		EntityLiving entityLiving = (EntityLiving) entity;
		return (entity == null) ? false:(mc.thePlayer.getDistanceToEntity(entityLiving) < (justLook ? 6.5:4) && !entityLiving.isDead && entityLiving != mc.thePlayer && (entityLiving instanceof EntityPlayer ? !FriendsFile.contains(entityLiving.getEntityName()):true)) && entityLiving.getHealth() > 0 && mc.thePlayer.canEntityBeSeen(entityLiving);
	}
	
	public boolean isOnList(Entity entity) {
		if(entity instanceof EntityPlayer && players.isState())
			return true;
		
		if(entity instanceof EntityAnimal && animals.isState())
			return true;

		if(entity instanceof EntityMob && mobs.isState())
			return true;
		
		return false;
	}
	
	@Override
	public void onEnable() {
		eventHandler.registerListener(EventServerUpdate.class, this);
		eventHandler.registerListener(EventMovement.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventServerUpdate.class, this);
		eventHandler.unRegisterListener(EventMovement.class, this);
	}
	
}