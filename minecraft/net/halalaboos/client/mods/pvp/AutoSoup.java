package net.halalaboos.client.mods.pvp;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityItem;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Packet15Place;
import net.minecraft.src.Packet16BlockItemSwitch;
import net.minecraft.src.Slot;

public class AutoSoup extends Mod implements Listener {

	private boolean eatItem = false;
	private int oldItem;
	private Long cooldown = 69L;

	public AutoSoup() {
		super("AutoSoup", -1, false);
		setType(Type.PVP);
		setDescription("Automagically eats soups. Like a boss.");
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement mEvent = (EventMovement) event;
			if(mEvent.getType().equals(EventMovement.EventType.PRE_UPDATE))
				soup();
		}
	}
	
	public void soup() {

		if (mc.thePlayer.getHealth() > 14)
			return;
		
		if (!delay(80L)) {
			return;
		}

		if (eatItem) {
			swap(oldItem);
			eatItem = false;
			cooldown = mc.theClient.getClientUtils().getSystemTime();
			return;
		}

		int soupIndex = findSoup();
		if (soupIndex != -1 && !eatItem) {
			oldItem = mc.thePlayer.inventory.currentItem;
			swap(soupIndex);
			useItem(mc.thePlayer.inventory.getStackInSlot(soupIndex));
			cooldown = mc.theClient.getClientUtils().getSystemTime();
			eatItem = true;
			return;
		} else {
			int avSlot = availableSlot();
			int replacementSoup = findSoupInventory();

			if(replacementSoup != -1 && avSlot != -1) {
				ItemStack badItem = mc.thePlayer.inventory.getStackInSlot(avSlot);
				if(isShiftable(badItem)) {
					clickSlot(avSlot, true);
					clickSlot(replacementSoup, true);
				} else {
					clickSlot(replacementSoup, false);
					clickSlot(avSlot, false);
				}	
			}
		}
	
	}
	
	public int findSoup() {
		for (int o = 0; o < 9; o++) {
			ItemStack i = mc.thePlayer.inventory.getStackInSlot(o);
			if (i != null) {
				if (i.itemID == Item.bowlSoup.itemID)
					return o;
			}
		}
		return -1;
	}

	public int findSoupInventory() {
		for (int o = 9; o < 36; o++) {
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
				ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if(item != null) {
					if (item.itemID == Item.bowlSoup.itemID)
						return o;	
				}
			}
		}
		return -1;
	}

	public int availableSlot() {
		for (int o = 36; o < 45; o++) {
			ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
			if(item == null) 
					return o;	
			else {
				if(item.itemID == Item.bowlEmpty.itemID)
					return o;
			}
		}
		return -1;
	}
	
	public boolean isShiftable(ItemStack preferedItem) {
		if(preferedItem == null)
			return true;
		for (int o = 9; o < 36; o++) {
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
				ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if(item == null)
					return true;
				if(item.itemID == 0)
					mc.theClient.addChatMessage("0");
				else if(item.itemID == preferedItem.itemID) {
					if(item.stackSize + preferedItem.stackSize <= preferedItem.getMaxStackSize())
						return true;
				}
				
			} else
				return true;
		}
		return false;
	}

	public void swap(int soupIndex) {
		mc.getNetHandler()
				.addToSendQueue(new Packet16BlockItemSwitch(soupIndex));
	}

	public void useItem(ItemStack item) {
		mc.getNetHandler().addToSendQueue(
				new Packet15Place(-1, -1, -1, 255, item, 0.0F, 0.0F, 0.0F));
	}

	public void clickSlot(int slot, boolean shiftClick) {
		mc.playerController.windowClick(
				mc.thePlayer.inventoryContainer.windowId, slot, 0, shiftClick ? 1:0,
				mc.thePlayer);
	}

	public boolean delay(long d) {
		return (mc.theClient.getClientUtils().getSystemTime() - cooldown) > d;
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

}
