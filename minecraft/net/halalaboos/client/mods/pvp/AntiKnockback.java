package net.halalaboos.client.mods.pvp;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRecievePacket;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityItem;
import net.minecraft.src.Packet28EntityVelocity;
import net.minecraft.src.Packet30Entity;

public class AntiKnockback extends Mod implements Listener {

	public AntiKnockback() {
		super("AntiKnockback", -1, false);
		setType(Type.MOVEMENT);
		setDescription("Stops you from recieving knockback");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRecievePacket) {
			EventRecievePacket pEvent = (EventRecievePacket) event;
			if(pEvent.getPacket() instanceof Packet28EntityVelocity) {
				pEvent.setCancelled(true);
			}
		}
	}
	@Override
	public void onToggle() {

	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRecievePacket.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRecievePacket.class, this);
		
	}

}
