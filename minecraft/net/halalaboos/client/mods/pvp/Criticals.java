package net.halalaboos.client.mods.pvp;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventEntityInteract;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.*;

public class Criticals extends Mod implements Listener {

	public Criticals() {
		super("Criticals", -1, false);
		setType(Type.PVP);
		setDescription("Short, yet to the point hops to land criticals.");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventEntityInteract) {
			EventEntityInteract iEvent = (EventEntityInteract) event;
			if(iEvent.getEntity() instanceof EntityLiving && mc.thePlayer.onGround && !mc.thePlayer.isPotionActive(Potion.blindness) && !mc.thePlayer.isOnLadder() && !mc.thePlayer.isInWater()) {
				mc.thePlayer.motionY += 0.2f;
				mc.thePlayer.fallDistance = 0.46415937f;
				mc.thePlayer.onGround = false;
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventEntityInteract.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventEntityInteract.class, this);
		
	}

}
