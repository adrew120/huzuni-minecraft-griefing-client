package net.halalaboos.client.files;

import java.util.ArrayList;

import net.halalaboos.client.manager.WindowManager;
import net.halalaboos.client.ui.window.Window;

public class WindowFile extends HFile {

	public WindowFile() {
		super("Windows");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		for(String s : readFile()) {
			String s1[] = s.split(":");
			for(Window w : WindowManager.getModWindows().getWindows()) {
				if(w.getName().equalsIgnoreCase(s1[0])) {
					w.setX(Integer.parseInt(s1[1]));
					w.setY(Integer.parseInt(s1[2]));
					w.setWidth(Integer.parseInt(s1[3]));
					w.setHeight(Integer.parseInt(s1[4]));
					w.setClosed(Boolean.parseBoolean(s1[5]));
					w.formatPanel();
				}
			}
		}
	}

	@Override
	public void onEnd() {
		ArrayList<String> l = new ArrayList<String>();
		for (Window w : WindowManager.getModWindows().getWindows()) {
			l.add(new String(w.getName() + ":" + w.getX() + ":" + w.getY()
					+ ":" + w.getWidth() + ":" + w.getHeight() + ":" + w.isClosed()));
		}
		saveFile(l);
	}

}
