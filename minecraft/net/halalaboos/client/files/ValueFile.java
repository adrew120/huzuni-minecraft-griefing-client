package net.halalaboos.client.files;

import java.util.*;

import net.halalaboos.client.base.Value;
import net.halalaboos.client.manager.ValueManager;

public class ValueFile extends HFile {

	public ValueFile() {
		super("Value");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		for(String s : readFile()) {
			for(Value v : ValueManager.getValues()) {
				if(s.startsWith(v.getName())) {
					v.setValue(Float.parseFloat(s.split(":")[1]));
				}
			}
		}
	}

	@Override
	public void onEnd() {
		List<String> list = new ArrayList<String>();
		for(Value v : ValueManager.getValues())
			list.add(v.getName() + ":" + v.getValue());
		saveFile(list);
	}

}
