package net.halalaboos.client.files;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.base.Waypoint;
import net.halalaboos.client.manager.WaypointManager;

public class WaypointFile extends HFile {

	public WaypointFile() {
		super("Waypoints");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		for(String s : this.readFile()) {
			String[] args = s.split(":");
			WaypointManager.add(args[0], Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]), args[4], Integer.parseInt(args[5]));
		}
	}

	@Override
	public void onEnd() {
		List<String> list = new ArrayList<String>();
		for(Waypoint waypoint : WaypointManager.getWaypoints()) {
			list.add(waypoint.getName() + ":" + waypoint.getCoordinates().getX() + ":" + waypoint.getCoordinates().getY() + ":" + waypoint.getCoordinates().getZ() + ":" + waypoint.getServer() + ":" + waypoint.getPort());
		}
		saveFile(list);
	}

}
