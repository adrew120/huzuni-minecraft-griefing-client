package net.halalaboos.client.files;

import java.util.*;

import net.halalaboos.client.Client;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.ui.Gui.Type;

public class SettingsFile extends HFile {

	private static boolean firstTime = true;
	private static String username = "Default", serverIP = "127.0.0.1", huzuniServer = "127.0.0.1";
	private static int serverPort = 25565;
	public SettingsFile() {
		super("Settings");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		for(String s : readFile()) {
			String s1[] = s.split(":");
			if(s1[0].equalsIgnoreCase("firstTime"))
				firstTime = Boolean.parseBoolean(s1[1].toUpperCase());
			else if(s1[0].equalsIgnoreCase("username"))
				username = s1[1];
			else if(s1[0].equalsIgnoreCase("style")) {
				for(Type type : Type.values()) {
					if(type.getName().equalsIgnoreCase(s1[1])) {
						GuiManager.selectGUIStyle(type);
					}
				}
			} else if(s1[0].equalsIgnoreCase("huzuni")) {
				if(s1.length > 2)
					huzuniServer = s1[1] + ":" + s1[2];
				else
					huzuniServer = s1[1];
			}
			
		}
	}

	@Override
	public void onEnd() {
		List<String> list = new ArrayList<String>();
		list.add("Version:" + Client.CLIENT_VERSION);
		list.add("firstTime:FALSE");
		list.add("username:" + username);
		list.add("huzuni:" + huzuniServer);
		list.add("style:" + GuiManager.getSelectedGUI().getType().getName());

		saveFile(list);
	}

	public static boolean isFirstTime() {
		return firstTime;
	}

	public static void setFirstTime(boolean firstTime) {
		SettingsFile.firstTime = firstTime;
	}

	public static String getUsername() {
		return username;
	}

	public static void setUsername(String username) {
		SettingsFile.username = username;
	}

	public static String getServerIP() {
		return serverIP;
	}

	public static void setServerIP(String serverIP) {
		SettingsFile.serverIP = serverIP;
	}

	public static int getServerPort() {
		return serverPort;
	}

	public static void setServerPort(int serverPort) {
		SettingsFile.serverPort = serverPort;
	}

	public static String getHuzuniServer() {
		return huzuniServer;
	}

	public static void setHuzuniServer(String huzuniServer) {
		SettingsFile.huzuniServer = huzuniServer;
	}

}
