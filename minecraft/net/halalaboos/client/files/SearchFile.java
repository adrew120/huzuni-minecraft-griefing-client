package net.halalaboos.client.files;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SearchFile extends HFile{

	private static List<Integer> blockIDs = new CopyOnWriteArrayList<Integer>();
	
	public SearchFile() {
		super("Search");
	}

	public static boolean add(int id) {
		for(Integer i : blockIDs) {
			if(i == id)
				return false;
		}
		blockIDs.add(new Integer(id));
		return true;
	}
	
	public static boolean remove(int id) {
		for(int i = 0; i < blockIDs.size(); i++) {
			if(blockIDs.get(i) == id) {
				blockIDs.remove(i);
				return true;
			}
		}
		return false;
	}
	
	public static boolean contains(int id) {
		for(Integer i : blockIDs) {
			if(i == id)
				return true;
		}
		return false;
	}
	
	@Override
	public void onStart() {
		blockIDs.add(56);
		for(String s : readFile()) {
			blockIDs.add(Integer.parseInt(s));
		}
	}

	@Override
	public void onEnd() {
		List<String> tempList = new ArrayList<String>();
		for(int blockID : blockIDs) {
			tempList.add("" + blockID);
		}
		this.saveFile(tempList);
	}

	public static List<Integer> getBlockIDs() {
		return SearchFile.blockIDs;
	}
	
}
