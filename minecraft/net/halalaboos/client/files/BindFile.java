package net.halalaboos.client.files;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.Client;
import net.halalaboos.client.base.BindCommand;
import net.halalaboos.client.base.Binding;
import net.halalaboos.client.base.Friend;
import net.halalaboos.client.event.events.EventKeyPressed;

public class BindFile extends HFile {
	
	private static List<Binding> binds = new ArrayList<Binding>();
	public BindFile() {
		super("Binds");
		// TODO Auto-generated constructor stub
	}

	public static void add(BindCommand bind) {
		for(Binding b : binds) {
			if(b instanceof BindCommand) {
				if(b.getKeyCode() == bind.getKeyCode()) {
					b.setName(bind.getName());
					((BindCommand)b).setCommand(bind.getName());
					return;
				}
			}
		}
		binds.add(bind);
		Client.getEventHandler().registerListener(EventKeyPressed.class, bind);
	}
	
	@Override
	public void onStart() {
		for(String s : readFile()) {
			String s1[] = s.split(":");
			BindCommand bind = new BindCommand(s1[0], s.split(":")[2].equals("-1") ? -1:Keyboard.getKeyIndex(s.split(":")[2]));
			add(bind);
		}
	}

	@Override
	public void onEnd() {
		List<String> words = new ArrayList<String>();
		for(Binding bind : binds)
			words.add(bind.getName() + ":" + bind.getKeyName());
		saveFile(words);
	}

	public static List<Binding> getBinds() {
		return binds;
	}

	public static void setBinds(List<Binding> binds) {
		BindFile.binds = binds;
	}

}
