package net.halalaboos.client.files;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.manager.*;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.*;

public class ModFile extends HFile {

	public ModFile() {
		super("Mod");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		for(String s : readFile()) {
			for(Mod m : ModManager.getMods()) {
				if(s.startsWith(m.getName())) {
					m.setKey(s.split(":")[2].equals("-1") ? -1:Keyboard.getKeyIndex(s.split(":")[2]));
					m.setState(Boolean.parseBoolean(s.split(":")[1]));
				}
			}
		}
	}

	@Override
	public void onEnd() {
		List<String> list = new ArrayList<String>();
		for(Mod m : ModManager.getMods())
			list.add(m.getName() + ":" + m.isState() + ":" + m.getKeyName());
		saveFile(list);
	}
}
