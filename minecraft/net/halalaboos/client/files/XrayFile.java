package net.halalaboos.client.files;

import java.util.*;

import net.halalaboos.client.base.Value;
import net.halalaboos.client.manager.ValueManager;

public class XrayFile extends HFile {

	private static List<Integer> xray = new ArrayList<Integer>();
	
	public XrayFile() {
		super("Xray");
	}

	public static boolean add(int id) {
		for(Integer i : xray) {
			if(i == id)
				return false;
		}
		xray.add(new Integer(id));
		return true;
	}
	
	public static boolean remove(int id) {
		for(int i = 0; i < xray.size(); i++) {
			if(xray.get(i) == id) {
				xray.remove(i);
				return true;
			}
		}
		return false;
	}
	
	public static boolean contains(int id) {
		for(Integer i : xray) {
			if(i == id)
				return true;
		}
		return false;
	}
	
	@Override
	public void onStart() {
		xray.add(56);
		xray.add(8);
		xray.add(9);
		xray.add(10);
		xray.add(11);

		for(String s : readFile()) {
			add(Integer.parseInt(s));
		}
	}

	@Override
	public void onEnd() {
		List<String> list = new ArrayList<String>();
		for(int id : xray)
			list.add("" + id);
		saveFile(list);
	}

	public static List<Integer> getXray() {
		return xray;
	}

	public static void setXray(List<Integer> xray) {
		XrayFile.xray = xray;
	}

}
