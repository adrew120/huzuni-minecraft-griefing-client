package net.halalaboos.client.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;

import net.halalaboos.client.utils.Account;
import net.minecraft.src.StringUtils;

public class AccountFile extends HFile {

	private static ArrayList<Account> accounts = new ArrayList<Account>();
	public AccountFile() {
		super("Accounts.txt");
	}

	@Override
	public void onStart() {
		for (String s : this.readFile()) {
			try {
				String s1[] = s.split(":");
				accounts.add(new Account(s1[0], s1[1], true));
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void onEnd() {
		ArrayList<String> l = new ArrayList<String>();
		for (Account a : accounts) {
			if (a.isPrivate()) {
				String userPass = a.getUsername() + ":" + a.getPassword();
				l.add(userPass);
			}
		}
		this.saveFile(l);
	}

	public static boolean contains(Account account) {
		for(Account account2 : getAccounts()) {
			if(account.getUsername().equalsIgnoreCase(account2.getUsername()))
				return true;
		}
		return false;
	}
	
	public static boolean remove(String username) {
		for(Account account : getAccounts()) {
			if(account.getUsername().equalsIgnoreCase(username)) {
				getAccounts().remove(account);
				return true;
			}
		}
		return false;
	}
	
	public static ArrayList<Account> getAccounts() {
		return accounts;
	}

	public static void setAccounts(ArrayList<Account> accounts) {
		AccountFile.accounts = accounts;
	}

}
