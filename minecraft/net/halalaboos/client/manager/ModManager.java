package net.halalaboos.client.manager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.*;
import net.halalaboos.client.base.*;
import net.halalaboos.client.mods.*;
import net.halalaboos.client.mods.modes.*;
import net.halalaboos.client.mods.player.*;
import net.halalaboos.client.mods.pvp.*;
import net.halalaboos.client.mods.render.*;
import net.halalaboos.client.mods.world.*;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.GuiNewChat;

public class ModManager extends Manager{

	private static List<Mod> mods = new ArrayList<Mod>();
	private static Mod noCheatMode, ttfMode;

	public static List<Mod> getMods() {
		return mods;
	}

	public static void load(Mod mod) {
		if(getMod(mod.getName()) == null) {
			getMods().add(mod);
			System.out.println(mod.getName() + " loaded!");
		}
	}
	
	@Override
	public void onStartup() {

		noCheatMode = new SideMod("NoCheat");
		noCheatMode.setCategory(WindowCategory.MODES);
		noCheatMode.setState(true);
		noCheatMode.setDescription("Will adjust any mods to bypass NoCheat accordingly.");
		
		ttfMode = new SideMod("Huzuni Chat");
		ttfMode.setCategory(WindowCategory.MODES);
		ttfMode.setState(true);
		ttfMode.setDescription("Simple TTF Chat.");
		mods.add(noCheatMode);
		mods.add(ttfMode);
		mods.add(new Sprint());
		mods.add(new KillAura());
		mods.add(new Flight());
		mods.add(new Bright());
		mods.add(new ModUI());
		mods.add(new NameProtect());
		mods.add(new AutoSoup());
		mods.add(new NameTags());
		mods.add(new Tracer());
		mods.add(new ESP());
		mods.add(new History());
		mods.add(new AntiKnockback());
		mods.add(new ProjectilePrediction());
		mods.add(new Speedmine());
		mods.add(new Xray());
		mods.add(new Dolphin());
		mods.add(new Criticals());
		mods.add(new NoFall());
		mods.add(new AutoTool());
		mods.add(new AutoTPA());
		mods.add(new Freecam());
		mods.add(new Nuker());
		mods.add(new ChestESP());
		mods.add(new Sneak());
		mods.add(new FastPlace());
		mods.add(new WaypointRenderer());
		mods.add(new AutoWalk());
		mods.add(new AutoCrafting());
		mods.add(new Search());
		mc.theClient.getModLoader().loadExternalMods(mc);
	}

	@Override
	public void onEnd() {
		
	}
	
	public static Mod getMod(String s)
	{
		for(Mod m : getMods())
		{
			if(m.getName().equalsIgnoreCase(s))
				return m;
		}
		return null;
	}
	
	public static Mod getNoCheatMode() {
		return noCheatMode;
	}

	public static Mod getTtfMode() {
		return ttfMode;
	}
	
}
