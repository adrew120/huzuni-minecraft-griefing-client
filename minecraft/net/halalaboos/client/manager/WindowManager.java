package net.halalaboos.client.manager;

import net.halalaboos.client.base.*;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.*;
import net.halalaboos.client.ui.window.custom.*;
import net.halalaboos.client.ui.window.elements.*;

public class WindowManager extends Manager {

	private static WManager modWindows = new WManager();
	@Override
	public void onStartup() {
		int startX = 2;
		int startY = 2;
		for(WindowCategory w : WindowCategory.values()) {
			Window window = new Window(w.getName(), startX, startY, 0, 0, modWindows);
			int yPos = 14;
			for(Mod m : ModManager.getMods()) {
				if(m.getCategory() == w) {
					Button button = new Button(m, 2, yPos);
					window.add(button);
					yPos = button.getObjectHeight() - 1;
				}
			}
			modWindows.add(window);
		}
		
		Window value = new Window("Values", startX, startY, 0, 0, modWindows);
		int yPos = 14;
		for(Value v : ValueManager.getValues()) {
			Slider slider = new Slider(v, 2, yPos, 100, 0.1F
					);
			value.add(slider);
			yPos = slider.getObjectHeight() - 1;
		}
		modWindows.add(value);
		Window xrayWindow = new WXray(modWindows);
		modWindows.add(xrayWindow);
		Window pvpWindow = new WPVP(modWindows);
		modWindows.add(pvpWindow);
		Window minimapWindow = new WMinimap(modWindows);
		modWindows.add(minimapWindow);
		modWindows.formatAllPanels(3);
	}

	@Override
	public void onEnd() {
		// TODO Auto-generated method stub
		
	}

	public static WManager getModWindows() {
		return modWindows;
	}

}
