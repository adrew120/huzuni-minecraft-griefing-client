package net.halalaboos.client.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.client.base.Manager;
import net.halalaboos.client.base.Waypoint;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.mods.Mod;
import net.minecraft.src.Entity;

public class WaypointManager extends Manager {

	private static List<Waypoint> waypoints = new CopyOnWriteArrayList<Waypoint>();

	@Override
	public void onStartup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnd() {
		// TODO Auto-generated method stub
		
	}

	public static List<Waypoint> getWaypoints() {
		return waypoints;
	}

	public static void addAtEntityPos(String name, Entity entity) {
		add(name, entity.posX, entity.posY, entity.posZ, SettingsFile.getServerIP(), SettingsFile.getServerPort());
	}
	public static void add(String name, double posX, double posY, double posZ) {
		add(name, posX, posY, posZ, SettingsFile.getServerIP(), SettingsFile.getServerPort());
	}
	
	public static void add(String name, double posX, double posY, double posZ, String server, int port) {
		remove(name);
		waypoints.add(new Waypoint(name, posX, posY, posZ, server, port));
	}
	
	public static void remove(String name) {
		for(Waypoint waypoint : waypoints) {
			if(waypoint.getName().equalsIgnoreCase(name) && waypoint.isOnServer()) {
				waypoints.remove(waypoint);
				break;
			}
		}
	}
}
