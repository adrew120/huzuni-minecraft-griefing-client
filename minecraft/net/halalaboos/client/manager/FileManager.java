package net.halalaboos.client.manager;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.base.Manager;
import net.halalaboos.client.files.*;
import net.halalaboos.client.mods.Mod;

public class FileManager extends Manager {

	private static List<HFile> files = new ArrayList<HFile>();

	@Override
	public void onStartup() {
		files.add(new ModFile());
		files.add(new ValueFile());
		files.add(new FriendsFile());
		files.add(new BindFile());
		files.add(new XrayFile());
		files.add(new WindowFile());
		files.add(new AccountFile());
		files.add(new WaypointFile());
		files.add(new SettingsFile());
		files.add(new SearchFile());
		for(HFile file : files) {
			try {
				file.onStart();
			} catch(Exception e) {
				mc.theClient.addChatMessage("Unable to load file '" + file.getName() + "'. Corrupt file?");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onEnd() {
		for(HFile file : files) {
			try {
				file.onEnd();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

}
