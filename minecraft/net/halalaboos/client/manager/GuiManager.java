package net.halalaboos.client.manager;

import net.halalaboos.client.base.*;
import net.halalaboos.client.event.*;
import net.halalaboos.client.event.events.*;
import net.halalaboos.client.ui.*;
import net.halalaboos.client.ui.Gui.*;
import net.halalaboos.client.ui.simple.*;
import net.halalaboos.client.ui.tabbed.*;

public class GuiManager extends Manager implements Listener {

	private static Gui selectedGUI;
	
	@Override
	public void onStartup() {
		selectedGUI = new Array();
		TabbedController tabbedUI = new TabbedController();
		tabbedUI.initialize();
		new OldFashioned();
		new Vanilla();
		new Informative();
		new Paged();
		mc.theClient.getEventHandler().registerListener(EventRender.class, this);
	}

	@Override
	public void onEnd() {
		
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender) {
			int width = mc.theClient.getClientUtils().getScreenWidth(), 
			height = mc.theClient.getClientUtils().getScreenHeight();
			selectedGUI.render(width, height);
		}
	}

	public static Gui getSelectedGUI() {
		return selectedGUI;
	}

	public static void selectGUIStyle(Type guiType) {
		for(Gui gui : Gui.getGuis()) {
			if(gui.getType().equals(guiType)) {
				GuiManager.selectedGUI = gui;
				break;
			}
		}
	}
	
}
