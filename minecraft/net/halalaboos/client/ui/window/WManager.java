package net.halalaboos.client.ui.window;

import java.awt.Color;
import java.util.*;
import java.util.concurrent.*;
import net.halalaboos.client.Client;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.elements.Button;
import net.halalaboos.client.utils.ClientUtils;

import org.lwjgl.input.Keyboard;

public class WManager {
	private List<Window> windows = new CopyOnWriteArrayList<Window>();
	private List<Window> renderList = new CopyOnWriteArrayList<Window>();
	private Color primaryColor = new Color(0.1f, 0.1f, 0.1f);
	
	public WManager() {
	}
	
	public void onMouseClicked(int mouseX, int mouseY) {
		try {
			for (Window p : renderList) {
				if (p.onClick(mouseX, mouseY)) {
					pushToTop(p);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void renderWindows(int mouseX, int mouseY) {
		try {
			for (int x = renderList.size() - 1; x >= 0; x--) {
				Window p = renderList.get(x);
				p.render(mouseX, mouseY);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void renderWindowsIngame(int mouseX, int mouseY) {
		try {
			for (int x = renderList.size() - 1; x >= 0; x--) {
				Window p = renderList.get(x);
				if(!p.isPinned())
					continue;
				p.render(mouseX, mouseY);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onKeyTyped(char par1, int par2) {
		try {
			for (Window p : renderList) {
				if (isTopWindow(p)) {
					p.keyTyped(par1, par2);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateWindows() {
		for (Window p : getWindows()) {
			p.updatePanel();
			for (WElement o : p.objects) {
				if (o instanceof Button) {
					Button b = (Button) o;
					for (Mod m : ModManager.getMods()) {
						if (b.getText().equalsIgnoreCase(m.getName())) {
							b.setEnabled(m.isState());
							break;
						}
					}
				}
			}
		}
	}
	
	public void formatAllPanels(int rowSize) {
		int rowCount = 0,
		startX = 2,
		startY = 2,
		longestWindow = 0;
		
		for(Window window : renderList) {
			if(rowCount > rowSize) {
				rowCount = 0;
				startX = 2;
				startY = longestWindow;
			}
			int yPos = (int) (window.shouldRenderBody() ? window.getY() + window.getHeight():window.getTop().getY() + window.getTop().getHeight());
			if(yPos > longestWindow)
				longestWindow = yPos + 1;
			
			window.setX(startX);
			window.setY(startY);
			startX = window.getX() + window.getWidth() + 1;
			rowCount++;
		}
	}

	public void pushToTop(Window p) {
		renderList.remove(p);
		renderList.add(0, p);
	}

	public boolean isTopWindow(Window p) {
		return renderList.get(0) == p;
	}

	public boolean isWindowOverObject(Window currentPanel, WElement object) {
		int mouseX = Client.getClientUtils().getMouseX();
		int mouseY = Client.getClientUtils().getMouseY();

		for (Window p : renderList) {
			if (p == currentPanel)
				continue;

			if ((p.getTop().isPointInside(mouseX, mouseY) || (p.getBody()
					.isPointInside(mouseX, mouseY) && p.shouldRenderBody()))
					&& renderList.indexOf(p) < renderList.indexOf(currentPanel))
				return true;
		}
		return false;
	}

	public boolean isWindowAbove(Window currentPanel) {
		for (Window p1 : renderList) {
			if (isWindowAbove(currentPanel, p1))
				return true;
		}
		return false;
	}

	public boolean isWindowAbove(Window currentPanel, int mouseX, int mouseY) {
		for (Window p1 : renderList) {
			if (((p1.getBody().isPointInside(mouseX, mouseY) && p1.shouldRenderBody()) || p1
					.getTop().isPointInside(mouseX, mouseY))
					&& renderList.indexOf(p1) < renderList.indexOf(currentPanel))
				return true;
		}
		return false;
	}
	
	public boolean isWindowAbove(Window currentPanel, Window window) {
		return ((window.shouldRenderBody() && currentPanel.shouldRenderBody() && currentPanel.getBody().compare(window.getBody())) || 
				currentPanel.getTop().compare(window.getTop()))
				&& renderList.indexOf(window) < renderList.indexOf(currentPanel);
	}

	public void add(Window window) {
		if(!windows.contains(window))
			this.windows.add(window);
		if(!renderList.contains(window))
			this.renderList.add(window);
	}
	
	public void remove(Window window) {
		if(windows.contains(window))
			this.windows.remove(window);
		if(renderList.contains(window))
			this.renderList.remove(window);
	}
	
	public void clear() {
		this.windows.clear();
		this.renderList.clear();
	}
	
	public Window getWindow(String name) {
		for(Window p : getWindows()) {
			if(p.getName().equalsIgnoreCase(name))
				return p;
		}
		return null;
	}
	
	public List<Window> getWindows() {
		return windows;
	}

	public void setWindows(List<Window> panels) {
		this.windows = panels;
	}

	public Color getPrimaryColor() {
		return primaryColor;
	}

	public void setPrimaryColor(Color primaryColor) {
		this.primaryColor = primaryColor;
	}
	
}
