package net.halalaboos.client.ui.window;

import net.halalaboos.client.Client;
import net.halalaboos.client.utils.*;
import net.minecraft.client.Minecraft;

public abstract class WElement {

	private Window window;
	public Client cl = Client.getInstance();
	public Minecraft mc = Client.getMC();

	public abstract int getObjectWidth();

	public abstract int getObjectHeight();

	public abstract void render(Window window, WindowTheme theme, boolean isMouseAround);

	public abstract boolean onMouseClicked(Window window, int mouseX, int mouseY);

	public abstract void keyTyped(Window window, char charICTOR, int key);

	public abstract String getToolTipText(Window window, WindowTheme theme);

	public abstract Bounds getObjectBounds(Window window);

	public Window getWindow() {
		return window;
	}

	public void setWindow(Window panel) {
		this.window = panel;
	}

}
