package net.halalaboos.client.ui.window;

public abstract class WindowTheme {

	public abstract void renderElement(double x, double y, double width, double height, boolean mouseAround, boolean isEnabled);
	public abstract void renderPanel(double x, double y, double width, double height);
	public abstract void renderToolTip(String text, double x, double y);

}
