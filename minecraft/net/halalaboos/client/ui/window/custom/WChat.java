package net.halalaboos.client.ui.window.custom;

import java.io.IOException;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

import net.halalaboos.client.networking.HuzuniClient;
import net.halalaboos.client.networking.HuzuniClientImpl;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.WManager;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.elements.TextArea;
import net.halalaboos.client.ui.window.elements.TextField;

public class WChat extends Window {
	private TextArea textArea, userList;
	private TextField textField;

	public WChat(WManager manager) {
		super("Chat Window", 2, 2, 100, 0, manager);
		setResizable(true);
		setPinnable(true);
	}
	
	public void init() {
		textField = new TextField(2, 80 - 18, 100);
		textField.setMaxLength(100);
		textField.setScaleWidthToPanel(true);
		add(textField);
		textArea = new TextArea(73, 14, 80, 47);
		textArea.setAutoScroll(true);
		textArea.setScaleWidthToWindow(true);
		textArea.setMaxLines(15);
		add(textArea);
		textArea.append("This is the Huzuni Chatting window. Connect to a Huzuni server, and begin chatting with friends :)");
		userList = new TextArea(2, 14, 70, 47);
		userList.setAutoScroll(true);
		add(userList);
	}
	public void onObjectTyped(WElement o) {
		if(o == textField) {
			if(textField.getText().length() > 0) {
				if(textField.getText().startsWith("/")) {
					String command = textField.getText().substring(1);
					if(command.toLowerCase().startsWith("leave")) {
						mc.theClient.getNetworkingClient().stop();
					} else if(command.toLowerCase().startsWith("getip")) {
						String user = command.substring(6);
						mc.theClient.getNetworkingClient().getIP(user);
					} else if(command.toLowerCase().startsWith("help")) {
						textArea.append("This is the Huzuni Chatting window. Connect to a Huzuni server by pressing the pause button and navigating to the screen, then begin chatting with friends :)");
						textArea.append("Commands: /leave, /getip <user>");
					}
				} else {
					if(mc.theClient.getNetworkingClient().isConnected())
						mc.theClient.getNetworkingClient().sendMessage(textField.getText());
					else
						textArea.append("Type '/help' for a list of commands.");
				}
				textField.setText("");
			}
		}
	}

	public TextArea getTextArea() {
		return textArea;
	}

	public TextArea getUserList() {
		return userList;
	}
	
}
