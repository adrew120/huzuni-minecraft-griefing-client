package net.halalaboos.client.ui.window.custom;

import java.awt.image.BufferedImage;

import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.screen.PanelScreen;
import net.halalaboos.client.ui.window.WManager;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.elements.Dropdown;
import net.halalaboos.client.ui.window.elements.TextureButton;
import net.halalaboos.client.ui.window.elements.internal.IButton;
import net.halalaboos.client.utils.Coordinates;
import net.halalaboos.client.utils.Counter;
import net.halalaboos.client.utils.TextureUtils;
import net.minecraft.src.Block;
import net.minecraft.src.Chunk;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.MapColor;
import net.minecraft.src.MapInfo;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Tessellator;

public class WMinimap extends Window implements Listener {

	private byte[] colors = new byte[16384];
	private int bufferedImage;
	private int[] intArray;
	private TextureButton imageButton;
	private Coordinates lastPos;
	private Counter msCounter = new Counter(50, 1);
	
	public WMinimap(WManager manager) {
		super("Minimap", 2, 2, 128, 128, manager);
		setPinnable(true);
		setResizable(true);
		mc.theClient.getEventHandler().registerListener(EventTick.class, this);
	}

	public void init() {
		bufferedImage = mc.renderEngine
				.allocateAndSetupTexture(new BufferedImage(128, 128, 2));
		intArray = new int[16384];
		for (int var4 = 0; var4 < 16384; ++var4) {
			intArray[var4] = 0;
		}
	}

	public void render(int mouseX, int mouseY) {
		super.render(mouseX, mouseY);
		if (shouldRenderBody()) {
			int x = this.getX() + 2, y = this.getY() + 14, width = getWidth() - 4, height = getHeight() - 16;
			renderMap(x, y, width, height);
			glPushMatrix();
			glDisable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);

			glTranslatef(x + width / 2, y + height / 2, 0);
			glRotatef(180, 0, 0, 1);

			glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
			glColor4f(1, 1f, 1F, 0.6f);
			glLineWidth(1F);
			renderTriangle(0, 0, 2, 5);
			glEnable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			glPopMatrix();
		}
	}

	private void renderTriangle(int x, int y, int width, int height) {
		glBegin(GL_TRIANGLES);
		glVertex2f(x, -height);
		glVertex2f(-width, y);
		glVertex2f(width, y);
		glEnd();

		glEnable(GL_LINE_SMOOTH);
		glBegin(GL_LINES);
		glVertex2f(width, y);
		glVertex2f(-width, y);

		glVertex2f(width, y);
		glVertex2f(x, -height);

		glVertex2f(-width, y);
		glVertex2f(x, -height);
		glEnd();
		glDisable(GL_LINE_SMOOTH);
	}

	private void renderMap(int x, int y, int width, int height) {

		for (int var4 = 0; var4 < 16384; ++var4) {
			byte var5 = colors[var4];

			if (var5 / 4 == 0) {
				intArray[var4] = (var4 + var4 / 128 & 1) * 8 + 16 << 24;
			} else {
				int var6 = MapColor.mapColorArray[var5 / 4].colorValue;
				int var7 = var5 & 3;
				short var8 = 220;

				if (var7 == 2) {
					var8 = 255;
				}

				if (var7 == 0) {
					var8 = 180;
				}

				int var9 = (var6 >> 16 & 255) * var8 / 255;
				int var10 = (var6 >> 8 & 255) * var8 / 255;
				int var11 = (var6 & 255) * var8 / 255;

				if (mc.gameSettings.anaglyph) {
					int var12 = (var9 * 30 + var10 * 59 + var11 * 11) / 100;
					int var13 = (var9 * 30 + var10 * 70) / 100;
					int var14 = (var9 * 30 + var11 * 70) / 100;
					var9 = var12;
					var10 = var13;
					var11 = var14;
				}

				intArray[var4] = -16777216 | var9 << 16 | var10 << 8 | var11;
			}
		}

		mc.renderEngine.createTextureFromBytes(intArray, 128, 128,
				bufferedImage);
		Tessellator tesselator = Tessellator.instance;
		float var18 = 0.0F;
		glBindTexture(GL_TEXTURE_2D, bufferedImage);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		glEnable(GL_TEXTURE_2D);
		glColor3f(1, 1, 1);
		tesselator.startDrawingQuads();
		tesselator.setColorOpaque_F(1, 1, 1);
		tesselator.addVertexWithUV((double) ((float) (x + 0) + var18),
				(double) ((float) (y + height) - var18),
				-0.009999999776482582D, 0.0D, 1.0D);
		tesselator.addVertexWithUV((double) ((float) (x + width) - var18),
				(double) ((float) (y + height) - var18),
				-0.009999999776482582D, 1.0D, 1.0D);
		tesselator.addVertexWithUV((double) ((float) (x + width) - var18),
				(double) ((float) (y + 0) + var18), -0.009999999776482582D,
				1.0D, 0.0D);
		tesselator.addVertexWithUV((double) ((float) (x + 0) + var18),
				(double) ((float) (y + 0) + var18), -0.009999999776482582D,
				0.0D, 0.0D);
		tesselator.draw();
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glPushMatrix();
		glTranslatef(0.0F, 0.0F, -0.04F);
		glScalef(1.0F, 1.0F, 1.0F);
		glPopMatrix();
	}

	private void updateMap(int scale) {
		short var4 = 128;
		short var5 = 128;
		int var6 = 1 << scale;
		int var7 = (int) mc.thePlayer.posX;
		int var8 = (int) mc.thePlayer.posZ;
		int var9 = MathHelper.floor_double(mc.thePlayer.posX - (double) var7)
				/ var6 + var4 / 2;
		int var10 = MathHelper.floor_double(mc.thePlayer.posZ - (double) var8)
				/ var6 + var5 / 2;
		int var11 = 128 / var6;

		if (mc.theWorld.provider.hasNoSky) {
			var11 /= 2;
		}
		for (int var13 = var9 - var11 + 1; var13 < var9 + var11; ++var13) {

			int var14 = 255;
			int var15 = 0;
			double var16 = 0.0D;

			for (int var18 = var10 - var11 - 1; var18 < var10 + var11; ++var18) {
				if (var13 >= 0 && var18 >= -1 && var13 < var4 && var18 < var5) {
					int var19 = var13 - var9;
					int var20 = var18 - var10;
					boolean var21 = var19 * var19 + var20 * var20 > (var11 - 2)
							* (var11 - 2);
					int var22 = (var7 / var6 + var13 - var4 / 2) * var6;
					int var23 = (var8 / var6 + var18 - var5 / 2) * var6;
					int[] var24 = new int[256];
					Chunk var25 = mc.theWorld.getChunkFromBlockCoords(var22,
							var23);

					if (!var25.isEmpty()) {
						int var26 = var22 & 15;
						int var27 = var23 & 15;
						int var28 = 0;
						double var29 = 0.0D;
						int var31;
						int var32;
						int var33;
						int var36;

						if (mc.theWorld.provider.hasNoSky) {
							var31 = var22 + var23 * 231871;
							var31 = var31 * var31 * 31287121 + var31 * 11;

							if ((var31 >> 20 & 1) == 0) {
								var24[Block.dirt.blockID] += 10;
							} else {
								var24[Block.stone.blockID] += 10;
							}

							var29 = 100.0D;
						} else {
							for (var31 = 0; var31 < var6; ++var31) {
								for (var32 = 0; var32 < var6; ++var32) {
									var33 = var25.getHeightValue(var31 + var26,
											var32 + var27) + 1;
									int var34 = 0;

									if (var33 > 1) {
										boolean var35;

										do {
											var35 = true;
											var34 = var25.getBlockID(var31
													+ var26, var33 - 1, var32
													+ var27);

											if (var34 == 0) {
												var35 = false;
											} else if (var33 > 0
													&& var34 > 0
													&& Block.blocksList[var34].blockMaterial.materialMapColor == MapColor.airColor) {
												var35 = false;
											}

											if (!var35) {
												--var33;

												if (var33 <= 0) {
													break;
												}

												var34 = var25.getBlockID(var31
														+ var26, var33 - 1,
														var32 + var27);
											}
										} while (var33 > 0 && !var35);

										if (var33 > 0
												&& var34 != 0
												&& Block.blocksList[var34].blockMaterial
														.isLiquid()) {
											var36 = var33 - 1;
											boolean var37 = false;
											int var43;

											do {
												var43 = var25.getBlockID(var31
														+ var26, var36--, var32
														+ var27);
												++var28;
											} while (var36 > 0
													&& var43 != 0
													&& Block.blocksList[var43].blockMaterial
															.isLiquid());
										}
									}

									var29 += (double) var33
											/ (double) (var6 * var6);
									++var24[var34];
								}
							}
						}

						var28 /= var6 * var6;
						var31 = 0;
						var32 = 0;

						for (var33 = 0; var33 < 256; ++var33) {
							if (var24[var33] > var31) {
								var32 = var33;
								var31 = var24[var33];
							}
						}

						double var40 = (var29 - var16) * 4.0D
								/ (double) (var6 + 4)
								+ ((double) (var13 + var18 & 1) - 0.5D) * 0.4D;
						byte var39 = 1;

						if (var40 > 0.6D) {
							var39 = 2;
						}

						if (var40 < -0.6D) {
							var39 = 0;
						}

						var36 = 0;

						if (var32 > 0) {
							MapColor var42 = Block.blocksList[var32].blockMaterial.materialMapColor;

							if (var42 == MapColor.waterColor) {
								var40 = (double) var28 * 0.1D
										+ (double) (var13 + var18 & 1) * 0.2D;
								var39 = 1;

								if (var40 < 0.5D) {
									var39 = 2;
								}

								if (var40 > 0.9D) {
									var39 = 0;
								}
							}

							var36 = var42.colorIndex;
						}

						var16 = var29;

						if (var18 >= 0
								&& var19 * var19 + var20 * var20 < var11
										* var11
								&& (!var21 || (var13 + var18 & 1) != 0)) {
							byte var41 = colors[var13 + var18 * var4];
							byte var38 = (byte) (var36 * 4 + var39);

							if (var41 != var38) {
								if (var14 > var18) {
									var14 = var18;
								}

								if (var15 < var18) {
									var15 = var18;
								}

								colors[var13 + var18 * var4] = var38;
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void onEvent(Event event) {
		if (event instanceof EventTick) {
			if (shouldRenderBody()
					&& (mc.currentScreen instanceof PanelScreen ? true
							: isPinned())) {
				msCounter.count();
				if(lastPos == null)
					lastPos = new Coordinates(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ);
				
				if(new Coordinates(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ).getDifference(lastPos) != 0) {
					if(msCounter.getCount() > 1) {
						updateMap(0);
						msCounter.reset();
						lastPos = new Coordinates(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ);
					}
				}
			}
		}
	}

}
