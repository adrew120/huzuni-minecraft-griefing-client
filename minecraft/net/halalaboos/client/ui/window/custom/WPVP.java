package net.halalaboos.client.ui.window.custom;

import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.ui.window.WManager;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.elements.Button;
import net.halalaboos.client.ui.window.elements.Dropdown;
import net.halalaboos.client.ui.window.elements.internal.IButton;

public class WPVP extends Window {

	public WPVP(WManager manager) {
		super("PvP Modes", 2, 2, 0, 0, manager);
		// TODO Auto-generated constructor stub
	}
	
	public void init() {
		int y = 26;
		String[] mods = new String[] {"Criticals", "AntiKnockback", "AutoSoup", "Projectiles", "Aimbot"};
		for(String s : mods) {
			add(new Button(s, ModManager.getMod(s).getDescription(), 2, y));
			y+=13;
		}
		
		y = 14;		
		Dropdown dropdown = new Dropdown("Modes", 2, y, 60, 20);
		add(dropdown);
		dropdown.setScaleWidthToPanel(true);
		String[] modes = new String[] {"Players", "Mobs", "Animals"};
		y = 12;
		for(String s : modes) {
			dropdown.add(new IButton(dropdown, ModManager.getMod(s), 2, y));
			y+=14;
		}
	}
}
