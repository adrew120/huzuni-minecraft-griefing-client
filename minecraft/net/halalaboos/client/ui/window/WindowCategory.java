package net.halalaboos.client.ui.window;

public enum WindowCategory {
	
	//PVP("PVP", 0xff0000),
	RENDER("Render", 0xffff33),
	WORLD("World", 0x3399ff),
	PLAYER("Player", 0x99ff33),
	MODES("Modes");
	
	private String name;
	private int color;
	WindowCategory(String name) {
		this(name, 0xffffff);
	}
	WindowCategory(String name, int color) {
		this.name = name;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	
	public int getColor() {
		return color;
	}
}
