package net.halalaboos.client.ui.window.elements;

import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.ClientUtils;
import net.minecraft.src.ChatAllowedCharacters;
import net.minecraft.src.GuiScreen;

public class TextField extends WElement {

	private int x, y, width, height, maxLength;

	private boolean enabled, scaleWidthToPanel;

	private String text;

	/**
	 * @param x
	 *            X coordinate on panel.
	 * @param y
	 *            Y coordinate on panel.
	 * @param w
	 *            width of the text field.
	 * */
	public TextField(int x, int y, int w) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = 11;
		text = "";
	}

	@Override
	public boolean onMouseClicked(Window w, int mouseX, int mouseY) {
		if (getObjectBounds(w).isPointInside(mouseX, mouseY)) {
			{
				this.enabled = true;
				w.onObjectClicked(this);
			}
			return true;
		} else {
			this.enabled = false;
			return false;
		}
	}

	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return x + width + 2;
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return y + height + 2;
	}

	@Override
	public void render(Window w, WindowTheme theme, boolean isMouseAround) {
		theme.renderElement(getObjectBounds(w).getX(), getObjectBounds(w).getY(),
				getObjectBounds(w).getWidth(),
				getObjectBounds(w).getHeight(),
				isMouseAround, isEnabled());
		if(cl.getGUIFR().getStringWidth(text + "|") > getObjectBounds(w).getWidth()) {
			String text = "";
			for(int index = getText().toCharArray().length - 1; index >= 0; index--) {
				if(cl.getGUIFR().getStringWidth(getText().toCharArray()[index] + text + "|") < getObjectBounds(w).getWidth())
					text = getText().toCharArray()[index] + text;
			}

			cl.getGUIFR().drawString(text + getThingy(), (int) getObjectBounds(w).getX() + 2, (int) getObjectBounds(w).getY() + 2, 0xffffff);
		} else 
			cl.getGUIFR().drawString(getText() + getThingy(),
						(int) getObjectBounds(w).getX() + 2,
						(int) getObjectBounds(w).getY() + 2, 0xffffff);
		
		if(hasBackPressed && Keyboard.isKeyDown(Keyboard.KEY_BACK)) {
			if(isBackSpacing())
				minus(1);
		}else {
			hasBackPressed = false;			
		}
		
		if(hasSameKeyPressed && Keyboard.isKeyDown(lastKey) && !(hasBackPressed && Keyboard.isKeyDown(Keyboard.KEY_BACK))) {
			if(isHoldingKey()) {
				text += lastChar;
				if (getText().length() > getMaxLength() && getMaxLength() > 0)
					text = text.substring(0, getMaxLength());
			}
				
		}else {
			hasSameKeyPressed = false;			
		}
	}

	@Override
	public Bounds getObjectBounds(Window p) {
		// TODO Auto-generated method stub
		return new Bounds(p.getX() + x, // x
				p.getY() + y, // y
				isScaleWidthToPanel() ? p.getWidth() - 4 : width, // x1
				height// y1
		);
	}

	public String getThingy() {
		return (mc.ingameGUI.getUpdateCounter() / 6 % 2 == 0 && enabled) ? "|"
				: "";
	}

	private boolean hasBackPressed, hasSameKeyPressed;
	private int lastKey;
	private char lastChar;
	private long backSpaceDelay, sameKeyDelay;
	
	@Override
	public void keyTyped(Window p, char charICTOR, int key) {
		if (!enabled)
			return;
		
		if(key == Keyboard.KEY_V) {
			if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) {
				if(Sys.getClipboard() != null) {
					text += Sys.getClipboard();
				}
			}
		}
		
		if (key == Keyboard.KEY_ESCAPE)
			setEnabled(false);

		if (key == Keyboard.KEY_BACK) {
			minus(1);
			hasBackPressed = true;
			backSpaceDelay = ClientUtils.getSystemTime();
			return;
		}
		if (ChatAllowedCharacters.isAllowedCharacter(charICTOR)) {
			text += charICTOR;
			if(lastChar == charICTOR) {
				hasSameKeyPressed = true;
				sameKeyDelay =  ClientUtils.getSystemTime();
			}
			lastChar = charICTOR;
			lastKey = key;
		}

		if (getText().length() > getMaxLength() && getMaxLength() > 0)
			text = text.substring(0, getMaxLength());

		if (key == Keyboard.KEY_RETURN) {
			p.onObjectTyped(this);
		}
	}

	public void minus(int p) {
		if((text.length() - p) >= 0)
			text = text.substring(0, text.length() - p);
	}
	
	public boolean isBackSpacing() {
		if((ClientUtils.getSystemTime() - backSpaceDelay) > 500)
			return hasBackPressed;
		return false;
	}
	
	public boolean isHoldingKey() {
		if((ClientUtils.getSystemTime() - sameKeyDelay) > 500)
			return hasSameKeyPressed;
		return false;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isScaleWidthToPanel() {
		return scaleWidthToPanel;
	}

	public void setScaleWidthToPanel(boolean scaleWidthToPanel) {
		this.scaleWidthToPanel = scaleWidthToPanel;
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		// TODO Auto-generated method stub
		return null;
	}
}
