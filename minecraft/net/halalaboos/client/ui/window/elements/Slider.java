package net.halalaboos.client.ui.window.elements;

import java.text.*;
import net.halalaboos.client.base.Value;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.ClientUtils;

import org.lwjgl.input.*;

public class Slider extends WElement {

	private static final float SLIDER_BUTTON_SIZE = 7F;

	private int x, y, width;
	private float sliderPercentage, incrementValue;
	private Value value;
	private boolean dragging, scaleToWindow;
	private DecimalFormat format;
	
	public Slider(Value value, int x, int y, int width, float incrementValue) {
		this.value = value;
		this.x = x;
		this.y = y;
		this.width = width;
		this.incrementValue = incrementValue;
		format = new DecimalFormat("#.#");
	}

	@Override
	public int getObjectWidth() {
		return getX() + width;
	}

	@Override
	public int getObjectHeight() {
		return getY() + 15;
	}

	@Override
	public void render(Window window, WindowTheme theme, boolean isMouseAround) {
		drag(window, cl.getClientUtils().getMouseX(), cl.getClientUtils().getMouseY());
		
		// rendering background of the slider
		Bounds objectBounds = this.getObjectBounds(window);
		theme.renderElement(objectBounds.getX(), objectBounds.getY(), objectBounds.getWidth(), objectBounds.getHeight(), isMouseAround || isDragging(), false);
		
		// rendering name of the value object
		cl.getGUIFR().drawString(getValue().getName(), objectBounds.getX() + 2, objectBounds.getY() + 3, (isMouseAround || isDragging()) ? 0xE6E6E6 : 0xBABABA);
		
		// rendering the (formatted) value of the value object
		String formattedValue = format.format(getValue().getValue());
		cl.getGUIFR().drawString(formattedValue, (window.getX() + objectBounds.getWidth() - cl.getGUIFR().getStringWidth(formattedValue) - 2), (int) (window.getY() + y + 3), 0xBABABA);
		
		// rendering of the slider's point
		Bounds pointBounds = this.getPointBounds(window);		
		theme.renderElement(pointBounds.getX(), pointBounds.getY(), pointBounds.getWidth(), pointBounds.getHeight(), isMouseAround, true);
	}

	@Override
	public boolean onMouseClicked(Window window, int mouseX, int mouseY) {

		if (getPointBounds(window).isPointInside(mouseX, mouseY))
			dragging = true;
		else
			dragging = false;
		
		return getObjectBounds(window).isPointInside(mouseX, mouseY);
	}
	
	/**
	 * Handles the dragging of the slider's point. Will calculate percentage of the slider and pass that into the value object.
	 * */
	public void drag(Window window, int mouseX, int mouseY) {
		if (Mouse.isButtonDown(0) && dragging) {
			
			//get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
			float differenceWithMouseAndSliderBase = (float) (mouseX - (getObjectBounds(window).getX())) - (SLIDER_BUTTON_SIZE / 2F);
						
			//converted into 0.0F ~ 1.0F percentage
			sliderPercentage = differenceWithMouseAndSliderBase / getWidthForPoint(window);

			if (sliderPercentage < 0.0F)
				sliderPercentage = 0.0F;

			if (sliderPercentage > 1.0F)
				sliderPercentage = 1.0F;
			
			//set the value of the value object to the percentage of the maximum allowed value, minus the minimum. Add the minimum. percent * ((max value) - (min value)) + min value.
			setValue();
		} else {
			sliderPercentage = (float) ((value.getValue() - value.getMin()) / (value.getMax() - value.getMin()));
			dragging = false;
		}
	}

	@Override
	public Bounds getObjectBounds(Window window) {
		return new Bounds(window.getX() + this.getX(),
				window.getY() + getY(),
				(window.getWidth() - 4),
				13
		);
	}
	
	public Bounds getPointBounds(Window window) {
		return new Bounds(getObjectBounds(window).getX() + getPositionForPoint(window) + 1,
				window.getY() + getY() + 1,
				SLIDER_BUTTON_SIZE,
				11
		);
	}
	
	/**
	 * Sets our value object to the calculated value of the slider.
	 * */
	public void setValue() {
		incrementValue = value.getIncrementValue();
		float calculatedValue = (sliderPercentage * (value.getMax() - value.getMin()));
		
		if(calculatedValue % incrementValue != 0)
			value.setValue(((calculatedValue) - ((calculatedValue) % incrementValue)) + value.getMin());
		else	
			value.setValue(calculatedValue + value.getMin());
	}
	
	@Override
	public void keyTyped(Window window, char charICTOR, int key) {

	}
	
	/**
	 * @return the rendering position for the slider's point.
	 * */
	public float getPositionForPoint(Window window) {
		return ((float) sliderPercentage * getWidthForPoint(window));
	}

	/**
	 * 	@return Width the slider point can be inside of. <br>
	 * 	NOT including the actual pixel positions.
	 * */
	private float getWidthForPoint(Window window) {
		float maxPointForRendering = (window.getWidth() - x - SLIDER_BUTTON_SIZE - 1),
		beginPoint = (x + 1);
		return maxPointForRendering - beginPoint;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public float getSliderPercentage() {
		return sliderPercentage;
	}

	public void setSliderPercentage(float sliderValue) {
		this.sliderPercentage = sliderValue;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	public boolean isDragging() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		return null;
	}

}
