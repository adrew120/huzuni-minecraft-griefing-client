package net.halalaboos.client.ui.window.elements.internal;

import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.elements.IElement;
import net.halalaboos.client.utils.Bounds;

public class IButton extends IElement{

	private String text, tooltip;
	private boolean scaleWidthToHeight, enabled;
	private Mod mod;
	
	public IButton(WElement parent, Mod mod, int x, int y, int width, int height) {
		this(parent, mod.getName(), mod.getDescription(), x, y, width, height);
		this.mod = mod;
	}
	
	public IButton(WElement parent, String text, String tooltip, int x, int y, int width, int height) {
		this.setParent(parent);
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
		this.text = text;
		this.tooltip = tooltip;
	}
	
	public IButton(WElement parent, Mod mod, int x, int y) {
		this(parent, mod, x, y, -1, 12);
		this.enabled = mod.isState();
		scaleWidthToHeight = true;
	}

	@Override
	public void drawObject(boolean isMouseAround) {
		if(mod != null)
			this.enabled = mod.isState();
		Bounds bound = getObjectBounds();
		getParent().getWindow().getTheme().renderElement(bound.getX(), bound.getY(), bound.getWidth(), bound.getHeight(), isMouseAround, enabled);
		mc.theClient.getGUIFR().drawStringWithShadow(text, bound.getX() + 2, bound.getY() + 2, enabled ? 0xEEEEEE : 0xAAAAAA);
		if(isMouseAround && tooltip != null) {
			int mouseX = mc.theClient.getClientUtils().getMouseX(),
			mouseY = mc.theClient.getClientUtils().getMouseY();
			mc.theClient.getRenderUtils().drawRect(mouseX, mouseY, mouseX + mc.theClient.getGUIFR().getStringWidth(text) + 2, 12, 0, 0, 0, 0.75F);
			mc.theClient.getGUIFR().drawStringWithShadow(tooltip, mouseX + 2, mouseY + 2, 0xFFFFFF);
		}
	}

	@Override
	public boolean onMouseClicked(int mouseX, int mouseY) {
		if(getObjectBounds().isPointInside(mouseX, mouseY)) {
			if(mod != null) {
				mod.toggle();
			}
			return true;
		}
		return false;
	}

	@Override
	public Bounds getObjectBounds() {
		// TODO Auto-generated method stub
		Bounds parentBounds = getParent().getObjectBounds(getParent().getWindow());
		return new Bounds(parentBounds.getX() + getX(), parentBounds.getY() + getY(), scaleWidthToHeight ? getParent().getObjectWidth() - 8:getWidth(), getHeight());
	}

}
