package net.halalaboos.client.ui.window.elements;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;

public class Dropdown extends WElement {

	private int x, y, width, height;
	private String text;
	private boolean show, scaleWidthToPanel;
	private List<IElement> objects = new CopyOnWriteArrayList<IElement>();

	public Dropdown(String text, int x, int y, int width, int height) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return (int) (getX() + getObjectBounds(getWindow()).getWidth() + 2);
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return (int) (getY() + getObjectBounds(getWindow()).getHeight() + 2);
	}

	@Override
	public void render(Window w, WindowTheme wTheme, boolean isMouseAround) {
		Bounds bo = getObjectBounds(w);
		if (isShow()) {
			wTheme.renderElement(bo.getX(), bo.getY(), bo.getWidth(),
					bo.getHeight(), isMouseAround, false);
			mc.theClient.getGUIFR().drawString(getText(), bo.getX() + 2,
					bo.getY() + 2, 0xffffff);
			for (IElement o : getObjects()) {
				o.drawObject(o.getObjectBounds().isPointInside(
						mc.theClient.getClientUtils().getMouseX(),
						mc.theClient.getClientUtils().getMouseY()));
			}
		} else {
			wTheme.renderElement(
					bo.getX(),
					bo.getY(),
					bo.getWidth(),
					bo.getHeight(),
					false,
					isMouseAround
							&& getTop(w).isPointInside(
									mc.theClient.getClientUtils().getMouseX(),
									mc.theClient.getClientUtils().getMouseY()));
			mc.theClient.getGUIFR().drawString(getText(), bo.getX() + 2,
					bo.getY() + 2, 0xffffff);
		}

	}

	@Override
	public boolean onMouseClicked(Window w, int mouseX, int mouseY) {
		Bounds bo = getObjectBounds(w);
		if (getTop(w).isPointInside(mouseX, mouseY)) {
			setShow(!isShow());
			w.onObjectClicked(this);
			return true;
		}
		if (isShow() && bo.isPointInside(mouseX, mouseY)) {
			w.onObjectClicked(this);
			for (IElement o : getObjects()) {
				if (o.onMouseClicked(mouseX, mouseY)) {
					return true;
				}
			}
			return true;
		} else
			setShow(false);
		return false;
	}

	@Override
	public void keyTyped(Window w, char charICTOR, int key) {
		// TODO Auto-generated method stub
	}

	@Override
	public Bounds getObjectBounds(Window w) {
		// TODO Auto-generated method stub
		if (!isShow()) {
			return getTop(w);
		} else
			return new Bounds(w.getX() + getX(), w.getY() + getY(),
					isScaleWidthToPanel() ? w.getWidth() - 4 : getWidth(),
					getHeight());
	}

	public Bounds getTop(Window w) {
		return new Bounds(w.getX() + getX(), w.getY() + getY(),
				isScaleWidthToPanel() ? w.getWidth() - 4 : getWidth(), 11);
	}

	public void add(IElement o) {
		o.setParent(this);
		Bounds bounds = o.getObjectBounds();
		if (bounds.getWidth() > getWidth())
			setWidth((int) bounds.getWidth() + 2);
		if (bounds.getHeight() + bounds.getY() - getY() > getHeight())
			setHeight((int) bounds.getHeight() + (int) bounds.getY() - getY()
					+ 2);
		getObjects().add(o);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public List<IElement> getObjects() {
		return objects;
	}

	public void setObjects(List<IElement> objects) {
		this.objects = objects;
	}

	public boolean isScaleWidthToPanel() {
		return scaleWidthToPanel;
	}

	public void setScaleWidthToPanel(boolean scaleWidthToPanel) {
		this.scaleWidthToPanel = scaleWidthToPanel;
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		// TODO Auto-generated method stub
		return null;
	}

}
