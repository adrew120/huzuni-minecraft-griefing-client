package net.halalaboos.client.ui.window.elements;

import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;

public class Scrollable extends WElement {

	
	private int x, y, width, height, begindexWithinList = 0, maxLines = -1;
	private boolean scrolling, autoScroll = true, scaleHeightToWindow, scaleWidthToWindow;
	
	public Scrollable(int x, int y, int width, int height) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	}
	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void render(Window window, WindowTheme theme,
			boolean isMouseAround) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMouseClicked(Window window, int mouseX, int mouseY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyTyped(Window window, char charICTOR, int key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bounds getObjectBounds(Window window) {
		return new Bounds(window.getX() + getX(), window.getY() + getY(),
				isScaleWidthToWindow() ? (window.getWidth() - 2) - getX() : getWidth(),
				isScaleHeightToWindow() ? (window.getHeight() - getY() - 2)
						: getHeight());
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getBegindexWithinList() {
		return begindexWithinList;
	}
	
	public boolean isScrolling() {
		return scrolling;
	}

	public void setScrolling(boolean scrolling) {
		this.scrolling = scrolling;
	}

	public boolean isAutoScroll() {
		return autoScroll;
	}

	public void setAutoScroll(boolean autoScroll) {
		this.autoScroll = autoScroll;
	}

	public boolean isScaleHeightToWindow() {
		return scaleHeightToWindow;
	}

	public void setScaleHeightToWindow(boolean scaleHeightToWindow) {
		this.scaleHeightToWindow = scaleHeightToWindow;
	}

	public boolean isScaleWidthToWindow() {
		return scaleWidthToWindow;
	}

	public void setScaleWidthToWindow(boolean scaleWidthToWindow) {
		this.scaleWidthToWindow = scaleWidthToWindow;
	}

	public int getMaxLines() {
		return maxLines;
	}

	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines;
	}
}
