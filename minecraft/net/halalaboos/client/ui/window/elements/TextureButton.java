package net.halalaboos.client.ui.window.elements;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.Texture;
import net.halalaboos.client.utils.TextureUtils;
import org.lwjgl.opengl.GL11;

public class TextureButton extends WElement {

	private int x, y, width, height;
	private Texture texture;
	
	public TextureButton(Texture texture, int x, int y, int width, int height) {
		this.texture = texture;
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
	}

	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return getX() + getWidth() / 2 + 6;
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return getY() + getHeight() / 2 + 6;
	}

	@Override
	public void render(Window w, WindowTheme wTheme, boolean isMouseAround) {
		wTheme.renderElement(getObjectBounds(w).getX(), getObjectBounds(w).getY(), getObjectBounds(w).getWidth(), getObjectBounds(w).getHeight(), isMouseAround, false);
		texture.renderTexture(getObjectBounds(w).getX(), getObjectBounds(w).getY(), getObjectBounds(w).getWidth(), getObjectBounds(w).getHeight(), Color.WHITE);
	}

	@Override
	public boolean onMouseClicked(Window w, int mouseX, int mouseY) {
		if (getObjectBounds(w).isPointInside(mouseX, mouseY)) {
			w.onObjectClicked(this);
			return true;
		} else
			return false;
	}

	@Override
	public Bounds getObjectBounds(Window w) {
		// TODO Auto-generated method stub
		return new Bounds(
				w.getX() + getX(), // x
				w.getY() + getY(), // y
				getWidth() / 2 + 4, // x1
				getHeight() / 2 + 4// y1
		);
	}

	@Override
	public void keyTyped(Window w, char charICTOR, int key) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
}