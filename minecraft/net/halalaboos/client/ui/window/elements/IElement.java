package net.halalaboos.client.ui.window.elements;

import net.halalaboos.client.Client;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.utils.Bounds;
import net.minecraft.client.Minecraft;

public abstract class IElement {

	public int x, y, width, height;
	public WElement parent;
	public Minecraft mc = Client.getMC();
	public abstract void drawObject(boolean isMouseAround);
	
	public abstract boolean onMouseClicked(int mouseX, int mouseY);

	public abstract Bounds getObjectBounds();

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public WElement getParent() {
		return parent;
	}

	public void setParent(WElement parent) {
		this.parent = parent;
	}
	
}
