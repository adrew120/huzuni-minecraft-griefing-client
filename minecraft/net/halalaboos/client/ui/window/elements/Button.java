package net.halalaboos.client.ui.window.elements;

import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.Counter;

import org.lwjgl.opengl.GL11;

public class Button extends WElement {

	private int x, y;
	private String text, description = null;
	private boolean enabled;
	private Mod mod = null;
	/**
	 * @param m
	 *            Mod being toggled when the object is fired.
	 * @param x
	 *            x coordinate on panel.
	 * @param y
	 *            y coordinate on panel.
	 * */
	public Button(Mod m, int x, int y) {
		this(m.getName(), m.getDescription(), x, y);
		mod = m;
	}

	/**
	 * @param s
	 *            Text displayed when rendered.
	 * @param x
	 *            x coordinate on panel.
	 * @param y
	 *            y coordinate on panel.
	 * */
	public Button(String text, String description, int x, int y) {
		this.text = text;
		this.description = description;
		this.x = x;
		this.y = y;
		mod = ModManager.getMod(text);
	}

	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return getX() + cl.getGUIFR().getStringWidth(text) + 20;
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return getY() + 14;
	}

	@Override
	public void render(Window p, WindowTheme theme, boolean isMouseAround) {
		theme.renderElement(getObjectBounds(p).getX(), 
				getObjectBounds(p).getY(),
				getObjectBounds(p).getWidth(),
				getObjectBounds(p).getHeight(),isMouseAround, enabled);
		cl.getGUIFR().drawString(text, p.getX() + getX() + ((getWindow().getWidth() / 2 - 2) - cl.getGUIFR().getStringWidth(text) / 2),
				p.getY() + getY() + 2, (enabled) ? 0xEEEEEE : 0xAAAAAA);
	}

	@Override
	public boolean onMouseClicked(Window p, int mouseX, int mouseY) {
		if (getObjectBounds(p).isPointInside(mouseX, mouseY)) {
			p.onObjectClicked(this);
			return true;
		} else
			return false;
	}

	@Override
	public Bounds getObjectBounds(Window p) {
		// TODO Auto-generated method stub
		return new Bounds(
				p.getX() + getX(),
		//p.getX() + p.getWidth() + getX() - 21, // x
				p.getY() + getY(), // y
				getWindow().getWidth() - 4, // x1
				12 // y1
		);
	}

	public void keyTyped(Window p, char charICTOR, int key) {

	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		// TODO Auto-generated method stub
		return description;
	}

}
