package net.halalaboos.client.ui.window.elements;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;
import net.minecraft.src.ChatAllowedCharacters;
import net.minecraft.src.MathHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class TextArea extends WElement {

	private int x, y, width, height, stringColor = 0xffffff,
			begindexWithinList = 0, maxLines = -1;
	private boolean scrolling, autoScroll = true, scaleHeightToWindow,
			scaleWidthToWindow, renderBackground = true;
	private CopyOnWriteArrayList<String> strings = new CopyOnWriteArrayList<String>();

	public TextArea(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return x + width + 2;
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return y + height + 2;
	}

	@Override
	public void render(Window w, WindowTheme wTheme, boolean isMouseAround) {
		List<String> list = formattedStrings(w, getStrings());
		scroll(w, list);
		adjustPosition(w, isMouseAround, list);
		keepTextRendered(list);
		double x = getObjectBounds(w).getX();
		double y = getObjectBounds(w).getY();
		double y1 = getObjectBounds(w).getY() + getObjectBounds(w).getHeight();
		double x1 = getObjectBounds(w).getX() + getObjectBounds(w).getWidth();
		
		GL11.glPushMatrix();
		if(isRenderBackground())
			wTheme.renderElement(x, y, x1 - x, y1 - y, false, false);

		mc.theClient.getRenderUtils().prepareScissorBox((float) x + 1, (float) y + 1, (float) x1 - 1,
				(float) y1 - 1);
		GL11.glEnable(GL11.GL_SCISSOR_TEST);

		int yindex = 0;
		for (int index = getBegindexWithinList(); (index <= (getBegindexWithinList() + getMaxRenderableLines()))
				&& (index < (list.size())); index++) {
			mc.theClient.getGUIFR().drawStringWithShadow(list.get(index), x + 2,
					y + yindex + 2, getStringColor());
			yindex += mc.theClient.getGUIFR().getFont().getStringHeight(list.get(index));
		}
		Bounds b = getSliderBounds(w, list);
		wTheme.renderElement(b.getX(), b.getY(), b.getWidth(), b.getHeight(), isMouseAround, false);
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
		GL11.glPopMatrix();
	}

	public void adjustPosition(Window p, boolean isMouseAround,
			List<String> stwings) {
		if (isMouseAround && !Mouse.isButtonDown(0)) {
			if ((Mouse.hasWheel()) && (mc.currentScreen != null)) {
				if(Mouse.next()) {
					int l = Mouse.getEventDWheel();
					if (l > 0)
						l = -1;
					else if (l < 0)
						l = 1;

					setBegindexWithinList(getBegindexWithinList() + l, stwings);	
				}
			}
		}
	}

	public void scroll(Window p, List list) {
		if (scrolling && Mouse.isButtonDown(0)) {
			double percent = ((double) (mc.theClient.getClientUtils().getMouseY() - getObjectBounds(
					p).getY()) / (double) (getObjectBounds(p).getHeight()));
			setBegindexWithinList((int) (percent * (double) (list.size() - 1)), list);
		} else
			scrolling = false;
	}

	@Override
	public boolean onMouseClicked(Window p, int mouseX, int mouseY) {
		if (getSliderBounds(p).isPointInside(mouseX, mouseY)) {
			scrolling = true;
			return true;
		} else
			scrolling = false;

		if (getObjectBounds(p).isPointInside(mouseX, mouseY)) {
			p.onObjectClicked(this);
			return true;
		}
		return false;
	}

	@Override
	public void keyTyped(Window p, char charICTOR, int key) {

	}

	@Override
	public Bounds getObjectBounds(Window p) {
		return new Bounds(p.getX() + getX(), p.getY() + getY(),
				isScaleWidthToWindow() ? (p.getWidth() - 2) - getX() : getWidth(),
				isScaleHeightToWindow() ? (p.getHeight() - getY() - 2)
						: getHeight());
	}

	public int getSliderY(Window p, List list) {
		double percent = ((double) getBegindexWithinList() / (double) (list
				.size() - getMaxRenderableLines()));
		return (int) (percent * ((double) getObjectBounds(p).getHeight() - mc.theClient.getGUIFR().getFont().getStringHeight("|")));
	}

	public Bounds getSliderBounds(Window p) {
		List<String> list = formattedStrings(p, strings);
		return getSliderBounds(p, list);
	}

	public Bounds getSliderBounds(Window p, List<String> list) {
		Bounds box = getObjectBounds(p);
		return new Bounds(box.getX() + box.getWidth() - 7, box.getY()
				+ getSliderY(p, list) + 2, 5, 8);
	}

	public List<String> formattedStrings(Window p,
			CopyOnWriteArrayList<String> strings) {
		List<String> l = new ArrayList<String>();
		for (String s : strings) {
			List<String> goml = mc.theClient.getGUIFR().wrapWords(s,
					getObjectBounds(p).getWidth());
			for (String s1 : goml) {
				l.add(s1);
			}
		}
		return l;
	}

	public void append(String args) {
		boolean as = false;
		if (isAutoScroll()) {
			if (getBegindexWithinList() == getMaxIndex()) {
				as = true;
			}
		}
		if(getStrings().size() > getMaxLines() && getMaxLines() != -1)
			getStrings().remove(0);
		
		getStrings().add(args);
		if (as)
			setBegindexWithinList(getMaxIndex());
	}

	public int getMaxRenderableLines() {
		return (int) ((double) getObjectBounds(getWindow()).getHeight() / mc.theClient.getGUIFR().getFont().getStringHeight("|"));
	}

	public int getMaxIndex() {
		return (formattedStrings(getWindow(), strings).size() - getMaxRenderableLines());
	}

	public int getMaxIndex(List l) {
		return (l.size() - getMaxRenderableLines());
	}
	
	public void trimList() {
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getStringColor() {
		return stringColor;
	}

	public void setStringColor(int stringColor) {
		this.stringColor = stringColor;
	}

	public int getBegindexWithinList() {
		return begindexWithinList;
	}

	public void setBegindexWithinList(int begindexWithinList) {
		List<String> list = formattedStrings(getWindow(), strings);
		this.setBegindexWithinList(begindexWithinList, list);
	}

	public void setBegindexWithinList(int begindexWithinList, List<String> list) {
		this.begindexWithinList = begindexWithinList;

		if (!keepTextRendered(list))
			return;
	}
	public boolean keepTextRendered(List list) {
		if (begindexWithinList < 0)
			begindexWithinList = 0;
		if (list.size() - getMaxRenderableLines() > 0) {
			if (begindexWithinList > list.size() - getMaxRenderableLines())
				begindexWithinList = list.size() - getMaxRenderableLines();
			return true;
		} else {
			begindexWithinList = 0;
		}
		return false;
	}
	
	public void clearLines() {
		this.strings.clear();
	}

	public CopyOnWriteArrayList<String> getStrings() {
		return strings;
	}

	public void setStrings(CopyOnWriteArrayList<String> strings) {
		this.strings = strings;
	}

	public boolean isScrolling() {
		return scrolling;
	}

	public void setScrolling(boolean scrolling) {
		this.scrolling = scrolling;
	}

	public boolean isAutoScroll() {
		return autoScroll;
	}

	public void setAutoScroll(boolean autoScroll) {
		this.autoScroll = autoScroll;
	}

	public boolean isScaleHeightToWindow() {
		return scaleHeightToWindow;
	}

	public void setScaleHeightToWindow(boolean scaleHeightToWindow) {
		this.scaleHeightToWindow = scaleHeightToWindow;
	}

	public boolean isScaleWidthToWindow() {
		return scaleWidthToWindow;
	}

	public void setScaleWidthToWindow(boolean scaleWidthToWindow) {
		this.scaleWidthToWindow = scaleWidthToWindow;
	}

	public int getMaxLines() {
		return maxLines;
	}

	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines;
	}

	public boolean isRenderBackground() {
		return renderBackground;
	}

	public void setRenderBackground(boolean renderBackground) {
		this.renderBackground = renderBackground;
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		// TODO Auto-generated method stub
		return null;
	}
	
}