package net.halalaboos.client.ui.window.elements;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.Bounds;
import net.minecraft.src.*;

public class ItemButton extends WElement {
	private int x, y, width, height;
	private boolean enabled = false;
	private ItemStack item;
	
	public ItemButton(int itemID, int x, int y, int w, int h) {
		this(new ItemStack(Item.itemsList[itemID]), x, y, w, h);
	}

	public ItemButton(ItemStack item, int x, int y, int w, int h) {
		this.item = item;
		this.x = x;
		this.y = y;
		this.width = 20;
		this.height = 20;
	}
	
	@Override
	public boolean onMouseClicked(Window p, int mouseX, int mouseY) {
		if (getObjectBounds(p).isPointInside(mouseX, mouseY)) {
			p.onObjectClicked(this);
			return true;
		} else
			return false;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int getObjectWidth() {
		// TODO Auto-generated method stub
		return x + width + 2;
	}

	@Override
	public int getObjectHeight() {
		// TODO Auto-generated method stub
		return y + height + 2;
	}

	@Override
	public void render(Window p, WindowTheme wTheme, boolean isMouseAround) {

		wTheme.renderElement(p.getX() + x, p.getY() + y, width,
				height, isMouseAround, enabled);
		if(item == null)
			return;
		RenderItem itemRenderer = new RenderItem();
		GL11.glPushMatrix();
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDepthMask(true);
		itemRenderer.renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, item, p.getX() + x + 2, p.getY() + y + 2);
		itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, item, p.getX() + x + 2, p.getY() + y + 2);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	@Override
	public Bounds getObjectBounds(Window p) {
		// TODO Auto-generated method stub
		return new Bounds(p.getX() + x, // x
				p.getY() + y, // y
				width, // x1
				height// y1
		);
	}

	public void keyTyped(Window p, char charICTOR, int key) {

	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getToolTipText(Window window, WindowTheme theme) {
		if(item == null)
			return null;
		return item.getDisplayName() + " (" + item.itemID + ")";
	}

	public ItemStack getItem() {
		return item;
	}

	public void setItem(ItemStack item) {
		this.item = item;
	}
	
	public int getItemID() {
		return item.itemID;
	}

}
