package net.halalaboos.client.ui.window.theme;

import net.halalaboos.client.Client;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.RenderUtils;
import net.halalaboos.client.utils.Texture;

public class Default extends WindowTheme {

	@Override
	public void renderElement(double x, double y, double width, double height,  boolean mouseAround, boolean isEnabled) {
//		Client.getRenderUtils().drawHalRect(x, y, x + width, y + height, mouseAround, isEnabled);
		int bCo = 0;
			if(isEnabled)
				bCo = mouseAround ? 0x8f00B3FF : 0x9f00B3FF;
			else
				bCo = mouseAround ? 0x8f5E5E5E : 0x9f5E5E5E;
			
		Client.getRenderUtils().drawRect(x, y, x + width, y + height, bCo);
	}

	@Override
	public void renderPanel(double x, double y, double width, double height) {
		Client.getRenderUtils().drawRect(x, y, x + width, y + height, 0x9f000000);
//		Client.getRenderUtils().drawBorderedRect(x, y, x + width, y + height, 1f, 0.1F,  0.1F,  0.1F, 0.75f);
	}

	@Override
	public void renderToolTip(String text, double x, double y) {
		double width =  Client.getGUIFR().getStringWidth(text) + 4,
		height = 12;
		Client.getRenderUtils().drawRect(x, y, x + width, y + height, 0, 0, 0, 0.3f);
		Client.getGUIFR().drawStringWithShadow(text, x + 2, y + 2, 0xffffbb);		
	}

}
