package net.halalaboos.client.ui;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.Client;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.utils.RenderUtils;
import net.minecraft.client.Minecraft;

public abstract class Gui {
	protected Minecraft mc = Client.getMC();
	protected RenderUtils renderUtils = Client.getRenderUtils();
	protected Type type;
	protected static List<Gui> guis = new ArrayList<Gui>();
	
	public Gui(Type type) {
		this.type = type;
		guis.add(this);
	}
	
	public boolean isType(Type type) {
		return this.type.equals(type);
	}
	
	public abstract void render(int screenWidth, int screenHeight);

	public static List<Gui> getGuis() {
		return guis;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public static void setGuis(List<Gui> guis) {
		Gui.guis = guis;
	}
	
	public enum Type {
		TABBED("Tabbed"), 
		DEFAULT("Default"),
		OLDFASHIONED("Old Fashioned"),
		PAGED("Paged"),
		INFORMATIVE("Informative"),
		VANILLA("Vanilla");
		
		protected String name;
		Type(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
	}
}
