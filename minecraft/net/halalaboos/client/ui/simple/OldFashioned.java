package net.halalaboos.client.ui.simple;

import net.halalaboos.client.Client;
import net.halalaboos.client.base.Value;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.manager.ValueManager;
import net.halalaboos.client.ui.Gui.Type;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.Gui;

public class OldFashioned extends Gui {

	public OldFashioned() {
		super(Type.OLDFASHIONED);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Should we also make this show the coordinates?
	 */

	@Override
	public void render(int screenWidth, int screenHeight) {
		int yPos = 2;
		mc.fontRenderer.drawStringWithShadow(Client.CLIENT_TITLE + 
				" (FPS: " + mc.debug.split(" ")[0] + ")", 2, yPos, 0xFFFFFF);
		yPos += 10;
		for(Mod m : ModManager.getMods()) {
			if(m.isRenderIngame() && !m.getKeyName().equals("-1")) {
				mc.fontRenderer.drawStringWithShadow(m.getKeyName() + ": " + m.getName(), 2, yPos, m.isState() ? 0x339900:0x990000);
				yPos += 9;
			}
		}
	}

}
