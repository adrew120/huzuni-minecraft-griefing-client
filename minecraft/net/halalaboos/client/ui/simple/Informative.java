package net.halalaboos.client.ui.simple;

import org.lwjgl.opengl.GL11;

import net.halalaboos.client.Client;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.ui.Gui.Type;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.Gui;
import net.halalaboos.client.utils.*;

public class Informative extends Gui {

	/*
	 * Old PhoenixLABs gui look
	 */
	public Informative() {
		super(Type.INFORMATIVE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(int screenWidth, int screenHeight) {
		String coords = "XYZ: " + (int)mc.thePlayer.posX + ", " + (int)mc.thePlayer.posY + ", " + (int)mc.thePlayer.posZ;
		String biome = "Biome: " + Client.getClientUtils().getBiome();
		String time = "Time: "+ Client.getClientUtils().getTime();
		String fps = "FPS: " + Client.getClientUtils().getFPS();	
		int datFps = mc.debugFPS;
		int width = 80;
		int yPos = 1;	
		int[] arrayHeight = getArrayHeight();		
		if(mc.fontRenderer.getStringWidth(coords) > width) width = mc.fontRenderer.getStringWidth(coords)+5;
		if(mc.fontRenderer.getStringWidth(biome) > width) width = mc.fontRenderer.getStringWidth(biome)+5;
		if(mc.fontRenderer.getStringWidth(time) > width) width = mc.fontRenderer.getStringWidth(time)+5;
		mc.theClient.getRenderUtils().drawRect(0, 0, width, 40, 0x9f000000);		
		mc.fontRenderer.drawStringWithShadow(coords, 0, yPos, 0xffffff); yPos += 9;		
		mc.fontRenderer.drawStringWithShadow(biome, 0, yPos, 0xffffff); yPos += 9;		
		mc.fontRenderer.drawStringWithShadow(time, 0, yPos, 0xffffff); yPos += 9;		
		mc.fontRenderer.drawStringWithShadow(fps, 0, yPos, 0xffffff); yPos += 9; 		
		Client.getRenderUtils().drawRect(0, yPos, width, yPos+2, 0x5f000000);
		if(datFps > width) datFps = width;
		Client.getRenderUtils().drawRect(0, yPos, datFps, yPos+2, 0x8fff00ff); yPos+=4;		
		mc.theClient.getRenderUtils().drawRect(0, yPos, width, yPos+2, 0xffffff00);
		mc.theClient.getRenderUtils().drawRect(0, 42, width, yPos+arrayHeight[0], 0x8f000000); yPos+=4;		
		for(Mod m : ModManager.getMods()){
			if(m.isState() && m.isRenderIngame()){
				mc.fontRenderer.drawStringWithShadow(m.getName(), 0, yPos, 0x525252);
				yPos+=9;
			}
		}
	}

	private int[] getArrayHeight() {
		int yPos = 4, count = 0;
		for(Mod mod : ModManager.getMods()) {
			if(mod.isRenderIngame() && mod.isState()) {
				yPos += 9;
				count++;
			}
		}
		return new int[] { yPos };
	}

}
