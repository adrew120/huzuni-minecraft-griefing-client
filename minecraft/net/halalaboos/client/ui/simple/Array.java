package net.halalaboos.client.ui.simple;

import net.halalaboos.client.Client;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.ui.Gui.Type;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.Gui;

public class Array extends Gui {

	public Array() {
		super(Type.DEFAULT);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(int screenWidth, int screenHeight) {
		int yPos = 2;
		mc.fontRenderer.drawStringWithShadow(Client.CLIENT_TITLE, 2, 2, 0xffffff);
		for(Mod m : ModManager.getMods()) {
			if(m.isRenderIngame() && m.isState()) {
				int color = m.type == null ? 0xffff44:m.getType().getColor();
				mc.fontRenderer.drawStringWithShadow(m.getName(), mc.theClient.getClientUtils().getScreenWidth() - mc.fontRenderer.getStringWidth(m.getName()) - 2, yPos, color);
				yPos += 10;
			}
		}
	}

}
