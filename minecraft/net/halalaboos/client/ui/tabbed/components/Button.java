package net.halalaboos.client.ui.tabbed.components;

import net.halalaboos.client.Client;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.RenderUtils;

public class Button {
	protected Mod mod;
	protected RenderUtils renderUtils = Client.getRenderUtils();
	public Button(Mod mod) {
		this.mod = mod;
	}
	
	public void render(boolean selected, int posX, int posY, int width) {
		Client.getRenderUtils().drawRect(posX - 1, posY - 1, posX + width + 1, posY + 12, 0x30FFFFFF);
		Client.getRenderUtils().drawRect(posX, posY, posX + width, posY + 11, 0x99000000);
		Client.getMC().fontRenderer.drawStringWithShadow(selected ? "\247a" + mod.getName() : mod.getName(), 
				posX + 2, posY + 2, 0xFFFFFF);
	}
	
	public void toggle() {
		mod.toggle();
	}

	public Mod getMod() {
		return mod;
	}
	
}
