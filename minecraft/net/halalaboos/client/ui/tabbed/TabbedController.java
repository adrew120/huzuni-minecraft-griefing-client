package net.halalaboos.client.ui.tabbed;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.input.Keyboard;

import net.halalaboos.client.Client;
import net.halalaboos.client.base.Binding;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventKeyPressed;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.ui.Gui.Type;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.Gui;
import net.halalaboos.client.ui.tabbed.components.Button;
import net.halalaboos.client.ui.tabbed.components.Tab;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.RenderUtils;
import net.minecraft.client.Minecraft;

public class TabbedController extends Gui {
	private List<Tab> tabs;
	private Input up, down, right, left, enter;
	private Tab selectedTab = null;
	private int maxWidth;
	
	public TabbedController() {
		super(Type.TABBED);
		tabs = new ArrayList<Tab>();
		up = new Input("UP", Keyboard.KEY_UP);
		down = new Input("DOWN", Keyboard.KEY_DOWN);
		left = new Input("LEFT", Keyboard.KEY_LEFT);
		right = new Input("RIGHT", Keyboard.KEY_RIGHT);
		enter = new Input("ENTER", Keyboard.KEY_RETURN);
	}
	
	public void initialize() {
		for(WindowCategory category : WindowCategory.values()) {
			Tab tab = new Tab(category.getName());
			if(selectedTab == null)
				selectedTab = tab;
			for(Mod mod : ModManager.getMods()) {
				if(mod.isRenderIngame() && mod.category == category) {
					tab.add(new Button(mod));
				}
			}
			if(!tab.isEmpty())
				tabs.add(tab);
		}
		int maxWidth = 0;
		for(Tab tab : tabs) {
			if(Client.getGUIFR().getStringWidth(tab.getTitle()) > maxWidth)
				maxWidth = Client.getGUIFR().getStringWidth(tab.getTitle()) * 4;
		}
		this.maxWidth = maxWidth;
	}

	@Override
	public void render(int screenWidth, int screenHeight) {
		int[] dimensions = getArrayHeight();

		int startY = 2;
		int startX = 2;
		int width = maxWidth;
		Client.getRenderUtils().drawRect(startX, startY, startX + width, startY + (tabs.size() * 13) + 23, 0x30FFFFFF);
		Client.getRenderUtils().drawRect(startX + 2, startY + 2, startX + width - 2, startY + 16,  0x99000000);
		mc.fontRenderer.drawStringWithShadow("\247a" + Client.CLIENT_TITLE + "\247f " + Client.CLIENT_VERSION, 
				startX + 5, startY + 5, 0xFFFFFF);
		startY += 15;
		for(Tab tab : tabs) {
			int index = tabs.indexOf(tab);
			tab.render(selectedTab == tab, startX + 2, startY + index + 3, width - 4);
			startY += 13;
		}
		startY += 13;

		mc.theClient.getRenderUtils().drawRect(startX, startY - 4, startX + width, startY + dimensions[0], 0x30FFFFFF);
		for(Mod m : ModManager.getMods()){
			if(m.isState() && m.isRenderIngame()){
				mc.theClient.getRenderUtils().drawRect(startX + 2, startY - 1, startX + width - 2, startY+10, 0x99000000);
				mc.fontRenderer.drawStringWithShadow(m.getName(), startX + 4, startY + 1, 0xffffff);
				startY += 12;
			}
		}
		
	}
	
	private int[] getArrayHeight() {
		int yPos = 0, count = 0;
		for(Mod mod : ModManager.getMods()) {
			if(mod.isRenderIngame() && mod.isState()) {
				yPos += 12;
				count++;
			}
		}
		return new int[] { yPos };
	}
	
	private void recieveUserInput(Input keyBind) {
		if(selectedTab != null) {
			if(selectedTab.isOpened()) {
				selectedTab.recieveInput(keyBind.getName());
				return;
			} else {
				if(keyBind == up)
					movePosition(-1);
				else if(keyBind == down)
					movePosition(1);
				else if(keyBind == right || keyBind == left)
					selectedTab.toggleOpen();
			}
		}
	}
	
	public void movePosition(int ammount) {
		int index = tabs.indexOf(selectedTab) + ammount;
		if(index > tabs.size() - 1)
			index = 0;
		else if(index < 0)
			index = tabs.size() - 1;
		selectedTab = tabs.get(index);
	}
	
	private class Input extends Binding {

		public Input(String name, int keyCode) {
			super(name, keyCode);
			mc.theClient.getEventHandler().registerListener(EventKeyPressed.class, this);
		}

		@Override
		public void onPressed() {
			if(GuiManager.getSelectedGUI().getType() == Type.TABBED)
				recieveUserInput(this);
		}
		
	}
	
}
