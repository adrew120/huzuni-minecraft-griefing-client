package net.halalaboos.client.screen;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.utils.Account;
import net.minecraft.src.*;

public class GuiHuzuniConnect extends GuiScreen {

    private GuiTextField serverIP;
	private GuiScreen prevScreen;
	
	public GuiHuzuniConnect(GuiScreen prevScreen) {
        this.prevScreen = prevScreen;

	}
    public void updateScreen() {
        this.serverIP.updateCursorCounter();
    }
    
    public void initGui() {
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, "Connect"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, "Cancel"));
        this.serverIP = new GuiTextField(this.fontRenderer, this.width / 2 - 100, 66, 200, 20);
        this.serverIP.setFocused(true);
        this.serverIP.setMaxStringLength(128);
        this.serverIP.setText(SettingsFile.getHuzuniServer());
        if(mc.theClient.getNetworkingClient().isConnected()) {
        	((GuiButton)buttonList.get(0)).displayString = "Disconnect";
        	serverIP.setEnabled(false);
        	serverIP.setFocused(false);
        }
    }
    
    protected void actionPerformed(GuiButton par1GuiButton) {
    	if(par1GuiButton.id == 0) {
    		 if(mc.theClient.getNetworkingClient().isConnected()) {
    			mc.theClient.getNetworkingClient().stop();
		    	mc.displayGuiScreen(prevScreen);
    		 } else {
    	    		if(serverIP.getText().length() > 0) {
    	    			int port = serverIP.getText().contains(":") ? Integer.parseInt(serverIP.getText().split(":")[1]):6969;
    	    			String ip = serverIP.getText().contains(":") ? serverIP.getText().split(":")[0]:serverIP.getText();
    	    			try {
    						mc.theClient.getNetworkingClient().connect(ip, port);
    			    		mc.displayGuiScreen(prevScreen);
    					} catch (IOException e) {
    						e.printStackTrace();
    					}
    	    		} 
    		 }
    	} else if(par1GuiButton.id == 1)
    		mc.displayGuiScreen(prevScreen);
    	
    }
    
    public void keyTyped(char ch, int key) {
    	super.keyTyped(ch, key);
        this.serverIP.textboxKeyTyped(ch, key);

        if (key == Keyboard.KEY_RETURN)
            this.actionPerformed((GuiButton)this.buttonList.get(0));
        
        ((GuiButton)this.buttonList.get(0)).enabled = this.serverIP.getText().length() > 0;
    }
    
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        this.serverIP.mouseClicked(par1, par2, par3);
    }
    
    public void drawScreen(int par1, int par2, float par3) {
        StringTranslate var4 = StringTranslate.getInstance();
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, "Connect to a " + Client.CLIENT_TITLE + " server.", this.width / 2, 17, 16777215);
        this.drawString(this.fontRenderer, "Server IP", this.width / 2 - 100, 53, 10526880);
        this.serverIP.drawTextBox();
        super.drawScreen(par1, par2, par3);
        ((GuiButton)this.buttonList.get(0)).enabled = this.serverIP.getText().length() > 0;

    }
}
