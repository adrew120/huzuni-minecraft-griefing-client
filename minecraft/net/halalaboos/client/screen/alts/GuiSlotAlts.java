package net.halalaboos.client.screen.alts;

import java.awt.Color;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import org.lwjgl.opengl.GL11;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.utils.Account;
import net.halalaboos.client.utils.Texture;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

class GuiSlotAlts extends GuiSlot {

	static Texture background;
	private Minecraft mc = Client.getMC();
	private ArrayList list;
	private GuiScreen screen;
	private int currentSlot = -1;

	public GuiSlotAlts(GuiScreen par1GuiLanguage) {
		super(Client.getMC(), par1GuiLanguage.width,
				par1GuiLanguage.height, 32, (par1GuiLanguage.height - 65) + 4,
				24);

		if(background == null)
			background = new Texture("deadfetus.jpg");
		screen = par1GuiLanguage;
		setList(AccountFile.getAccounts());
	}

	/**
	 * Gets the size of the current slot list.
	 */
	protected int getSize() {
		return getList().size();
	}
	
	/**
	 * the element in the slot that was clicked, boolean for whether it was
	 * double clicked or not
	 */
	protected void elementClicked(int par1, boolean par2) {
		currentSlot = par1;

		if (par2 && par1 < getList().size()) {
			Account account = ((Account) getList().get(par1));
			String[] login = HttpUtil.loginToMinecraft(null, account.getUsername(), account.getPassword());
			if(login != null) {
				mc.session.username = login[0];
				mc.session.sessionId = login[1];
			}
		}
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	protected boolean isSelected(int par1) {
		return par1 == currentSlot;
	}

	/**
	 * return the height of the content being scrolled
	 */
	protected int getContentHeight() {
		return getSize() * 24;
	}

	protected void drawBackground() {
//		background.renderTexture(0, 0, this.width, this.height, Color.white);
	}

	protected void drawSlot(int par1, int par2, int par3, int par4,
			Tessellator par5Tessellator) {
		Account a = (Account) getList().get(par1);

		Client.getFR().drawCenteredString("\247f" + a.getUsername(), screen.width / 2, par3 + 1, 0xffffff);

		String s = "";

		for (int l = 0; l < a.getPassword().length(); l++)
			s += "*";

		Client.getFR().drawCenteredString("\247b" + s, screen.width / 2, par3 + 13, 0xffffff);
		
		/*
		 * GL11.glPushMatrix(); GL11.glBindTexture(GL11.GL_TEXTURE_2D,
		 * mc.renderEngine
		 * .getTextureForDownloadableImage("http://s3.amazonaws.com/MinecraftSkins/"
		 * + a.username + ".png", a.username+".png")); GL11.glColor4f(1.0F,
		 * 1.0F, 1.0F, 1.0F); mc.ingameGUI.drawTexturedModalRect(screen.width /
		 * 2 + 75, par3, 0, 0, 32, 30); GL11.glPopMatrix();
		 */
	}

	public int getCurrentSlot() {
		return currentSlot;
	}
	
	public void removeAccount() {
		if (currentSlot != -1) {
			AccountFile.getAccounts().remove(currentSlot);
			setList(AccountFile.getAccounts());
		}
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public ArrayList getList() {
		return list;
	}
}
