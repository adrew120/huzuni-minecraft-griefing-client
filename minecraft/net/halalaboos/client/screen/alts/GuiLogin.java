package net.halalaboos.client.screen.alts;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.utils.Account;
import net.minecraft.src.*;

public class GuiLogin extends GuiScreen {

    private GuiTextField username;
    private GuiTextField password;
	private GuiScreen prevScreen;
	private boolean addToList;
	
	public GuiLogin(GuiScreen prevScreen, boolean addToList) {
        this.prevScreen = prevScreen;
        this.addToList = addToList;

	}
    public void updateScreen() {
        this.username.updateCursorCounter();
        this.password.updateCursorCounter();
    }
    
    public void initGui() {
    	Keyboard.enableRepeatEvents(true);
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, addToList ? "Add":"Login"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, "Cancel"));
        this.username = new GuiTextField(this.fontRenderer, this.width / 2 - 100, 66, 200, 20);
        this.username.setFocused(true);
        this.username.setMaxStringLength(128);
        this.username.setText("");
        this.password = new GuiTextField(this.fontRenderer, this.width / 2 - 100, 106, 200, 20);
        this.password.setMaxStringLength(128);
        this.password.setText("");
    }
    protected void actionPerformed(GuiButton par1GuiButton) {
    	if(par1GuiButton.id == 0) {
    		if(addToList) {
    			AccountFile.getAccounts().add(new Account(username.getText(), password.getText(), true));
    			mc.displayGuiScreen(prevScreen);
    		} else {
    			if(username.getText().length() > 0 && password.getText().length() > 0) {
    				String[] login = HttpUtil.loginToMinecraft(null, username.getText(), password.getText());
    				if(login != null) {
    					mc.session.username = login[0];
    					mc.session.sessionId = login[1];
    					mc.displayGuiScreen(prevScreen);
    				}
    			}
    		}
    	} else if(par1GuiButton.id == 1) {
    		mc.displayGuiScreen(prevScreen);
    	}
    }
    
    public void keyTyped(char ch, int key) {
    	super.keyTyped(ch, key);
        this.username.textboxKeyTyped(ch, key);
        this.password.textboxKeyTyped(ch, key);

        if (key == Keyboard.KEY_TAB)
        {
            if (this.username.isFocused())
            {
                this.username.setFocused(false);
                this.password.setFocused(true);
            }
            else
            {
                this.username.setFocused(true);
                this.password.setFocused(false);
            }
        }

        if (key == 13)
        {
            this.actionPerformed((GuiButton)this.buttonList.get(0));
        }
        
        ((GuiButton)this.buttonList.get(0)).enabled = this.username.getText().length() > 3;
    }
    
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        this.username.mouseClicked(par1, par2, par3);
        this.password.mouseClicked(par1, par2, par3);
    }
    
    public void drawScreen(int par1, int par2, float par3) {
        StringTranslate var4 = StringTranslate.getInstance();
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, addToList ? "Add an account.":"Login with an account.", this.width / 2, 17, 16777215);
        this.drawString(this.fontRenderer, "Username", this.width / 2 - 100, 53, 10526880);
        this.drawString(this.fontRenderer, "Password", this.width / 2 - 100, 94, 10526880);
        this.username.drawTextBox();
        this.password.drawTextBox();
        super.drawScreen(par1, par2, par3);
        mc.fontRenderer.drawStringWithShadow(mc.session.username, 2, 2, 0xff0000);
        ((GuiButton)this.buttonList.get(0)).enabled = this.username.getText().length() > 3;

    }
}
