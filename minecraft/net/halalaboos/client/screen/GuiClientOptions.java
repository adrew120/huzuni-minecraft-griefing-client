package net.halalaboos.client.screen;

import net.halalaboos.client.screen.binder.GuiKeybinds;
import net.halalaboos.client.screen.styler.GuiStyler;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;

public class GuiClientOptions extends GuiScreen {
	private GuiScreen lastScreen;
	
	public GuiClientOptions(GuiScreen lastScreen) {
		this.lastScreen = lastScreen;
	}
    public void initGui() {
        byte flapjack = -16;
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 24 + flapjack, "Keybind Manager"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 46 + flapjack, "GUI Style"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 50,  this.height / 4 + 100 + flapjack, 98, 20, "Back"));
    }
    
    protected void actionPerformed(GuiButton button) {
    	switch(button.id) {
    	case 0:
    		mc.displayGuiScreen(new GuiKeybinds(this));
    		break;
    		
    	case 1:
    		mc.displayGuiScreen(new GuiStyler(this));
    		break;
    		
    	case 2:
    		mc.displayGuiScreen(lastScreen);
    		break;
    		
    	default:
    		break;
    	}
    }
    public void drawScreen(int par1, int par2, float par3)
    {
    	drawDefaultBackground();
    	super.drawScreen(par1, par2, par3);
    }
	
}
