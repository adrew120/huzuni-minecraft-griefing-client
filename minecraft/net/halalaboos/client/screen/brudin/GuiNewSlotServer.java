package net.halalaboos.client.screen.brudin;

import net.halalaboos.client.Client;
import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;
import net.minecraft.src.*;

public class GuiNewSlotServer extends GuiSlot
{
    /** Instance to the GUI this list is on. */
    final GuiNewMultiplayer parentGui;

    public GuiNewSlotServer(GuiNewMultiplayer par1GuiNewMultiplayer)
    {
        super(par1GuiNewMultiplayer.mc, par1GuiNewMultiplayer.width, par1GuiNewMultiplayer.height, 32, par1GuiNewMultiplayer.height - 64, 36);
        this.parentGui = par1GuiNewMultiplayer;
    }

    /**
     * Gets the size of the current slot list.
     */
    protected int getSize()
    {
        return GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers() + GuiNewMultiplayer.getListOfLanServers(this.parentGui).size() + 1;
    }

    /**
     * the element in the slot that was clicked, boolean for wether it was double clicked or not
     */
    protected void elementClicked(int par1, boolean par2)
    {
        if (par1 < GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers() + GuiNewMultiplayer.getListOfLanServers(this.parentGui).size())
        {
            int var3 = GuiNewMultiplayer.getSelectedServer(this.parentGui);
            GuiNewMultiplayer.getAndSetSelectedServer(this.parentGui, par1);
            ServerData var4 = GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers() > par1 ? GuiNewMultiplayer.getInternetServerList(this.parentGui).getServerData(par1) : null;
            boolean var5 = GuiNewMultiplayer.getSelectedServer(this.parentGui) >= 0 && GuiNewMultiplayer.getSelectedServer(this.parentGui) < this.getSize() && (var4 == null || var4.field_82821_f == 61);
            boolean var6 = GuiNewMultiplayer.getSelectedServer(this.parentGui) < GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers();
            GuiNewMultiplayer.getButtonSelect(this.parentGui).enabled = var5;
            GuiNewMultiplayer.getButtonEdit(this.parentGui).enabled = var6;
            GuiNewMultiplayer.getButtonDelete(this.parentGui).enabled = var6;

            if (par2 && var5)
            {
                GuiNewMultiplayer.func_74008_b(this.parentGui, par1);
            }
            else if (var6 && GuiScreen.isShiftKeyDown() && var3 >= 0 && var3 < GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers())
            {
                GuiNewMultiplayer.getInternetServerList(this.parentGui).swapServers(var3, GuiNewMultiplayer.getSelectedServer(this.parentGui));
            }
        }
    }

    /**
     * returns true if the element passed in is currently selected
     */
    protected boolean isSelected(int par1)
    {
        return par1 == GuiNewMultiplayer.getSelectedServer(this.parentGui);
    }

    /**
     * return the height of the content being scrolled
     */
    protected int getContentHeight()
    {
        return this.getSize() * 36;
    }

    protected void drawBackground()
    {
        this.parentGui.drawDefaultBackground();
    }

    protected void drawSlot(int par1, int par2, int par3, int par4, Tessellator par5Tessellator)
    {
        if (par1 < GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers())
        {
            this.func_77247_d(par1, par2, par3, par4, par5Tessellator);
        }
        else if (par1 < GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers() + GuiNewMultiplayer.getListOfLanServers(this.parentGui).size())
        {
            this.func_77248_b(par1, par2, par3, par4, par5Tessellator);
        }
        else
        {
            this.func_77249_c(par1, par2, par3, par4, par5Tessellator);
        }
    }

    private void func_77248_b(int par1, int par2, int par3, int par4, Tessellator par5Tessellator)
    {
        LanServer var6 = (LanServer)GuiNewMultiplayer.getListOfLanServers(this.parentGui).get(par1 - GuiNewMultiplayer.getInternetServerList(this.parentGui).countServers());
        Client.getFR().drawStringWithShadow(StatCollector.translateToLocal("lanServer.title"), par2 + 2, par3 + 1, 16777215);
        Client.getFR().drawStringWithShadow(var6.getServerMotd(), par2 + 2, par3 + 12, 8421504);

        if (Client.getMC().gameSettings.hideServerAddress)
        {
            Client.getFR().drawStringWithShadow(StatCollector.translateToLocal("selectServer.hiddenAddress"), par2 + 2, par3 + 12 + 11, 3158064);
        }
        else
        {
        	Client.getFR().drawStringWithShadow(var6.getServerIpPort(), par2 + 2, par3 + 12 + 11, 3158064);
//            Client.getFR().drawStringWithShadow(var6.getServerIpPort(), par2 + 2, par3 + 12 + 11, 3158064);
        }
    }

    private void func_77249_c(int par1, int par2, int par3, int par4, Tessellator par5Tessellator)
    { 
    	Client.getFR().drawCenteredString(StatCollector.translateToLocal("lanServer.scanning") + "...", this.parentGui.width / 2, par3 + 10, 16777215);
    
//        this.parentGui.drawCenteredString(Client.getWrapper().getMC().fontRenderer, StatCollector.translateToLocal("lanServer.scanning"), this.parentGui.width / 2, par3 + 1, 16777215);
        String var6;

        switch (GuiNewMultiplayer.getTicksOpened(this.parentGui) / 3 % 4)
        {
            case 0:
            default:
                var6 = "O o o";
                break;

            case 1:
            case 3:
                var6 = "o O o";
                break;

            case 2:
                var6 = "o o O";
        }      
     
    }

    private void func_77247_d(int par1, int par2, int par3, int par4, Tessellator par5Tessellator)
    {
        ServerData var6 = GuiNewMultiplayer.getInternetServerList(this.parentGui).getServerData(par1);

        synchronized (GuiNewMultiplayer.getLock())
        {
            if (GuiNewMultiplayer.getThreadsPending() < 5 && !var6.field_78841_f)
            {
                var6.field_78841_f = true;
                var6.pingToServer = -2L;
                var6.serverMOTD = "";
                var6.populationInfo = "";
                GuiNewMultiplayer.increaseThreadsPending();
                (new NewThreadPollServers(this, var6)).start();
            }
        }

        boolean var7 = var6.field_82821_f > 61;
        boolean var8 = var6.field_82821_f < 61;
        boolean var9 = var7 || var8;
    	Client.getFR().drawStringWithShadow(var6.serverName, par2 + 2, par3 + 1, 16777215);
//        Client.getFR().drawStringWithShadow(var6.serverName, par2 + 2, par3 + 1, 16777215);
    	Client.getFR().drawStringWithShadow(var6.serverMOTD, par2 + 2, par3 + 12, 8421504);
//        Client.getFR().drawStringWithShadow(var6.serverMOTD, par2 + 2, par3 + 12, 8421504);
    	Client.getFR().drawStringWithShadow(var6.populationInfo, par2 + 215 - Client.getFR().getStringWidth(var6.populationInfo), par3 + 12, 8421504);
//    	Client.getFR().drawStringWithShadow(var6.populationInfo, par2 + 215 - Client.getWrapper().getMC().fontRenderer.getStringWidth(var6.populationInfo), par3 + 12, 8421504);

        if (var9)
        {
            String var10 = EnumChatFormatting.DARK_RED + var6.gameVersion;
        	Client.getFR().drawStringWithShadow(var10, par2 + 200 - Client.getFR().getStringWidth(var10), par3 + 1, 8421504);
//            Client.getFR().drawStringWithShadow(var10, par2 + 200 - Client.getWrapper().getMC().fontRenderer.getStringWidth(var10), par3 + 1, 8421504);
        }

        if (!Client.getMC().gameSettings.hideServerAddress && !var6.isHidingAddress())
        {
        	Client.getFR().drawStringWithShadow(var6.serverIP, par2 + 2, par3 + 12 + 11, 0x555555);
//            Client.getFR().drawStringWithShadow(var6.serverIP, par2 + 2, par3 + 12 + 11, 3158064);
        }
        else
        {
        	Client.getFR().drawStringWithShadow(StatCollector.translateToLocal("selectServer.hiddenAddress"), par2 + 2, par3 + 12 + 11, 0x555555);
//            Client.getFR().drawStringWithShadow(StatCollector.translateToLocal("selectServer.hiddenAddress"), par2 + 2, par3 + 12 + 11, 3158064);
        }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        Client.getMC().renderEngine.bindTexture("/gui/icons.png");
        byte var16 = 0;
        boolean var11 = false;
        String var12 = "";
        int var15;

        if (var9)
        {
            var12 = var7 ? "Client out of date!" : "Server out of date!";
            var15 = 5;
        }
        else if (var6.field_78841_f && var6.pingToServer != -2L)
        {
            if (var6.pingToServer < 0L)
            {
                var15 = 5;
            }
            else if (var6.pingToServer < 150L)
            {
                var15 = 0;
            }
            else if (var6.pingToServer < 300L)
            {
                var15 = 1;
            }
            else if (var6.pingToServer < 600L)
            {
                var15 = 2;
            }
            else if (var6.pingToServer < 1000L)
            {
                var15 = 3;
            }
            else
            {
                var15 = 4;
            }

            if (var6.pingToServer < 0L)
            {
                var12 = "(no connection)";
            }
            else
            {
                var12 = var6.pingToServer + "ms";
            }
        }
        else
        {
            var16 = 1;
            var15 = (int)(Minecraft.getSystemTime() / 100L + (long)(par1 * 2) & 7L);

            if (var15 > 4)
            {
                var15 = 8 - var15;
            }

            var12 = "Polling..";
        }

        this.parentGui.drawTexturedModalRect(par2 + 205, par3, 0 + var16 * 10, 176 + var15 * 8, 10, 8);
        byte var13 = 4;

        if (this.mouseX >= par2 + 205 - var13 && this.mouseY >= par3 - var13 && this.mouseX <= par2 + 205 + 10 + var13 && this.mouseY <= par3 + 8 + var13)
        {
            GuiNewMultiplayer.getAndSetLagTooltip(this.parentGui, var12);
        }
    }
}
