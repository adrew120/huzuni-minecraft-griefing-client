package net.halalaboos.client.screen.brudin;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.*;

import org.lwjgl.Sys;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiLanguage;
import net.minecraft.src.GuiMultiplayer;
import net.minecraft.src.GuiOptions;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiSelectWorld;
import net.minecraft.src.StringTranslate;
import net.minecraft.src.Tessellator;
import net.halalaboos.client.Client;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.screen.ButtonPoop;
import net.halalaboos.client.screen.GuiClientOptions;
import net.halalaboos.client.screen.alts.GuiAlts;
import net.halalaboos.client.utils.ClientUtils;
import net.halalaboos.client.utils.Connection;
import net.halalaboos.client.utils.Counter;
import net.halalaboos.client.utils.SimpleParticleEngine;
import net.halalaboos.client.utils.SnowEngine;
import net.halalaboos.client.utils.Texture;

public class GuiNewMainMenu extends GuiScreen {

	static Texture background, title;
	private static int isUpToDate = -2;
	protected List<String> updatesList = null;
	private int scrolledIndex;
	long ticktack;
	float alphalpha = 0;
	//private SnowEngine snowEngine;

	public GuiNewMainMenu(){

		if(title == null)
			title = new Texture("fuktits.png");
		
		if(updatesList == null) {
			updatesList = new ArrayList<String>();
			try {
				Connection connection = new Connection(new URL("http://halalaboos.net/client/updates"));
				connection.createConnection();
				for(String s : connection.readURL())
					updatesList.add(s.trim());
			} catch (IOException e) {
				updatesList.add("\2474Could not connect to server.");
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
	
	@Override
	public void initGui() {
		StringTranslate var2 = StringTranslate.getInstance();
		int var4 = this.height / 4 + 48;
		buttonList.clear();
		buttonList.add(new ButtonPoop(1, 0, this.height / 2 - 5, 90, 20, var2.translateKey("menu.singleplayer")));
		buttonList.add(new ButtonPoop(2, 0, this.height / 2 + 15, 90, 20, var2.translateKey("menu.multiplayer")));
		buttonList.add(new ButtonPoop(3, 0, this.height / 2 + 35, 90, 20, "Account Manager"));
		buttonList.add(new ButtonPoop(0, 0,this.height / 2 + 55, 90, 20, ("Options")));
		buttonList.add(new ButtonPoop(4, 0, this.height / 2 + 75, 90, 20, var2.translateKey("menu.quit")));
		if(isUpToDate == -2)
			isUpToDate = mc.theClient.getClientUtils().isClientUpToDate();
		
		List<String> tempList = new ArrayList<String>();
		for(String update : updatesList) {
			for(String formattedUpdate : mc.theClient.getUpdateFR().wrapWords(update, ((width) - (width - 100))))
				tempList.add(formattedUpdate);
		}
		updatesList = tempList;
		
		//snowEngine = new SnowEngine(100, 50L);
	}

	@Override
	protected void actionPerformed(GuiButton button) {
		switch(button.id){
		case 0:
			this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
			break;

		case 1:
			this.mc.displayGuiScreen(new GuiSelectWorld(this));
			break;

		case 2:
			if ((new Random().nextInt(100000) + 1) == 1)
				Sys.openURL("http://goo.gl/NTXjm");
			this.mc.displayGuiScreen(new GuiNewMultiplayer(this));
			break;

		case 3:
			this.mc.displayGuiScreen(new GuiAlts(this));
			break;

		case 4:
			this.mc.shutdown();
			break;
		}
	}

	@Override
    public void handleMouseInput() {
    	super.handleMouseInput();
    	if(updatesList != null) {
	    	if(Mouse.hasWheel() && updatesList.size() > 8) {
	        	int movement = 0;
	        	if(Mouse.getEventDWheel() > 0)
	        		movement = -1;
	        	else if(Mouse.getEventDWheel() < 0)
	        		movement = 1;
	        		
	        	scrolledIndex += movement;
	    		if(scrolledIndex > updatesList.size() - 8)
	    			scrolledIndex = updatesList.size() - 8;
	    		else if(scrolledIndex < 0)
	    			scrolledIndex = 0;
	    	}
    	}
    }
	
	@Override
	public void drawScreen(int par1, int par2, float par3) {

		GL11.glColor3f(1, 1, 1);
		Client.drawClientBG();
		//snowEngine.renderParticles();
		//snowEngine.spawnParticles(width, 1, 100);
		
		Client.getRenderUtils().drawGradientRect(0, 0, this.width, this.height, 0,0x5f000000);
        Client.getRenderUtils().drawGradientRect(0, 0, this.width, this.height, 0x2fffffff,0);

		int posX = this.width - 100,
		posY = this.height / 2 - 13;
		mc.theClient.getRenderUtils().drawRect(this.width - 102, 0, this.width, this.height, 0x1ddddddd);
		renderUpdates(posX, 10, this.width);
		
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		title.renderTexture(0, this.height / 2 - 75, 256, 128, Color.white);
		GL11.glDisable(GL11.GL_BLEND);
		
		String notUptoDate = "Client is not up to date!";
		String notWebServer = "Could not connect to webserver.";
		
		if(isUpToDate == -1) {
			mc.theClient.getFR().drawCenteredString(notWebServer, width / 2, height / 2 + 97, 0xffffff);
		}else if(isUpToDate == 1)
			mc.theClient.getUpdateFR().drawString(notUptoDate,  
					width - mc.theClient.getUpdateFR().getStringWidth(notUptoDate), height - 9, 0xDBDBDB);
		super.drawScreen(par1, par2, par3);
		mc.theClient.getFR().drawString("v" + Client.CLIENT_VERSION, 190, height /2-10, 0xdddddd);
	}

	public void renderUpdates(int posX, int posY, int maxWidth) {
		if(updatesList != null) {
			for(int i = 0; i < this.height/12 && scrolledIndex + i < updatesList.size(); i++) {
				String update = updatesList.get(scrolledIndex + i);
				mc.theClient.getUpdateFR().drawString(update.replaceAll("-", ""), posX, posY, 0xA8A8A8);
				posY += 10;
			}
		}
	}
	
	public void updateScreen() {
		//snowEngine.updateParticles(3.1F);
	}
}