package net.halalaboos.client.screen.styler;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import org.lwjgl.Sys;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.Account;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class GuiStyler extends GuiScreen {
	protected GuiScreen parentGui;
	private GuiSlotStyler styleList;

	public GuiStyler(GuiScreen par1GuiScreen) {
		parentGui = par1GuiScreen;
	}

	GuiButton select;

	public void initGui() {
		StringTranslate stringtranslate = StringTranslate.getInstance();
		buttonList.add(new GuiSmallButton(6, width / 2 - 75, height - 22, "Done"));

		styleList = new GuiSlotStyler(this);
		styleList.registerScrollButtons(buttonList, 7, 8);

		select = new GuiSmallButton(1, width / 2 - 37, height - 44, 75, 20, "Select");
		buttonList.add(select);
		select.enabled = styleList.getCurrentSlot() >= 0;
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	protected void actionPerformed(GuiButton par1GuiButton) {
		if (!par1GuiButton.enabled) {
			return;
		}

		switch (par1GuiButton.id) {
		case 6:
			mc.displayGuiScreen(parentGui);
			break;

		case 0:
			break;
			
		case 1:
			if (styleList.getCurrentSlot() >= 0 && styleList.getList().size() > 0)
				styleList.elementClicked(styleList.getCurrentSlot(), true);
			break;

		default:
			styleList.actionPerformed(par1GuiButton);
			break;
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	protected void keyTyped(char par1, int par2) {
		super.keyTyped(par1, par2);
	}

	/**
	 * Called when the mouse is clicked.
	 */
	protected void mouseClicked(int par1, int par2, int par3) {
		super.mouseClicked(par1, par2, par3);
	}

	/**
	 * Called when the mouse is moved or a mouse button is released. Signature:
	 * (mouseX, mouseY, which) which==-1 is mouseMove, which==0 or which==1 is
	 * mouseUp
	 */
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
		super.mouseMovedOrUp(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(int par1, int par2, float par3) {
		styleList.drawScreen(par1, par2, par3);
		StringTranslate stringtranslate = StringTranslate.getInstance();
		drawCenteredString(fontRenderer, "Select GUI Style", width / 2, 16,
				0xffffff);
		drawCenteredString(fontRenderer,
				"Select the GUI style you prefer.", width / 2,
				height - 56, 0x990000);
		super.drawScreen(par1, par2, par3);
		select.enabled = styleList.getCurrentSlot() >= 0;
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen() {
		super.updateScreen();
	}

	public void onGuiClosed() {

	}
}
