package net.halalaboos.client.screen;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.ProgressMonitorInputStream;
import net.minecraft.src.*;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import net.halalaboos.client.Client;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.screen.brudin.GuiNewMainMenu;

public class GuiUpdate extends GuiScreen {
	
	private float percent;
	private String updateVersion, errorCode = "";
	private boolean hasUpdated;
	
	public GuiUpdate(String updateVersion) {
		this.updateVersion = updateVersion;
	}
	
	protected void actionPerformed(GuiButton guibutton) {
		if (guibutton.id == 1) {
			if(!hasUpdated) {
				if(SettingsFile.isFirstTime())
					mc.displayGuiScreen(new GuiFirstTime());
				else
					mc.displayGuiScreen(new GuiNewMainMenu());
			} else
				mc.shutdown();
		} else if (guibutton.id == 0) {
			((ButtonPoop)buttonList.get(1)).enabled = false;
			((ButtonPoop)buttonList.get(0)).enabled = false;
			File newClient = new File(mc.getMinecraftDir() + "/bin", Client.CLIENT_TITLE + "_update.jar");
			try {
				downloadFile(new URL("http://halalaboos.net/client/update.jar"), newClient);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	public void initGui() {
		buttonList.add(new ButtonPoop(0, width / 2 - 102,
				height / 4 + 96 - 12, 98, 20, "Download"));
		buttonList.add(new ButtonPoop(1, width / 2 + 2,
				height / 4 + 96 - 12, 98, 20, "Skip"));
		if(percent != 0 && percent < 1) {
			((ButtonPoop)buttonList.get(0)).enabled = false;
			((ButtonPoop)buttonList.get(1)).enabled = false;
		}
		if(percent == 1) {
			((ButtonPoop)buttonList.get(0)).enabled = false;
			((ButtonPoop)buttonList.get(1)).displayString = "Close";
			((ButtonPoop)buttonList.get(1)).enabled = true;
		}
	}

	public void drawScreen(int i, int j, float f) {
		Client.drawClientBG();	
		Client.getFR().drawCenteredString("Update Available for " + Client.CLIENT_TITLE, width / 2,
				(height / 4 - 60) + 20, 0xFFFFFF);
		Client.getFR().drawCenteredString("Current version: " + Client.CLIENT_VERSION, width / 2,
				(height / 4 - 60) + 80, 0xFFFFFF);
		Client.getFR().drawCenteredString("Latest version: " + updateVersion, width / 2,
				(height / 4 - 60) + 90, 0xFFFFFF);
		int originalX = width / 2 - 100;
		int originalY = height / 4 - 10;
		mc.theClient.getRenderUtils().drawBorderedRect(originalX, originalY, originalX + 200, originalY + 20, 1f, 0.1f, 0.1f, 0.1f, 1);
		mc.theClient.getRenderUtils().drawRect(originalX + 1, originalY + 1, originalX + 2 + (percent * 197), originalY + 19, 0, 1, 1, 1);
		
		if(percent == 1)
			Client.getFR().drawCenteredString("File successfully downloaded!", width / 2, (height / 4 - 60) + 30, 0xFF0000);
		Client.getFR().drawCenteredString(errorCode, width / 2, (height / 4 - 60) + 40, 0xFF0000);

		super.drawScreen(i, j, f);
	}
	
	public void downloadFile(URL websiteURL, File saveFile) {
		Runnable download = new DownloadThread(websiteURL, saveFile);
		new Thread(download).start();
	}
	
	 class DownloadThread implements Runnable {
		 URL websiteURL;
		 File saveFile;
		 DownloadThread(URL websiteURL, File saveFile) {
			this.websiteURL = websiteURL;
			this.saveFile = saveFile;
		 }
		
		@Override
		public void run() {
		    try {
			BufferedInputStream in = new BufferedInputStream(websiteURL.openStream());
			OutputStream  out = new BufferedOutputStream(new FileOutputStream(saveFile));
			byte data[] = new byte[1024];
		    int count;
			HttpURLConnection connection = (HttpURLConnection) websiteURL.openConnection();
			int filesize = connection.getContentLength();
			connection.disconnect();
			float progress = 0;
		    if(saveFile.exists())
		    	
		    while((count = in.read(data, 0, 1024)) >= 0)
		    {
		    	progress += count;
		        out.write(data, 0, count);
		        percent = (progress) / filesize;
		        try {
					Thread.sleep(1L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    }
		    if(in != null)
		    	in.close();
		    if(in != null)
		    	out.close();
		    } catch (MalformedURLException e) {
				errorCode = "Could not connect to site: " + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorCode = "Could not connect to site: " + e.getMessage();
				e.printStackTrace();
			}
			((GuiButton)buttonList.get(1)).displayString = "Close";
			((GuiButton)buttonList.get(1)).enabled = true;
			hasUpdated = true;
			Sys.openURL(new File(mc.getMinecraftDir(), "bin").getAbsolutePath());
		}
	}
}
