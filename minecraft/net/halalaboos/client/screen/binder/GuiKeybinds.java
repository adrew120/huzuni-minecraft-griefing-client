package net.halalaboos.client.screen.binder;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import org.lwjgl.Sys;

import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.Account;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class GuiKeybinds extends GuiScreen {
	protected GuiScreen parentGui;
	private GuiSlotKeybinds modList;
	public GuiTextField field;

	public GuiKeybinds(GuiScreen par1GuiScreen) {
		parentGui = par1GuiScreen;
	}

	GuiButton clearKeybind;
	GuiButton toggle;

	public void initGui() {
		field = new GuiTextField(mc.fontRenderer, width / 2 + 2, height - 44, 75, 20);
		field.setFocused(true);
		StringTranslate stringtranslate = StringTranslate.getInstance();
		buttonList.add(new GuiSmallButton(6, width / 2 - 75, height - 22, "Done"));

		modList = new GuiSlotKeybinds(this);
		modList.registerScrollButtons(buttonList, 7, 8);

		clearKeybind = new GuiSmallButton(1, width / 2 - 75, height - 44, 75, 20, "Unbind");
		buttonList.add(clearKeybind);
		clearKeybind.enabled = modList.getCurrentSlot() >= 0 ? !((Mod)modList.getList().get(modList.getCurrentSlot())).getKeyName().equals("-1"):false;
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	protected void actionPerformed(GuiButton par1GuiButton) {
		if (!par1GuiButton.enabled) {
			return;
		}

		switch (par1GuiButton.id) {
		case 6:
			mc.displayGuiScreen(parentGui);
			break;

		case 0:
			if (modList.getCurrentSlot() >= 0 && modList.getList().size() > 0)
				modList.elementClicked(modList.getCurrentSlot(), true);
			break;
		case 1:
			if (modList.getCurrentSlot() >= 0 && modList.getList().size() > 0) {
				((Mod)modList.getList().get(modList.getCurrentSlot())).setKey(-1);
				modList.setCurrentSlot(-1);
			}
			break;

		default:
			modList.actionPerformed(par1GuiButton);
			break;
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	protected void keyTyped(char par1, int par2) {
		if (modList.getCurrentSlot() >= 0 && modList.getList().size() > 0 && !field.isFocused()) {
			((Mod)modList.getList().get(modList.getCurrentSlot())).setKey(par2);
			modList.setCurrentSlot(-1);
		} else 
			super.keyTyped(par1, par2);

		field.textboxKeyTyped(par1, par2);

		if (field.getText().length() > 0) {
			ArrayList l = new ArrayList();
			for (Mod mod : ModManager.getMods()) {
				if (mod.getName().toLowerCase()
						.startsWith(field.getText().toLowerCase().trim()))
					l.add(mod);
			}
			modList.setCurrentSlot(-1);
			modList.setList(l);
		} else
			modList.setList(ModManager.getMods());
	}

	/**
	 * Called when the mouse is clicked.
	 */
	protected void mouseClicked(int par1, int par2, int par3) {
		super.mouseClicked(par1, par2, par3);
		field.mouseClicked(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(int par1, int par2, float par3) {
		modList.drawScreen(par1, par2, par3);
		StringTranslate stringtranslate = StringTranslate.getInstance();
		drawCenteredString(fontRenderer, "Keybind Manager", width / 2, 16,
				0xffffff);
		drawCenteredString(fontRenderer,
				"Select the mod, then type the key specified.", width / 2,
				height - 56, 0x990000);
		super.drawScreen(par1, par2, par3);
		mc.fontRenderer.drawString(
				"Amount of mods: " + (ModManager.getMods().size()), 2,
				height - 12, 0xffffff);
		clearKeybind.enabled = modList.getCurrentSlot() >= 0 ? !((Mod)modList.getList().get(modList.getCurrentSlot())).getKeyName().equals("-1"):false;
		field.drawTextBox();
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen() {
		super.updateScreen();
		field.updateCursorCounter();
	}

	public void onGuiClosed() {

	}
}
