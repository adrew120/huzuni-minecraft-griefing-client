package net.halalaboos.client.utils;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.ssl.*;
import org.lwjgl.opengl.*;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.AccountFile;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class Account {

	private String username, password;
	private boolean isPrivate;

	public Account(String s, String s1, boolean b) {
		username = s;
		password = s1;
		isPrivate = b;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

}
