package net.halalaboos.client.utils;

public class Counter {

	private long countDelay, lastTick = -1;
	private int count;
	private boolean reverse = false;
	public Counter(long countDelay, int startCount) {
		this.countDelay = countDelay;
		this.count = startCount;
	}
	
	public void count() {
		if(ClientUtils.getSystemTime() - getLastTick() > getCountDelay()) {
			setCount(getCount() + (reverse ? -1:1));
			setLastTick(ClientUtils.getSystemTime());
		}
	}
	
	public void reverse() {
		reverse = !reverse;
	}
	
	public void reset() {
		reverse = false;
		this.setCount(0);
	}

	public long getCountDelay() {
		return countDelay;
	}

	public void setCountDelay(long countDelay) {
		this.countDelay = countDelay;
	}

	public long getLastTick() {
		return lastTick;
	}

	public void setLastTick(long lastTick) {
		this.lastTick = lastTick;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
}
