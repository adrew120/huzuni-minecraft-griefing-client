package net.halalaboos.client.utils;

public class Bounds {

	private double x, y, width, height;

	public Bounds(double x, double y, double w, double h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public boolean isPointInside(int pX, int pY) {
		return pX > getX() && pX < getX() + getWidth() && pY > getY()
				&& pY < getY() + getHeight();
	}

	public boolean compare(Bounds b) {
		if (b.isPointInside((int) getX(), (int) getY())
				|| // x, y
				b.isPointInside((int) getX() + (int) getWidth(), (int) getY())
				|| // x1, y
				b.isPointInside((int) getX() + (int) getWidth(), (int) getY()
						+ (int) getHeight())
				|| // x1, y1
				b.isPointInside((int) getX(), (int) getY() + (int) getHeight()) // x,
																				// y1

				||

				isPointInside((int) b.getX(), (int) b.getY())
				|| // x, y
				isPointInside((int) b.getX() + (int) b.getWidth(),
						(int) b.getY()) || // x1, y
				isPointInside((int) b.getX() + (int) b.getWidth(),
						(int) b.getY() + (int) b.getHeight()) || // x1, y1
				isPointInside((int) b.getX(),
						(int) b.getY() + (int) b.getHeight()) // x, y1
		)
			return true;
		/*
		 * if(b.getX() > getX() && (b.getX()) < (getX() + getWidth())) return
		 * true;
		 * 
		 * if(b.getY() > getY() && (b.getY()) < (getY() + getHeight())) return
		 * true;
		 */

		return false;
	}
}
