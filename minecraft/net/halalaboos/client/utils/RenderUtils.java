package net.halalaboos.client.utils;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Gui;
import net.minecraft.src.ScaledResolution;
import net.minecraft.src.Tessellator;

public class RenderUtils extends Gui {
	private Minecraft mc;

	public RenderUtils(Minecraft mc) {
		this.mc = mc;
	}


	public void drawHalRect(double x, double y, double x1, double y1,
			boolean b, boolean e) {
		if (e) {
			if (b)
				drawBorderedRect(x, y, x1, y1, 1f,  0.3f, 0.6f, 0.9f, 0.75f);
			else
				drawBorderedRect(x, y, x1, y1, 1f,  0.2f, 0.5f, 0.8f, 0.75f);
		} else {
			if (b)
				drawBorderedRect(x, y, x1, y1, 1f, 0.5f, 0.5f, 0.5f, 0.75f);
			else
				drawBorderedRect(x, y, x1, y1, 1f, 0.3f, 0.3f, 0.3f, 0.75f);
		}
	}

	public void drawBorderedRect(double x, double y, double x1, double y1,
			float width, float r, float g, float b, float a) {
		drawRect(x + 0.5, y + 0.5, x1 - 0.5, y1 - 0.5, r, g, b, a);
		// TOP, left to right
		drawRect(x, y, x1, y + width, r + 0.1f, g + 0.1f,
				b + 0.1f, 1);

		// LEFT, top to bottom
		drawRect(x, y, x + width, y1, r + 0.1f, g + 0.1f,
				b + 0.1f, 1);

		// RIGHT, top to bottom
		drawRect(x1 - width, y, x1, y1, r + 0.1f, g + 0.1f,
				b + 0.1f, 1);

		// BOTTOM, top to bottom
		drawRect(x, y1 - width, x1, y1, r + 0.1f, g + 0.1f,
				b + 0.1f, 1);

	}
	
    public void drawRect(double x, double y, double d, double e, int color) {
        float alpha = (float)(color >> 24 & 255) / 255.0F;
        float red = (float)(color >> 16 & 255) / 255.0F;
        float green = (float)(color >> 8 & 255) / 255.0F;
        float blue = (float)(color & 255) / 255.0F;
        drawRect((double) x, (double) y, (double) d, (double) e, red, green, blue, alpha);
    }
    
	public void drawBorderedRect(double x, double y, double x1, double y1,
			float width, int color) {
        float alpha = (float)(color >> 24 & 255) / 255.0F;
        float red = (float)(color >> 16 & 255) / 255.0F;
        float green = (float)(color >> 8 & 255) / 255.0F;
        float blue = (float)(color & 255) / 255.0F;
        drawBorderedRect((double) x, (double) y, (double) x1, (double) y1, width, red, green, blue, alpha);
	}

	public void drawRect(double x, double y, double x1, double y1, float r,
			float g, float b, float a) {
		start2dRendering();
		enableSmoothing();
		glColor4f(r, g, b, a);
		drawRect(x, y, x1, y1);
		end2dRendering();
		disableSmoothing();
	}

	public void drawRect(double x, double y, double x1, double y1) {
		glBegin(GL_QUADS);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glEnd();
	}

	public void drawLine(double x, double y, double x1, double y1, float width) {
		glLineWidth(width);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x1, y1);
		glEnd();
	}

	public void enableSmoothing() {
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	}

	public void disableSmoothing() {
		glDisable(GL_LINE_SMOOTH);
	}

	public void start2dRendering() {
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public void end2dRendering() {
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
	}

	public void drawOutlinedBox(AxisAlignedBB box) {
		glBegin(GL_LINE_STRIP);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.minY, box.minZ);
		glEnd();
		glBegin(GL_LINE_STRIP);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glEnd();
		glBegin(GL_LINES);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glEnd();
	}

	public void drawBox(AxisAlignedBB box) {
		// back
		glBegin(GL_QUADS);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glEnd();
		// left
		glBegin(GL_QUADS);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glEnd();
		// right
		glBegin(GL_QUADS);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glEnd();
		// front
		glBegin(GL_QUADS);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glEnd();
		// top
		glBegin(GL_QUADS);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3d(box.maxX, box.maxY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.minZ);
		glVertex3d(box.minX, box.maxY, box.maxZ);
		glVertex3d(box.maxX, box.maxY, box.maxZ);
		glEnd();

		// bottom
		glBegin(GL_QUADS);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3d(box.maxX, box.minY, box.minZ);
		glVertex3d(box.minX, box.minY, box.minZ);
		glVertex3d(box.minX, box.minY, box.maxZ);
		glVertex3d(box.maxX, box.minY, box.maxZ);
		glEnd();
	}

	public void drawCrossedBox(AxisAlignedBB ax, float r, float g, float b, boolean c) {
		  if(c){
		   glColor4f(r,g,b, 0.1f);
		   this.drawBox(ax);
		  }
		  glColor4f(r, g, b, 0.75f);
		  glLineWidth(1f);
		  this.drawOutlinedBox(ax);
		  glLineWidth(0.75f);
		  glBegin(GL_LINES);
		  glVertex3d(ax.maxX, ax.maxY, ax.maxZ);
		  glVertex3d(ax.maxX, ax.minY, ax.minZ);
		  glVertex3d(ax.maxX, ax.maxY, ax.minZ);

		  glVertex3d(ax.minX, ax.maxY, ax.maxZ);
		  glVertex3d(ax.minX, ax.maxY, ax.minZ);
		  glVertex3d(ax.maxX, ax.minY, ax.minZ);

		  glVertex3d(ax.minX, ax.minY, ax.maxZ);
		  glVertex3d(ax.maxX, ax.maxY, ax.maxZ);
		  glVertex3d(ax.minX, ax.minY, ax.maxZ);

		  glVertex3d(ax.minX, ax.maxY, ax.minZ);
		  glVertex3d(ax.minX, ax.minY, ax.minZ);
		  glVertex3d(ax.maxX, ax.minY, ax.maxZ);
		  glEnd();
		 }

	/**
	 * @author Jonalu
	 * @return Used for nifty things ;)
	 * */
	public void prepareScissorBox(float x, float y, float x2, float y2) {
		ScaledResolution sr = mc.theClient.getClientUtils().getScaledResolution();
		int factor = sr.getScaleFactor();
		glScissor((int) (x * factor),
				(int) ((sr.getScaledHeight() - y2) * factor),
				(int) ((x2 - x) * factor), (int) ((y2 - y) * factor));
	}
}
