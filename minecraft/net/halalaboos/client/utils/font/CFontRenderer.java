package net.halalaboos.client.utils.font;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.halalaboos.client.Client;
import net.halalaboos.client.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.src.ChatAllowedCharacters;

import org.lwjgl.opengl.GL11;

public class CFontRenderer {
	public CFont font, boldFont, italicFont, boldItalicFont;
	private Minecraft mc;
	private Random fontRandom = new Random();

	public CFontRenderer(Minecraft mc, String fontName, int fontSize) {
		this.mc = mc;
		font = new CFont(mc, new Font(fontName, Font.TRUETYPE_FONT, fontSize));
		boldFont = new CFont(mc, new Font(fontName, Font.BOLD, fontSize));
		italicFont = new CFont(mc, new Font(fontName, Font.ITALIC, fontSize));
		boldItalicFont = new CFont(mc, new Font(fontName, Font.BOLD | Font.ITALIC, fontSize));
	}

	public void drawString(String s, double x, double y, int color) {
		drawString(s, x, y, color, true);
	}

	public void drawStringWithShadow(String s, double x, double y, int color) {
		drawString(s, x + 1, y + 1, color, false);
		drawString(s, x, y, color, true);
	}
	
	private void drawString(String text, double x, double y, int color, boolean renderColorCode) {
		if(text == null)
			return;
		GL11.glPushMatrix();
		GL11.glTranslated(x, y, 0);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		if ((color & -67108864) == 0)
        {
			color |= -16777216;
        }

        if (!renderColorCode)
        {
        	color = (color & 16579836) >> 2 | color & -16777216;
        }
		final float red = (color >> 16 & 0xff) / 255F;
		final float green = (color >> 8 & 0xff) / 255F;
		final float blue = (color & 0xff) / 255F;
		final float alpha = (color >> 24 & 0xff) / 255F;
		Color c = new Color(red, green, blue, alpha);
		if(text.contains("\247")) {
			String[] parts = text.split("\247");
			
			Color currentColor = c;
			CFont currentFont = font;
			int width = 0;
			boolean randomCase = false, bold = false, 
			italic = false, strikethrough = false, 
			underline = false;
			
			for(int index = 0; index < parts.length; index++) {
				if(index == 0) {
					
					font.drawString(mc.ingameGUI, parts[index], width, 0, currentColor, renderColorCode);
					width += getStringWidth(parts[index]);
					
				} else {
					
					String words = parts[index].substring(1);
					char type = parts[index].charAt(0);
					final int colorIndex = "0123456789abcdefklmnorq".indexOf(type);
					if(colorIndex != -1) {
						
						if (colorIndex < 16) { // coloring
							int colorcode = mc.fontRenderer.getColorCode()[colorIndex];
							currentColor = font.getColor(colorcode, alpha);
							bold = false;
							italic = false;
							randomCase = false;
							underline = false;
							strikethrough = false;
						} else if(colorIndex == 16) { // random case
							randomCase = true;
						} else if(colorIndex == 17) { // bold
							bold = true;
						} else if(colorIndex == 18) { // strikethrough
							strikethrough = true;
						} else if(colorIndex == 19) { // underline
							underline = true;
						} else if(colorIndex == 20) { // italic
							italic = true;
						} else if(colorIndex == 21) { // reset
							bold = false;
							italic = false;
							randomCase = false;
							underline = false;
							strikethrough = false;
							currentColor = c;
						} else if(colorIndex == 22) { // custom mang
							currentColor = new Color(0, 90, 163);
						}
						
					}
					if(bold && italic) {
						boldItalicFont.drawString(mc.ingameGUI, randomCase ? toRandom(currentFont, words):words, width, 0, currentColor, renderColorCode);
						currentFont = boldItalicFont;
					} else if(bold) {
						boldFont.drawString(mc.ingameGUI, randomCase ? toRandom(currentFont, words):words, width, 0, currentColor, renderColorCode);
						currentFont = boldFont;
					} else if(italic) {
						italicFont.drawString(mc.ingameGUI, randomCase ? toRandom(currentFont, words):words, width, 0, currentColor, renderColorCode);
						currentFont = italicFont;
					} else {
						 font.drawString(mc.ingameGUI, randomCase ? toRandom(currentFont, words):words, width, 0, currentColor, renderColorCode);
						 currentFont = font;
					}
					if(strikethrough) {
						GL11.glDisable(GL11.GL_TEXTURE_2D);
						Client.getRenderUtils().drawLine(width, currentFont.getStringHeight(words) / 2 + 1, width + currentFont.getStringWidth(words), currentFont.getStringHeight(words) / 2 + 1, 1.5f);
						GL11.glEnable(GL11.GL_TEXTURE_2D);
					}
					
					if(underline) {
						GL11.glDisable(GL11.GL_TEXTURE_2D);
						Client.getRenderUtils().drawLine(width, currentFont.getStringHeight(words), width + currentFont.getStringWidth(words), currentFont.getStringHeight(words), 1.5f);
						GL11.glEnable(GL11.GL_TEXTURE_2D);
					}
					width += currentFont.getStringWidth(words);
				}
			}
		} else
			font.drawString(mc.ingameGUI, text, 0, 0, c, renderColorCode);
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glPopMatrix();
	}
	
	/**
	 * Make dis
	 * */
	public String toRandom(CFont font, String text) {
		String newText = "";
		for(char c : text.toCharArray()) {
			if(ChatAllowedCharacters.allowedCharacters.indexOf(c) > 0) {
				int index = mc.fontRenderer.fontRandom.nextInt(ChatAllowedCharacters.allowedCharacters.length());
				newText += ChatAllowedCharacters.allowedCharacters.toCharArray()[index];
			}
		}
		return newText;
	}
	
	public int getStringWidth(String text) {
		if(text == null)
			return 0;
		if(text.contains("\247")) {
			String[] parts = text.split("\247");
			CFont currentFont = font;
			int width = 0;
			boolean randomCase = false, bold = false, 
			italic = false, strikethrough = false, 
			underline = false;
			
			for(int index = 0; index < parts.length; index++) {
				if(index == 0) {
					width += getStringWidth(parts[index]);
				} else {
					String words = parts[index].substring(1);
					char type = parts[index].charAt(0);
					final int colorIndex = "0123456789abcdefklmnor".indexOf(type);
					if(colorIndex != -1) {
						
						if (colorIndex < 16) { // coloring
							bold = false;
							italic = false;
							randomCase = false;
							underline = false;
							strikethrough = false;
						} else if(colorIndex == 16) { // random case
							randomCase = true;
						} else if(colorIndex == 17) { // bold
							bold = true;
						} else if(colorIndex == 18) { // strikethrough
							strikethrough = true;
						} else if(colorIndex == 19) { // underline
							underline = true;
						} else if(colorIndex == 20) { // italic
							italic = true;
						} else if(colorIndex == 21) { // reset
							bold = false;
							italic = false;
							randomCase = false;
							underline = false;
							strikethrough = false;
						}
						
					}
					if(bold && italic)
						currentFont = boldItalicFont;
					else if(bold)
						currentFont = boldFont;
					else if(italic)
						currentFont = italicFont;
					else
						currentFont = font;

					width += currentFont.getStringWidth(words);
				}
			}
			return width;
		} else
			return font.getStringWidth(text);
	}

	public CFont getFont() {
		return font;
	}
	

	public List<String> wrapWords(String text, double width) {
		List<String> finalWords = new ArrayList<String>();
		if (getStringWidth(text) > width) {
			String[] words = text.split(" ");
			String currentWord = "";
			int stringCount = 0;
			for (String word : words) {
				if (getStringWidth(currentWord + word + " ") < width) {
					currentWord += word + " ";
				} else {
					finalWords.add(currentWord);
					currentWord = word + " ";
					stringCount++;
				}
			}
			if (!currentWord.equals("")) {
				if (getStringWidth(currentWord) < width) {
					finalWords.add(currentWord);
					currentWord = "";
					stringCount++;
				} else {
					List<String> formattedWord = formatString(currentWord,
							width);
					for (String s : formattedWord)
						finalWords.add(s);
				}
			}
		} else
			finalWords.add(text);
		return finalWords;
	}

	public List<String> formatString(String s, double width) {
		List<String> finalWords = new ArrayList<String>();
		String currentWord = "";
		int charCount = 0;
		for (char c : s.toCharArray()) {
			String character = String.valueOf(c);
			if (getStringWidth(currentWord + character) < width)
				currentWord += character;
			else {
				finalWords.add(currentWord);
				currentWord = character;
				charCount++;
			}
		}

		if (charCount == 0) {
			if (currentWord.length() > 0)
				finalWords.add(currentWord);
		}

		return finalWords;
	}
	
    public void drawCenteredString(String par2Str, int x, int y, int co)
    {
    	mc.theClient.getFR().drawStringWithShadow(par2Str, x - mc.theClient.getFR().getStringWidth(par2Str) / 2, y, co);
    }
}
