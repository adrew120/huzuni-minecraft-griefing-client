package net.halalaboos.client.utils.font;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.Gui;

import org.lwjgl.opengl.GL11;

/**
 * 
 * @author TheObliterator
 * 
 *         A class to create and draw true type fonts onto the Minecraft game
 *         engine.
 */

public class CFont {
	public void getFont() {
		final GraphicsEnvironment enviro = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		final String[] fontTypes = enviro.getAvailableFontFamilyNames();

		for (final String fontType : fontTypes) {
			fontList.add(fontType);
		}
		System.out.println("System Fonts Loaded");
	}

	private int texID;
	private final int[] xPos;
	private final int[] yPos;
	private final int startChar;
	private final FontMetrics metrics;
	public static ArrayList fontList = new ArrayList();
	public static boolean flag = true;
	private final Minecraft mc = Minecraft.getMinecraft();

	/**
	 * Instantiates the font, filling in default start and end character
	 * parameters.
	 * 
	 * 'new CustomFont(ModLoader.getMinecraftInstance(), "Arial", 12);
	 * 
	 * @param mc
	 *            The Minecraft instance for the font to be bound to.
	 * @param fontName
	 *            The name of the font to be drawn.
	 * @param size
	 *            The size of the font to be drawn.
	 */
	public CFont(Minecraft mc, Font font) {
		this(mc, font, 32, 126);
	}

	/**
	 * Instantiates the font, pre-rendering a sprite font image by using a true
	 * type font on a bitmap. Then allocating that bitmap to the Minecraft
	 * rendering engine for later use.
	 * 
	 * 'new CustomFont(ModLoader.getMinecraftInstance(), "Arial", 12, 32, 126);'
	 * 
	 * @param mc
	 *            The Minecraft instance for the font to be bound to.
	 * @param fontName
	 *            The name of the font to be drawn.
	 * @param size
	 *            The size of the font to be drawn.
	 * @param startChar
	 *            The starting ASCII character id to be drawable. (Default 32)
	 * @param endChar
	 *            The ending ASCII character id to be drawable. (Default 126)
	 */
	public CFont(Minecraft mc, Font font, int startChar,
			int endChar) {
		this.startChar = startChar;
		xPos = new int[endChar - startChar];
		yPos = new int[endChar - startChar];
		final BufferedImage img = new BufferedImage(256, 256,
				BufferedImage.TYPE_INT_ARGB);
		final Graphics g = img.getGraphics();
		final Graphics2D g1 = (Graphics2D) g;
		g1.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		g.setFont(font);

		g.setColor(new Color(255, 255, 255, 0));
		g.fillRect(0, 0, 256, 256);
		g.setColor(Color.white);
		metrics = g.getFontMetrics();
		// Draw the specified range of characters onto
		// the new bitmap, spacing according to the font
		// widths. Also allocating positions of characters
		// on the bitmap to two arrays which will be used
		// later when drawing.
		int x = 2;
		int y = 2;

		for (int i = startChar; i < endChar; i++) {
			g.drawString("" + ((char) i), x, y + g.getFontMetrics().getAscent());
			xPos[i - startChar] = x;
			yPos[i - startChar] = y - metrics.getMaxDescent() + 2;
			x += metrics.stringWidth("" + (char) i) + 2;

			if (x >= 250 - metrics.getMaxAdvance()) {
				x = 2;
				y += metrics.getMaxAscent() + metrics.getMaxDescent()
						+ (g.getFont().getSize() / 2);
			}
		}

		// Render the finished bitmap into the Minecraft
		// graphics engine.
		try {
			texID = mc.renderEngine.allocateAndSetupTexture(img);
		} catch (final NullPointerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Private drawing method used within other drawing methods.
	 */
	public final void drawChar(Gui gui, char c, double x, double y)
			throws ArrayIndexOutOfBoundsException {
		final Rectangle2D bounds = metrics.getStringBounds(
				Character.toString(c), null);
		try {
			if((byte) c - startChar >= 0 && (xPos.length > (byte) c - startChar && yPos.length > (byte) c - startChar))
				gui.drawTexturedModalRect((int) x, (int) y, xPos[(byte) c - startChar], yPos[(byte) c - startChar], (int) bounds.getWidth(), (int) bounds.getHeight() + metrics.getMaxDescent());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Draws a given string onto a gui/subclass.
	 * 
	 * @param gui
	 *            The gui/subclass to be drawn on
	 * @param text
	 *            The string to be drawn
	 * @param x
	 *            The x position to start drawing
	 * @param y
	 *            The y position to start drawing
	 * @param color
	 *            The color of the non-shadowed text (Hex)
	 */
	public final void drawString(Gui gui, String text, double x, double y,
			Color color, boolean renderColorCode) {
		final FontRenderer fr = mc.fontRenderer;
		x *= 2;
		y = (y * 2) - 6;
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glScaled(0.5D, 0.5D, 0.5D);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texID);
		glColor(renderColorCode ? color:color.darker().darker().darker().darker().darker());
		final double startX = x;
		final int size = text.length();
		for (int indexInString = 0; indexInString < size; indexInString++) {
			final char character = filterChar(text.charAt(indexInString));
			drawChar(gui, character, x, y);
			x += metrics.getStringBounds(Character.toString(character), null).getWidth();
		}
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glPopMatrix();
	}
	
	private final char filterChar(char c) {
		if (c == '�') {
			c = 'a';
		}

		if (c == '�') {
			c = 'a';
		}

		if (c == '�') {
			c = 'o';
		}

		if (c == '�') {
			c = 'o';
		}

		if (c == '�') {
			c = 'O';
		}

		if (c == '�') {
			c = 'O';
		}

		if (c == '�') {
			c = 'A';
		}

		if (c == '�') {
			c = 'A';
		}

		if (c == '~') {
			c = '-';
		}

		return c;
	}
	
	public void glColor(Color color) {
		float red = (float) color.getRed() / 255F, green = (float) color.getGreen() / 255F, blue = (float) color.getBlue() / 255F, alpha = (float) color.getAlpha() / 255F;
		GL11.glColor4f(red, green, blue, alpha);
	}
	
	public Color getColor(int colorCode, float alpha) {
		return new Color((colorCode >> 16) / 255F, (colorCode >> 8 & 0xff) / 255F, (colorCode & 0xff) / 255F, alpha);
	}

	public Color getColor(int colorCode) {
		return new Color((colorCode >> 16) / 255F, (colorCode >> 8 & 0xff) / 255F, (colorCode & 0xff) / 255F);
	}

	/**
	 * A method that returns a Rectangle that contains the width and height
	 * demensions of the given string.
	 * 
	 * @param text
	 *            The string to be measured
	 * @return Rectangle containing width and height that the text will consume
	 *         when drawn.
	 */
	private final Rectangle getBounds(String text) {
		int w = 0;
		int h = 0;
		int tw = 0;
		final int size = text.length();

		for (int i = 0; i < size; i++) {
			final char c = text.charAt(i);

			if (c == '\\') {
				char type;

				try {
					type = text.charAt(i + 1);
				} catch (final StringIndexOutOfBoundsException e) {
					break;
				}

				if (type == 'n') {
					h += metrics.getAscent() + 2;

					if (tw > w) {
						w = tw;
					}

					tw = 0;
				}

				i++;
				continue;
			}

			tw += metrics.stringWidth(Character.toString(c));
		}

		if (tw > w) {
			w = tw;
		}

		h += metrics.getAscent();
		return new Rectangle(0, 0, w, h);
	}

	/**
	 * Returns the created FontMetrics which is used to retrive various
	 * information about the True Type Font
	 * 
	 * @return FontMetrics of the created font.
	 */
	public FontMetrics getMetrics() {
		return metrics;
	}

	/**
	 * Gets the drawing height of a given string of string.
	 * 
	 * @param text
	 *            The string to be measured
	 * @return The height of the given string.
	 */
	public int getStringHeight(String text) {
		return (int) getBounds(text).getHeight() / 2;
	}

	/**
	 * Gets the drawing width of a given string of string.
	 * 
	 * @param text
	 *            The string to be measured
	 * @return The width of the given string.
	 */
	public int getStringWidth(String text) {
		if(text == null)
			return 0;
		return (int) getBounds(text).getWidth() / 2;
	}
}
