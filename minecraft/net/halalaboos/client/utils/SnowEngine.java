package net.halalaboos.client.utils;

import java.awt.Color;
import java.util.Random;
import static org.lwjgl.opengl.GL11.*;

public class SnowEngine extends SimpleParticleEngine {
	private Counter particleCounter;
	private static Texture snowParticle;
	
	/**
	 * A simple snow particle engine, made by Halalalalalalbooobies.
	 * @param expirationTime - Time (in ticks) it takes for the particle to de-spawn.
	 * @param particleSpawnRate - How long it takes for a new pair of particles to spawn. (in MS)
	 * */
	public SnowEngine(int expirationTime, long particleSpawnRate) {
		super(expirationTime);
		particleCounter = new Counter(particleSpawnRate, 0);
		if(snowParticle == null)
			snowParticle = new Texture("snow_particle.png");
	}

	@Override
	public void renderParticle(Particle particle) {
		glColor4f(1, 1, 1, 0.7F);
		glRotatef((float) particle.getRandomValue(), 0, 0, 1);

		if(particle.getRandomValue() % 20 == 0) {
			snowParticle.renderTexture(0, 0, 10, 10, Color.WHITE);
		} else {
			glPointSize(1 + (float) particle.getRandomValue() / 20F);
			glBegin(GL_POINTS);
				glVertex2d(0, 0);
			glEnd();
		}
	}
	/***
	 * Spawns particles once the specified time has passed.
	 * @param screenWidth - Width of the entire screen.
	 * @param spawnRate - How many particles spawned when called.
	 * @param disperseRate - How much the particles will disperse.
	 * */
	public void spawnParticles(int screenWidth, float spawnRate, float disperseRate) {
		particleCounter.count();
		if(particleCounter.getCount() > 0) {
			particleCounter.reset();
			float randomXPosition = (float) (new Random().nextDouble() * (screenWidth - 2));
			spawnParticles(randomXPosition, 10, spawnRate, disperseRate);
		}
	}

}
