package net.halalaboos.client.utils;

import net.halalaboos.client.Client;
import net.minecraft.src.MathHelper;

public class Coordinates {
	
	private double x, y, z;

	public Coordinates(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public boolean compare(Coordinates c) {
		if(c == null)
			return false;
		return (c.getX() == getX()) && (c.getY() == getY())
				&& (c.getZ() == getZ());
	}
	
	public double getDifference(Coordinates coords) {
		if(coords == null)
			return 0;
        float xDiff = (float)(x - coords.getX());
        float yDiff = (float)(y - coords.getY());
        float zDiff = (float)(z - coords.getZ());
        return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}
	
	public String toString() {
		return "XYZ:" + getX() + ":" + getY() + ":" + getZ();
	}
}
