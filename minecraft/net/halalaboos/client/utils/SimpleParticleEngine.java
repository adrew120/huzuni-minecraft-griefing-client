package net.halalaboos.client.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.halalaboos.client.Client;
import static org.lwjgl.opengl.GL11.*;

public abstract class SimpleParticleEngine {

	private Random random;
	private List<Particle> particles;
	private int particleExpirationTime;
	
	/**
	 * @param expirationTime - Time it takes for the particle to expire.
	 * */
	public SimpleParticleEngine(int expirationTime) {
		random = new Random();
		particles = new ArrayList<Particle>();
		this.particleExpirationTime = expirationTime;
	}
	/**
	 * Updates all particles, applying gravity to them. <br>
	 * Called off inside of a tick method.
	 * @param gravity - Rate that the particles will fall.
	 * */
	public void updateParticles(float gravity) {
		for(int index = 0; index < particles.size(); index++) {
			Particle particle = particles.get(index);
			
			particle.update(gravity);
			
			if(particle.hasExpired()) {
				particles.remove(particle);
				index--;
			}
		}
	}
	/**
	 * Renders all particles onto the screen.
	 * */
	public void renderParticles() {
		Client.getRenderUtils().start2dRendering();
		for (Particle particle : particles) {
			glPushMatrix();
			glTranslated(particle.position.getX(), particle.position.getY(), 0);
			renderParticle(particle);
			glPopMatrix();
		}
		Client.getRenderUtils().end2dRendering();
	}
	
	public abstract void renderParticle(Particle particle);
	
	/**
	 * Spawns a set of particles at the position given.
	 * @param xPosition - X position the particles will begin their rain at.
	 * @param yPosition - Y position the particles will begin their reign at.
	 * @param spawnRate - How many particles spawned when called.
	 * @param disperseRate - How much the particles will disperse.
	 * */
	public void spawnParticles(float xPosition, float yPosition, float spawnRate, float disperseRate) {
		for(int index = 0; index < spawnRate; index++) {
			double x = (xPosition - disperseRate / 2) + (random.nextDouble() * disperseRate);
			double y = (yPosition - disperseRate / 2) + (random.nextDouble() * disperseRate);
			Particle particle = new Particle(x, y);
			particles.add(particle);
		}
	}
	
	public List<Particle> getParticles() {
		return particles;
	}

	public class Particle {
		public Coordinates position;
		private double maxTick, currentTick, randomValue;
		
		public Particle(double posX, double posY) {
			randomValue = random.nextInt(100) + 1;
			position = new Coordinates(posX, posY, 0);
			maxTick = particleExpirationTime + random.nextInt(10);
		}
		
		private void update(float gravity) {
            position.setY(position.getY() + gravity + random.nextInt((int) (gravity / 2)));
            currentTick++;
        }
        
		private boolean hasExpired() {
			return maxTick - currentTick <= 0;
		}
		
		public Coordinates getPosition() {
			return position;
		}
		
		public double getRandomValue() {
			return randomValue;
		}
	}
	
}
