package net.halalaboos.client.networking;

import java.util.List;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.manager.WindowManager;
import net.halalaboos.client.ui.window.custom.WChat;
import net.halalaboos.client.utils.Coordinates;
import net.minecraft.src.GuiScreen;

public class HuzuniClientImpl extends HuzuniClient {

	private WChat chatWindow;
	
	public HuzuniClientImpl(String ip, int port) {
		super(SettingsFile.getUsername(), ip, port);
	}

	@Override
	public void write(String text) {
		if(chatWindow != null)
			chatWindow.getTextArea().append(text);
	}

	@Override
	public void onClose() {
		if(chatWindow != null) {
			chatWindow.getUserList().clearLines();
			WindowManager.getModWindows().remove(chatWindow);
			chatWindow = null;
		}
	}

	@Override
	public void onStart() {
		chatWindow = new WChat(WindowManager.getModWindows());
		WindowManager.getModWindows().add(chatWindow);
	}

	@Override
	public void updateUsers(List<String> users) {
		if(chatWindow != null) {
			chatWindow.getUserList().clearLines();
			for(String s : users)
				chatWindow.getUserList().append("*" + s);
		}
	}

	@Override
	public void recieveCoordinates(String sender, Coordinates coords) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recieveServer(String sender, String ip, int port) {
		if(chatWindow != null) {
			chatWindow.getTextArea().append(sender + " has sent you his server. Copied to your clipboard.");
			GuiScreen.setClipboardString(ip + ":" + port);
		}
	}

	public WChat getChatWindow() {
		return chatWindow;
	}

	public void setChatWindow(WChat chatWindow) {
		this.chatWindow = chatWindow;
	}

}
