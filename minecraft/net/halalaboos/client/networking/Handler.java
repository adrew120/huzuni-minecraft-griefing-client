package net.halalaboos.client.networking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.utils.Coordinates;

public class Handler {

	private HuzuniClient client;
	
	public Handler(HuzuniClient client) {
		this.client = client;
	}
	
	public boolean isNorm(String text) throws IOException {
		if(text.startsWith(HuzuniClient.COMMAND_LINE)) {
			String[] args = text.split(":");
			String message = text.substring(1);
			if(message.toUpperCase().startsWith("USERS")) {
				String[] players = text.substring(7).split(", ");
				List<String> users = new ArrayList<String>();
				users.clear();
				for(String s : players) {
					users.add(s);
				}
				client.setUsers(users);
				return false;

			} else if(message.toUpperCase().startsWith("SERVER")) {
				String sender = args[1];
				String ip = args[2];
				int port = Integer.parseInt(args[3]);
				client.recieveServer(sender, ip, port);
				return false;

			} else if(message.toUpperCase().startsWith("COORDINATES")) {
				if(args.length >= 3) {
					String sender = args[1];
					double x = Double.parseDouble(args[2]);
					double y = Double.parseDouble(args[3]);
					double z = Double.parseDouble(args[4]);
					client.recieveCoordinates(sender, new Coordinates(x, y, z));
				}
				return false;
				
			} else 
				return true;
		} else
			return true;
	}
	
}
