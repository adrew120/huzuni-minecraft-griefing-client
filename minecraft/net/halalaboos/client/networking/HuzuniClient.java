package net.halalaboos.client.networking;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.manager.WindowManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.custom.WChat;
import net.halalaboos.client.utils.Coordinates;

public abstract class HuzuniClient implements Runnable {
	public static final String COMMAND_LINE = "\246";
	
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	private Socket serverConnection;
	private boolean running;
	private String ip, username;
	private int port;
	private List<String> messageLines, users;
	private Handler inputHandler;
	public HuzuniClient(String username, String ip, int port) {
		this.ip = ip;
		this.port = port;
		this.username = username;
		messageLines = new CopyOnWriteArrayList<String>();
		inputHandler = new Handler(this);
	}

	@Override
	public void run() {
		try {
			running = true;
			write("Connecting to " + ip + "... ");
			serverConnection = new Socket(ip, port);
			write("Connected!");
			onStart();
			outputStream = new ObjectOutputStream(serverConnection.getOutputStream());
			outputStream.flush();
			inputStream = new ObjectInputStream(serverConnection.getInputStream());
			sendLine(COMMAND_LINE + "USERNAME:" + username);
			sendIP(SettingsFile.getServerIP(), SettingsFile.getServerPort());
			while(isConnected() && running) {
				try {
					Object object = inputStream.readObject();
					if(object instanceof String) {
						String message = (String) object;
						if (inputHandler.isNorm(message))
							write(message);
						
					}
				} catch (ClassNotFoundException e) {
					write("You fucked up!");
				}	
			}
		} catch (Exception e) {
			if(Client.getMC().thePlayer != null)
				Client.getInstance().addChatMessage("\247rCould not connect to Huzuni server: \247a\247l" + e.getMessage());
		} finally {
			if(isConnected()) {
				onClose();
				closeConnection();
			}
		}
	}

	public void sendMessage(String message) {
		if (message.length() > 0) {
			try {
				sendLine(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendCoordinates(String recipient, Coordinates coords) {
		if (isConnected() && running) {
			try {
				sendLine(COMMAND_LINE + "COORDINATES:" + recipient + ":" + coords.getX() + ":" + coords.getY() + ":" + coords.getZ());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void sendIP(String ip, int port) {
		if (isConnected()) {
			try {
				sendLine(COMMAND_LINE + "SERVER:" + ip + ":" + port);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void getIP(String user) {
		if (isConnected() && running) {
			try {
				sendLine(COMMAND_LINE + "GETSERVER:" + user);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void sendLine(String message) throws IOException {
		if(isConnected() && running) {
			outputStream.writeObject(message);
			outputStream.flush();
		}
	}

	public abstract void write(String text);
	public abstract void onClose();
	public abstract void onStart();
	public abstract void updateUsers(List<String> users);
	public abstract void recieveCoordinates(String sender, Coordinates coords);
	public abstract void recieveServer(String sender, String ip, int port);

	public void stop() {
		if(isConnected() && running) {
			running = false;
			onClose();
			closeConnection();	
		}
	}

	public void closeConnection() {
		if(isConnected()) {
			try {
				if(outputStream != null)
					outputStream.close();
				if(inputStream != null)
					inputStream.close();
				serverConnection.close();
				write("Connection closing...");
				serverConnection = null;
				outputStream = null;
				inputStream = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isConnected() {
		return serverConnection == null ? false:serverConnection.isConnected() & inputStream != null && outputStream != null;
	}
	
	public void setIP(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}
	
	public void connect(String ip, int port) throws IOException {
		username = SettingsFile.getUsername();
		stop();
		
		setIP(ip, port);
		SettingsFile.setHuzuniServer(ip + (port == 6969 ? "":":" + port));
		Thread t = new Thread(this);
		t.start();
	}
	
	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
		updateUsers(users);
	}
	
	public void updateConnectionStatus(String ip, int port) {
		sendIP(SettingsFile.getServerIP(), SettingsFile.getServerPort());
	}
	
}