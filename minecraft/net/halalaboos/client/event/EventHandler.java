package net.halalaboos.client.event;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;

import net.halalaboos.client.event.events.*;
import net.halalaboos.client.mods.*;
public class EventHandler {

	private HashMap<Class<? extends Event>, CopyOnWriteArrayList<Listener>> eventMap;
	
	public EventHandler() {
		eventMap = new HashMap<Class<? extends Event>, CopyOnWriteArrayList<Listener>>();
		eventMap.put(EventTick.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventRecieveMessage.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventBlockInteract.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventDeath.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventEntityInteract.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventRecievePacket.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventMovement.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventRender3D.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventRenderBlock.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventRender.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventSendMessage.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventServerUpdate.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventKeyPressed.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventRenderNamePlate.class, new CopyOnWriteArrayList<Listener>());
		eventMap.put(EventDisplayScreen.class, new CopyOnWriteArrayList<Listener>());

	}
	
	public void registerListener(Class<? extends Event> c, Listener m) {
		if(eventMap.containsKey(c)) {
			List l = eventMap.get(c);
			if(!l.contains(m)) {
				l.add(m);
			}
		}
	}
	
	public void unRegisterListener(Class<? extends Event> c, Listener m) {
		if(eventMap.containsKey(c)) {
			List l = eventMap.get(c);
			if(l.contains(m)) {
				l.remove(m);
			}
		}
	}
	
	public Event call(Event e) {
		if(eventMap.containsKey(e.getClass())) {
			List<Listener> l = eventMap.get(e.getClass());
			for(Listener m : l) {
				try{
				m.onEvent(e);
				}catch(Exception e1) {
					e1.printStackTrace();
				}
			}
		} else 
			System.out.println("Event not supported. Add event into eventMap.");
		return e;
	}
}
