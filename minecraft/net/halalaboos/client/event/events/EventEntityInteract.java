package net.halalaboos.client.event.events;

import net.halalaboos.client.event.Event;
import net.minecraft.src.*;

public class EventEntityInteract extends Event{

	private EventType type;
	private Entity entity;
	
	public enum EventType {
		HIT,
		RIGHT_CLICK;
	}
	
	public EventEntityInteract(Object source, EventType type, Entity entity) {
		super(source);
		this.type = type;
		this.entity = entity;
	}

	public EventType getType() {
		return type;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
}
