package net.halalaboos.client.event.events;

import net.halalaboos.client.event.Event;

public class EventKeyPressed extends Event {

	private int key;
	
	public EventKeyPressed(Object source, int key) {
		super(source);
		this.key = key;
	}

	public int getKey() {
		return key;
	}

}
