package net.halalaboos.client.event.events;

import net.halalaboos.client.event.Event;


public class EventRecieveMessage extends Event {

	private String message;
	
	public EventRecieveMessage(Object source, String message) {
		super(source);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
