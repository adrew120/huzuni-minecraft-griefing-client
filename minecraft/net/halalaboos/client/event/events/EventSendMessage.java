package net.halalaboos.client.event.events;

import net.halalaboos.client.event.EventCancellable;

public class EventSendMessage extends EventCancellable {

	private String message;
	
	public EventSendMessage(Object source, String message) {
		super(source);
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
