package net.halalaboos.client.event.events;

import net.halalaboos.client.event.EventCancellable;
import net.minecraft.src.*;

public class EventRecievePacket extends EventCancellable {
	private Packet packet;
	
	public EventRecievePacket(Object source, Packet packet) {
		super(source);
		this.packet = packet;
	}

	public Packet getPacket() {
		return packet;
	}
	
}
