package net.halalaboos.client.event.events;

import net.halalaboos.client.Client;
import net.halalaboos.client.event.*;
import net.halalaboos.client.utils.*;
import net.minecraft.src.*;

public class EventRenderBlock extends EventCancellable {
	private EventType type;
	private Block block;
	private Coordinates blockCoordinates;
	public enum EventType {
		RENDER_TYPE,
		RENDER_ALL_FACES,
		RENDER_AS_NORMAL
		;
	}
	
	public EventRenderBlock(Object source, EventType type, int x, int y, int z) {
		super(source);
		this.type = type;
		blockCoordinates = new Coordinates(x, y, z);;
		this.block = Block.blocksList[Client.getMC().theWorld.getBlockId(x, y, z)];
	}

	public EventRenderBlock(Object source, EventType type, int id) {
		super(source);
		this.type = type;
		blockCoordinates = null;
		this.block = Block.blocksList[id];
	}
	public EventType getType() {
		return type;
	}

	public Block getBlock() {
		return block;
	}

	public Coordinates getBlockCoordinates() {
		return blockCoordinates;
	}
	
}
