package net.halalaboos.client.event.events;

import net.halalaboos.client.event.Event;

public class EventServerUpdate extends Event {

	private float rotationYaw, rotationPitch;
	
	public EventServerUpdate(Object source, float rotationYaw, float rotationPitch) {
		super(source);
		this.rotationYaw = rotationYaw;
		this.rotationPitch = rotationPitch;
	}

	public float getRotationYaw() {
		return rotationYaw;
	}

	public void setRotationYaw(float rotationYaw) {
		this.rotationYaw = rotationYaw;
	}

	public float getRotationPitch() {
		return rotationPitch;
	}

	public void setRotationPitch(float rotationPitch) {
		this.rotationPitch = rotationPitch;
	}
	
	

}
