package net.halalaboos.client.event.events;

import net.halalaboos.client.Client;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.utils.Coordinates;
import net.minecraft.src.*;

public class EventBlockInteract extends Event {
	private EventType type;
	private Coordinates blockCoords;
	private Block block;
	private int blockHitDelay = -1;
	
	public enum EventType {
		CLICK,
		DESTROY,
		HURT,
		BLOCK_HIT_DELAY,
		RIGHT_CLICK;
	}
	
	public EventBlockInteract(Object source, EventType type, int x, int y, int z) {
		this(source, type, new Coordinates(x, y, z));
	}
	
	public EventBlockInteract(Object source, EventType type, Coordinates blockCoords) {
		super(source);
		this.type = type;
		this.blockCoords = blockCoords;
		block = Block.blocksList[Client.getMC().theWorld.getBlockId((int) blockCoords.getX(), (int) blockCoords.getY(), (int) blockCoords.getZ())];
	}

	public int getBlockHitDelay() {
		return blockHitDelay;
	}

	public void setBlockHitDelay(int blockHitDelay) {
		this.blockHitDelay = blockHitDelay;
	}

	public EventType getType() {
		return type;
	}

	public Coordinates getBlockCoords() {
		return blockCoords;
	}

	public Block getBlock() {
		return block;
	}
	
	
}
