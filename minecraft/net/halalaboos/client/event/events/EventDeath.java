package net.halalaboos.client.event.events;

import net.halalaboos.client.event.Event;
import net.minecraft.src.*;

public class EventDeath extends Event {

	private Entity entity;
	
	public EventDeath(Object source, Entity entity) {
		super(source);
		this.entity = entity;
	}

	public Entity getEntity() {
		return entity;
	}
	
}
