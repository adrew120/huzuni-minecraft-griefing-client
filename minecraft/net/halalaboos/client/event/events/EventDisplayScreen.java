package net.halalaboos.client.event.events;

import net.halalaboos.client.event.EventCancellable;
import net.minecraft.src.GuiScreen;

public class EventDisplayScreen extends EventCancellable {

	private GuiScreen screen;
	
	public EventDisplayScreen(Object source, GuiScreen screen) {
		super(source);
		this.screen = screen;
	}

	public GuiScreen getScreen() {
		return screen;
	}

	public void setScreen(GuiScreen screen) {
		this.screen = screen;
	}

	
}
