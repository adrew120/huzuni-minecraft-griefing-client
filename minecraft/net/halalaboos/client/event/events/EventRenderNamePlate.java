package net.halalaboos.client.event.events;

import net.halalaboos.client.event.Event;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;

public class EventRenderNamePlate extends Event {
	private String text;
	private double x, y, z;
	private EntityLiving entityLiving;
	
	public EventRenderNamePlate(Object source, EntityLiving entityLiving, String text, double x, double y, double z) {
		super(source);
		this.text = text;
		this.x = x;
		this.y = y;
		this.z = z;
		this.entityLiving = entityLiving;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}
	public EntityLiving getEntityLiving() {
		return entityLiving;
	}

}
