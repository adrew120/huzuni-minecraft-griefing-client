package net.halalaboos.client.commands;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.base.BindCommand;
import net.halalaboos.client.base.Binding;
import net.halalaboos.client.base.Command;
import net.halalaboos.client.event.events.EventKeyPressed;
import net.halalaboos.client.files.BindFile;
import net.halalaboos.client.manager.ModManager;

public class Bind extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "bind" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".bind <keyname> <message / mod>", ".bind clear (only clears messages)", ".bind list"};
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		if(args[1].equalsIgnoreCase("list")) {
			for(Binding b : BindFile.getBinds()) {
				if(b instanceof BindCommand) {
					mc.theClient.addChatMessage("'" + b.getName() + "' bound to '" + b.getKeyName() + "'");
				}
			}
		} else if(args[1].equals("clear")) {
			for(Binding b : BindFile.getBinds())
				mc.theClient.getEventHandler().unRegisterListener(EventKeyPressed.class, b);

			BindFile.getBinds().clear();
			mc.theClient.addChatMessage("All binds cleared.");
		} else {
			String command = originalString.split(args[0] + " " + args[1] + " ")[1];
			int keyBind = Keyboard.getKeyIndex(args[1].toUpperCase());
			if(keyBind == 0)
				keyBind = -1;
			if(ModManager.getMod(command) != null) {
				ModManager.getMod(command).setKey(keyBind);
				mc.theClient.addChatMessage("Mod '" + command + "' bound to key '" + args[1] + "'");
			} else {
				BindCommand bind = new BindCommand(command, keyBind);
				BindFile.add(bind);
				mc.theClient.addChatMessage("'" + command + "' bound to key '" + args[1] + "'");
			}
		}
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Binds a key to a mod, or a message that is sent into chat.";
	}
}
