package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.files.SettingsFile;
import net.minecraft.src.GuiScreen;

public class GetIP extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "getip" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".getip" };
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Copies the current server's IP to your clipboard.";
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		GuiScreen.setClipboardString(SettingsFile.getServerIP() + ":" + SettingsFile.getServerPort());
		mc.theClient.addChatMessage("'" + SettingsFile.getServerIP() + ":" + SettingsFile.getServerPort() + "' copied to clipboard.");
	}

}
