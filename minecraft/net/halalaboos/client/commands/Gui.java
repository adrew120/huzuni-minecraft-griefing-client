package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.ui.Gui.Type;

public class Gui extends Command {
	
	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "gui" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".gui style <mode>", ".gui list"};
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		if(args.length > 0) {
			if(args[1].equalsIgnoreCase("style")) {
				String[] selectedType = originalString.split(args[0] + " " + args[1] + " ");
				for(Type type : Type.values()) {
					if(type.getName().equalsIgnoreCase(selectedType[1])) {
						GuiManager.selectGUIStyle(type);
						mc.theClient.addChatMessage("GUI style set to '\247e" + type.getName() + "\247f'.");
					}
				}
			} else if(args[1].equalsIgnoreCase("list")) {
				String styles = "Styles: ";
				for(Type type : Type.values()) {
					styles += "\247e" + type.getName() + "\247f, ";				
				}
				mc.theClient.addChatMessage(styles);
			}
		}
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "changes the style of your GUI.";
	}
}
