package net.halalaboos.client.commands;

import java.awt.Toolkit;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import net.halalaboos.client.base.Command;
import net.halalaboos.client.utils.Connection;
import net.minecraft.src.*;

public class Debug extends Command {
    private List inventory;

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "debug" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".debug (you facked up hal)" };
	}
	
	@Override
	public void runCommand(String originalString, String[] args) {
		/*
		List<IRecipe> craftableRecipes = new ArrayList<IRecipe>();
		inventory = copyInventoryStacks();
		for(Object o : CraftingManager.getInstance().getRecipeList()) {
			if(o instanceof ShapedRecipes) {
				ShapedRecipes recipe = (ShapedRecipes) o;
				boolean isCraftable = true;
				for(ItemStack item : recipe.getRecipeItems()) {
					if(item != null) {
						if(!containsInventory(item))
							isCraftable = false;	
					}
				}
				if(isCraftable)
					craftableRecipes.add(recipe);
			} else if(o instanceof ShapelessRecipes) {
				ShapelessRecipes recipe = (ShapelessRecipes) o;
				boolean isCraftable = true;
				for(Object o1 : recipe.getRecipeItems()) {
					ItemStack item = (ItemStack) o1;
					if(item != null) {
						if(!containsInventory(item))
							isCraftable = false;	
					}
				}
				if(isCraftable)
					craftableRecipes.add(recipe);
			}
			inventory = copyInventoryStacks();
		}
		String recipeees = "Recipes: ";
		for(IRecipe recipe : craftableRecipes)
			recipeees += "\2479" + recipe.getRecipeOutput().getDisplayName() + "\247f, ";
		mc.theClient.addChatMessage(recipeees);
		*/
	}
	
	private boolean containsInventory(ItemStack item) {
		for (int o = 0; o < inventory.size(); o++) {
			ItemStack item1 = (ItemStack)inventory.get(o);
			if (item1.itemID == item.itemID) {
				if(item1.getItemDamage() != item.getItemDamage() && item1.getItemDamage() != 32767)
					continue;
				item1.stackSize--;
				if(item1.stackSize < 1) {
					inventory.remove(item1);
				}
				return true;
			}
		}
		return false;
	}
	
	private List copyInventoryStacks() {
		List copy = new ArrayList();
		for(Object o : mc.thePlayer.inventoryContainer.inventorySlots) {
			if(((Slot)o).getHasStack())
				copy.add(((Slot)o).getStack().copy());
		}
		return copy;
		
	}
	
	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Debugging command, shouldn't do anything.";
	}
}
