package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.files.FriendsFile;


public class Remove extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "remove" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".remove <username / alias>" };
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		String alias = originalString.substring(7);
		if(FriendsFile.remove(alias))
			mc.theClient.addChatMessage(alias + " removed.");
		else
			mc.theClient.addChatMessage("Does not exist!");
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Removes a friend.";
	}
}
