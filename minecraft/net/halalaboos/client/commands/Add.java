package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.files.FriendsFile;

public class Add extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "add" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".add <username> <alias>" };
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		String username = args[1];
		String alias = originalString.split(args[0] + " " + args[1] + " ")[1];
		mc.theClient.addChatMessage("'" + username + "' protected under '" + alias + "'");
		FriendsFile.add(username, alias);
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Adds a friend.";
	}

}
