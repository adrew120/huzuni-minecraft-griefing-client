package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.minecraft.src.Enchantment;
import net.minecraft.src.ItemStack;
import net.minecraft.src.StatCollector;

public class Enchant extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "enchant" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".enchant (Have to be in creative mode)" };
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		if(mc.playerController.isInCreativeMode()) {
			ItemStack item = mc.thePlayer.inventory.getCurrentItem();
			if (item == null) {
				mc.theClient.addChatMessage("You must be holding an item!");
				return;
			}
			String enchantments = "Item enchanted with: ";
			for (Enchantment enchantment : Enchantment.enchantmentsList) {
				if (enchantment == null)
					continue;
				if (enchantment.type.canEnchantItem(item.getItem())) {
					item.addEnchantment(enchantment, enchantment.getMaxLevel());
					enchantments += "\247a" + StatCollector.translateToLocal(enchantment.getName()) + " " + enchantment.getMaxLevel() + "\247f, ";
				}
			}
			mc.theClient.addChatMessage(enchantments);
		} else
			mc.theClient.addChatMessage("You have to be in creative mode!");
	}
	
	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Forces all enchantments possible onto the current item held. (Requires creative)";
	}
}
