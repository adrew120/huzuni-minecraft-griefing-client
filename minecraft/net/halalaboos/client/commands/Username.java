package net.halalaboos.client.commands;

import net.halalaboos.client.Client;
import net.halalaboos.client.base.Command;
import net.halalaboos.client.files.SettingsFile;


public class Username extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "username" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".username <name>" };
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		String username = originalString.substring(9);
		mc.theClient.addChatMessage(Client.CLIENT_TITLE + " will now refer to you as '" + username + "'.");
		SettingsFile.setUsername(username);
	}
	
	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Changes your Huzuni client-sided name.";
	}
}
