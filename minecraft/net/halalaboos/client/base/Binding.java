package net.halalaboos.client.base;

import net.halalaboos.client.Client;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventKeyPressed;
import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;

public abstract class Binding implements Listener {
	
	public Minecraft mc = Client.getMC();
	private int keyCode;
	private String name;
	public Binding(String name, int keyCode) {
		this.keyCode = keyCode;
		this.name = name;
	}
	
	public abstract void onPressed();

	public String getKeyName() {
		return getKeyCode() == -1 ? "-1":Keyboard.getKeyName(getKeyCode());
	}
	
	public boolean keyPressed(int keyCode) {
		return getKeyCode() == keyCode;
	}
	
	public int getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventKeyPressed) {
			EventKeyPressed kEvent = (EventKeyPressed) event;
			if(keyPressed(kEvent.getKey())) {
				onPressed();
			}
		}
	}
}
