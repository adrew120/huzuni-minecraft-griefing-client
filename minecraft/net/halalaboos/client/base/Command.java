package net.halalaboos.client.base;

import net.halalaboos.client.Client;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.minecraft.client.Minecraft;

public abstract class Command {

	public Minecraft mc = Client.getMC();
	
	public Command() {
		
	}
	
	public abstract String[] getAliases();
	public abstract String[] getCommandHelp();
	public abstract String getCommandDescription();

	public abstract void runCommand(String originalString, String[] args);

}
