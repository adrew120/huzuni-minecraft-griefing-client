package net.halalaboos.client.base;

public class BindCommand extends Binding {
	private String command;
	
	public BindCommand(String command, int keyCode) {
		super(command, keyCode);
		this.command = command;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onPressed() {
		if(mc.theWorld != null)
			mc.thePlayer.sendChatMessage(command);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
