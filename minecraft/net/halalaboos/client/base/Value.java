package net.halalaboos.client.base;

import java.text.DecimalFormat;
import net.halalaboos.client.Client;

public class Value {

	private String name;
	private float value, minimalValue, maxValue, defaultValue, incrementValue;
	
	public Value(String valName, float defaultVal, float minVal, float maxVal, float incrementVal) {
		name = valName;
		value = defaultVal;
		defaultValue = defaultVal;
		minimalValue = minVal;
		maxValue = maxVal;
		incrementValue = incrementVal;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public float getMin() {
		return minimalValue;
	}

	public void setMin(float min) {
		this.minimalValue = min;
	}

	public float getMax() {
		return maxValue;
	}

	public void setMax(float max) {
		this.maxValue = max;
	}

	public float getDef() {
		return defaultValue;
	}
	
	public float getIncrementValue() {
		return incrementValue;
	}

	public String getName() {
		return name;
	}
	

}
