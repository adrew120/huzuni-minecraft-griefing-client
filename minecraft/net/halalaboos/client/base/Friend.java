package net.halalaboos.client.base;

import net.minecraft.src.StringUtils;

public class Friend {

	private String username, alias;
	
	public Friend(String username, String alias) {
		this.username = username;
		this.alias = alias;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	public boolean equals(String usernamealias) {
		usernamealias = StringUtils.stripControlCodes(usernamealias);
		return username.equalsIgnoreCase(usernamealias) || alias.equalsIgnoreCase(usernamealias);
	}
}
