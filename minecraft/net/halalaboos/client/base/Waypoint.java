package net.halalaboos.client.base;

import java.awt.Color;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.utils.Coordinates;

public class Waypoint {

	private Coordinates coordinates;
	private String name, server;
	private int port;
	private Color color;
	
	public Waypoint(String name, double posX, double posY, double posZ) {
		this(name, posX, posY, posZ, SettingsFile.getServerIP(), SettingsFile.getServerPort(), Client.getClientUtils().getRandomColor());
	}
	
	public Waypoint(String name, double posX, double posY, double posZ, String server, int port) {
		this(name, posX, posY, posZ, server, port, Client.getClientUtils().getRandomColor());
	}
	
	public Waypoint(String name, double posX, double posY, double posZ, String server, int port, Color color) {
		this.name = name;
		coordinates = new Coordinates(posX, posY, posZ);
		this.server = server;
		this.port = port;
		this.color = color;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isOnServer() {
		return server == null || port == 0 ? true:SettingsFile.getServerIP().equalsIgnoreCase(server) && SettingsFile.getServerPort() == port;
	}
	
	public float getRed() {
		return (float) color.getRed() / 255F;
	}
	
	public float getGreen() {
		return (float) color.getGreen() / 255F;
	}
	
	public float getBlue() {
		return (float) color.getBlue() / 255F;
	}

	public String getServer() {
		return server;
	}

	public int getPort() {
		return port;
	}
	
}
