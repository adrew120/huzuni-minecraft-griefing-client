package net.halalaboos.client.overrides;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventServerUpdate;
import net.halalaboos.client.event.events.EventSendMessage;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.minecraft.client.Minecraft;
import net.minecraft.src.DamageSource;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityItem;
import net.minecraft.src.ItemStack;
import net.minecraft.src.MathHelper;
import net.minecraft.src.NetClientHandler;
import net.minecraft.src.Packet101CloseWindow;
import net.minecraft.src.Packet10Flying;
import net.minecraft.src.Packet11PlayerPosition;
import net.minecraft.src.Packet12PlayerLook;
import net.minecraft.src.Packet13PlayerLookMove;
import net.minecraft.src.Packet14BlockDig;
import net.minecraft.src.Packet18Animation;
import net.minecraft.src.Packet19EntityAction;
import net.minecraft.src.Packet202PlayerAbilities;
import net.minecraft.src.Packet205ClientCommand;
import net.minecraft.src.Packet3Chat;
import net.minecraft.src.Session;
import net.minecraft.src.StatBase;
import net.minecraft.src.World;

public class HEntityClientPlayerMP extends EntityClientPlayerMP {

	public NetClientHandler sendQueue;
    private double oldPosX;

    /** Old Minimum Y of the bounding box */
    private double oldMinY;
    private double oldPosY;
    private double oldPosZ;
    private float oldRotationYaw;
    private float oldRotationPitch;

    /** Check if was on ground last update */
    private boolean wasOnGround = false;

    /** should the player stop sneaking? */
    private boolean shouldStopSneaking = false;
    private boolean wasSneaking = false;
    private int field_71168_co = 0;

    /** has the client player's health been set? */
    private boolean hasSetHealth = false;

    private Mod freecam;
    public HEntityClientPlayerMP(Minecraft par1Minecraft, World par2World,
			Session par3Session, NetClientHandler par4NetClientHandler) {
		super(par1Minecraft, par2World, par3Session, par4NetClientHandler);
		this.mc = par1Minecraft;
        this.sendQueue = par4NetClientHandler;
        if(freecam == null)
        	freecam = ModManager.getMod("Freecam");
		// TODO Auto-generated constructor stub
	}
    
    public void moveEntity(double motionX, double motionY, double motionZ) {
    	EventMovement event = (EventMovement) mc.theClient.getEventHandler().call(new EventMovement(this, motionX, motionY, motionZ));
    	if(event.isCancelled())
    		return;
    	else {
    		motionX = event.getMotionX();
        	motionY = event.getMotionY();
        	motionZ = event.getMotionZ();
    	}
    	
    	super.moveEntity(motionX, motionY, motionZ);
    }
    
    /**
     * Send updated motion and position information to the server
     */
    public void sendMotionUpdates()
    {
    	EventMovement event = (EventMovement) mc.theClient.getEventHandler().call(new EventMovement(this, EventMovement.EventType.PRE_UPDATE));
    	if(event.isCancelled())
    		return;
        boolean var1 = this.isSprinting();

        if (var1 != this.wasSneaking)
        {
            if (var1)
            {
                this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 4));
            }
            else
            {
                this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 5));
            }

            this.wasSneaking = var1;
        }

        boolean var2 = this.isSneaking();

        if (var2 != this.shouldStopSneaking)
        {
            if (var2)
            {
                this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 1));
            }
            else
            {
                this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 2));
            }

            this.shouldStopSneaking = var2;
        }
        EventServerUpdate rotEvent = (EventServerUpdate) mc.theClient.getEventHandler().call(new EventServerUpdate(this, rotationYaw, rotationPitch));
    	float newRotYaw = rotEvent.getRotationYaw(), newRotPitch = rotEvent.getRotationPitch();
    	double var3 = this.posX - this.oldPosX;
        double var5 = this.boundingBox.minY - this.oldMinY;
        double var7 = this.posZ - this.oldPosZ;
        double var9 = (double)(newRotYaw - this.oldRotationYaw);
        double var11 = (double)(newRotPitch - this.oldRotationPitch);
        boolean var13 = var3 * var3 + var5 * var5 + var7 * var7 > 9.0E-4D || this.field_71168_co >= 20;
        boolean var14 = var9 != 0.0D || var11 != 0.0D;

        if (this.ridingEntity != null)
        {
            this.sendQueue.addToSendQueue(new Packet13PlayerLookMove(this.motionX, -999.0D, -999.0D, this.motionZ, newRotYaw, newRotPitch, this.onGround));
            var13 = false;
        }
        else if (var13 && var14)
        {
            this.sendQueue.addToSendQueue(new Packet13PlayerLookMove(this.posX, this.boundingBox.minY, this.posY, this.posZ, newRotYaw, newRotPitch, this.onGround));
        }
        else if (var13)
        {
            this.sendQueue.addToSendQueue(new Packet11PlayerPosition(this.posX, this.boundingBox.minY, this.posY, this.posZ, this.onGround));
        }
        else if (var14)
        {
            this.sendQueue.addToSendQueue(new Packet12PlayerLook(newRotYaw, newRotPitch, this.onGround));
        }
        else
        {
            this.sendQueue.addToSendQueue(new Packet10Flying(this.onGround));
        }

        ++this.field_71168_co;
        this.wasOnGround = this.onGround;

        if (var13)
        {
            this.oldPosX = this.posX;
            this.oldMinY = this.boundingBox.minY;
            this.oldPosY = this.posY;
            this.oldPosZ = this.posZ;
            this.field_71168_co = 0;
        }

        if (var14)
        {
            this.oldRotationYaw = this.rotationYaw;
            this.oldRotationPitch = this.rotationPitch;
        }
    	mc.theClient.getEventHandler().call(new EventMovement(this, EventMovement.EventType.POST_UPDATE));
    }
    
    public void sendChatMessage(String par1Str)
    {
    	EventSendMessage event = (EventSendMessage) mc.theClient.getEventHandler().call(new EventSendMessage(this, par1Str));
        if(event.isCancelled())
        	return;
    	this.sendQueue.addToSendQueue(new Packet3Chat(event.getMessage()));
    }
    
    public boolean isEntityInsideOpaqueBlock()
    {
    	if(freecam.isState())
    		return false;
    	return super.isEntityInsideOpaqueBlock();
    }
    protected boolean pushOutOfBlocks(double par1, double par3, double par5)
    {
    	if(freecam.isState())
    		return false;
    	
    	return super.pushOutOfBlocks(par1, par3, par5);
    }
}
